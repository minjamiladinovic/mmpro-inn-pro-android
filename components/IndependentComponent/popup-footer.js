import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, Modal, TextInput, FlatList } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import InputHeading from '../IndependentComponent/text-input-heading';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

export default class PopupFooter extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { text, onClick } = this.props;
        return (
            <View style={{ backgroundColor: colors.colorGray, alignItems: 'center', margin: -10, height: 50 }}>
                <Text onPress={() => onClick()} style={styles.text}>{text}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontFamily: Fonts.boldText,
        fontSize: 18,
        color: colors.colorWhite,
        marginLeft: 10,
        height: 30,
        paddingHorizontal: 10,
        backgroundColor: colors.colorPrimary,
        marginTop:10
    },
});

PopupFooter.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func
};

