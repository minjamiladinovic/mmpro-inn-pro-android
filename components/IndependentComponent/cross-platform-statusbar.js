
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput, Modal,StatusBar } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';
import DeviceInfo from 'react-native-device-info'

export default class FilterModal extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { backgroundColor } = this.props;
        const hasNotch = DeviceInfo.hasNotch();
        const model = DeviceInfo.getModel();
        return (
            <View style={[{ height: hasNotch ? (model == 'iPhone XR' ? 40 : 37) : 20 }, { backgroundColor }]}>
                <StatusBar translucent backgroundColor={backgroundColor} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    tagView: {
        backgroundColor: colors.colorPrimary,
        paddingHorizontal: 5,
        height: 25,
        borderRadius: 3,
        justifyContent: "space-between",
        alignItems: "center",
        margin: 3,
        flexDirection: "row"
    },
    input: {
        fontSize: 12,
        fontFamily: Fonts.ralewayMedium,
        height: 30,
        padding: 0
    }
});

FilterModal.propTypes = {
    backgroundColor: PropTypes.string,
}
