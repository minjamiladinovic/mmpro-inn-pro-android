import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity, FlatList, TextInput } from "react-native";
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

import TagViewList from '../IndependentComponent/tag-view-list';


export default class InputContainer extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { removeItem, data, onClick } = this.props;
        return (
            <View style={{
                backgroundColor: colors.colorWhite,
                borderWidth: 0.5,
                borderColor: colors.colorPrimary,
                borderRadius: 10,
                padding: 3,
                marginVertical: 10,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <TagViewList removeItem={(item) => removeItem(item)} data={data} />
                <TouchableOpacity onPress={() => onClick()} >
                    <Ionicons name="md-arrow-dropdown" style={{ fontSize: 25, color: colors.colorPrimary,textAlign:'right',paddingHorizontal:15 }} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    input: {
        fontSize: 12,
        fontFamily: Fonts.ralewayMedium,
        backgroundColor: 'red',
        height: 30,
        padding: 0
    }
});

InputContainer.propTypes = {
    removeItem: PropTypes.func,
    data: PropTypes.array,
    onClick: PropTypes.func,

};

