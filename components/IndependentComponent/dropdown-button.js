import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fonts from '../../common-tasks/values/fonts.js';
import Dimension from '../../common-tasks/values/dimension.js';
import Styles from "../../common-tasks/values/styles"
import colors from '../../common-tasks/values/colors.js';
import Strings from '../../common-tasks/values/strings.js';

class DropDownButton extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const { text, onClick } = this.props;
        return (
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity style={styles.container} onPress={() => onClick()}>
                    <Text style={styles.head_text}>{text}</Text>
                    <Ionicons name="md-arrow-dropdown" style={{ fontSize: 15, color: colors.colorDarkGray,marginLeft:10 }} />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    head_text: {
        fontFamily: Fonts.ralewayMedium,
        fontSize: 12,
        color: colors.colorDarkGray,
        marginLeft: 5
    },
    container: {
        backgroundColor: colors.colorXLightGray,
        paddingHorizontal: 5,
        height: 25,
        borderRadius: 3,
        justifyContent: "space-between",
        alignItems: "center",
        margin: 3,
        flexDirection: "row",
        marginVertical:10
    }
});

DropDownButton.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func
}

export default DropDownButton;
