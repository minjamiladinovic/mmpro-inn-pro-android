
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    ScrollView,
    StatusBar,
    Platform,
    SafeAreaView,
    ActivityIndicator,
    BackHandler
} from 'react-native';

import { Picker, Item } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { TextInputMask } from 'react-native-masked-text';
import statesJsonObject from "../../assets/files/states";
import Fonts from '../../common-tasks/values/fonts';
import colors from '../../common-tasks/values/colors';
import Dimension from '../../common-tasks/values/dimension';
import Strings from '../../common-tasks/values/strings';
import InputHeading from '../IndependentComponent/text-input-heading';
import InputContainer from '../IndependentComponent/input-container';
import SeperatorLine from '../IndependentComponent/seperator-line';
import DropDownButton from '../IndependentComponent/dropdown-button';
import FilterModal from '../IndependentComponent/filter-modal';
import LightBackground from '../IndependentComponent/Light-background';
import MyStatusBar from '../IndependentComponent/cross-platform-statusbar';
import DropDownPopup from '../IndependentComponent/drop-down-popup';

const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class BusinessServiceAreaAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: this.props.navigation.state.params.uid,
            loginStatus:this.props.navigation.state.params.loginStatus,


            searchOtherStateText: '',
            searchOtherCityText: '',
            searchOtherZipText: '',
            searchAutoSelectedCityTerm: '',
            searchAutoSelectedZipCodeTerm: '',

            isStateFilterOpen: false,
            isCityFilterOpen: false,
            isZipFilterOpen: false,

            isAutoSelectedCitiesPopupOpen: false,
            isAutoSelectedZipCodesPopupOpen: false,

            selectedStatesArr: [],
            autoSelectedCitiesArr: [],
            otherSelectedCitiesArr: [],
            autoSelectedZipCodesArr: [],
            otherSelectedZipCodesArr: [],

            serachCitiesArr: [],
            serachStatesArr: [],
            serachZipCodesArr: [],
            allZipCodes: [],
            tempAutoZips: [],
            tempAutoCities: [],

            allInitialSelectedZips:[],
            allInitialSelectedCities:[]
        }
    }


    handleBackButton = () => {
        return true;
    }

    componentDidMount() {
        var zipCode = [];

        this.setState({
            selectedStatesArr: [],
            otherSelectedCitiesArr: [],
            autoSelectedCitiesArr: [],
            otherSelectedZipCodesArr: [],
            autoSelectedZipCodesArr: [],
            serachCitiesArr: [],
            serachZipCodesArr: [],
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        setTimeout(() => {
            var tempZips = [];
            var tempStates = [];
            statesJsonObject.forEach(element => {
                tempStates.push(element.name);
                Object.values(element.cities).forEach(el => {
                    el.forEach(zip => {
                        tempZips.push(zip + ' - ' + this.getKeyByValue(element.cities, el) + ', ' + element.name)
                    })
                })
            });
            this.setState({ allZipCodes: tempZips, serachStatesArr: tempStates })
        }, 1000)
    }
    componentWillUnmount() {
        this.backHandler.remove()
    }
    apiUpdateBusiness() {
        this.setState({ isLoading: true });
        db.collection("users").doc(this.state.uid).update({

            "BusinessInfo.businessZip.autoSelected": this.state.autoSelectedZipCodesArr.map(el => this.getZipCodeObj(el)),
            "BusinessInfo.businessZip.otherSelected": this.state.otherSelectedZipCodesArr.map(el => this.getZipCodeObj(el)),
            "BusinessInfo.businessCity.autoSelected": this.state.autoSelectedCitiesArr.map(el => this.getCityObj(el)),
            "BusinessInfo.businessCity.otherSelected": this.state.otherSelectedCitiesArr.map(el => this.getCityObj(el)),
            "BusinessInfo.businessState": this.state.selectedStatesArr,
            "BusinessInfo.SelectedZip": (this.state.autoSelectedZipCodesArr.concat(this.state.otherSelectedZipCodesArr)).map(el => this.getZipCode(el)),

        }).then((user) => {
            this.setState({ isLoading: false });
            this.props.navigation.replace('CreateAccountStep4Screen', { uid: this.state.uid, loginStatus: this.state.loginStatus });
            CommonTasks._displayToast("Successfully updated Business Info");
        }).catch((error) => {
            this.setState({ isLoading: false });
        })
    }

    removeOtherSelectedState = (stateItem) => {

    }
    removeOtherSelectedCity = (cityItem) => {

    }
    removeOtherSelectedZipCode = (zipCodeItem) => {

    }
    filterStates = text => {
        var s = statesJsonObject.slice();
        var searchResult = [];
        var i = 0;
        if (text != '') {
            searchResult = s.filter(function (data) {
                return data.name.toLowerCase().indexOf(text.toLowerCase()) > -1;
            }).map(el => el.name)
            this.setState({ serachStatesArr: searchResult, searchOtherStateText: text })

        } else {
            this.setState({ serachStatesArr: s.map(el => el.name), searchOtherStateText: '' })
        }
    }
    filterCities = val => {
        var tempArr = [];
        statesJsonObject.forEach(element => {
            Object.keys(element.cities).forEach(cityName => {
                if (cityName.toLowerCase().startsWith(val.toLowerCase())) {
                    tempArr.push(cityName + ', ' + element.name)
                }
            })
        });
        this.setState({ searchOtherCityText: val, serachCitiesArr: tempArr });
    }
    filterZipCodes = val => {
        var tempZips = []
        this.state.allZipCodes.forEach(el => {
            if (el.startsWith(val)) {
                tempZips.push(el);
            }
        })
        this.setState({ searchOtherZipText: val, serachZipCodesArr: tempZips });
    }
    searchAutoSelectedZipCodeWithTerm = val => {
        var tempZips = [];
        let tempAllArr=this.state.allInitialSelectedZips;
        if (val != '') {
            this.state.allInitialSelectedZips.forEach(el => {
                if (el.startsWith(val)) {
                    tempZips.push(el);
                }
            })
            this.setState({ autoSelectedZipCodesArr: tempZips, searchAutoSelectedZipCodeTerm: val });
        } else {
            this.setState({ autoSelectedZipCodesArr: tempAllArr, searchAutoSelectedZipCodeTerm: val });
        }
    }
    searchAutoSelectedCityWithTerm = val => {
        var tempCities = []
        let tempAllArr=this.state.allInitialAutoSelectedCities;
        if (val != '') {
            this.state.allInitialAutoSelectedCities.forEach(el => {
                if (el.toLowerCase().startsWith(val.toLowerCase())) {
                    tempCities.push(el);
                }
            })
            this.setState({ autoSelectedCitiesArr: tempCities, searchAutoSelectedCityTerm: val });
        } else {
            this.setState({ autoSelectedCitiesArr: tempAllArr, searchAutoSelectedCityTerm: val });
        }
    }
    removeCity = val => {
        this.selectCity(val)
    }
    removeState = val => {
        this.selectState(val);
    }

    removeZipCode = val => {
        this.selectZipCode(val)
    }
    getKeyByValue(object, value) {
        return Object.keys(object).find(key => object[key] === value);
    }
    getStateName(stateItem) {
        return stateItem;
    }
    getCityName(cityItem) {
        return cityItem.split(',')[0];
    }
    getZipCode(zipCodeItem) {
        return zipCodeItem.split(' ')[0];
    }
    getCityObj(cityItem) {
        return { cityName: this.getCityName(cityItem), cityStateName: cityItem };
    }
    getZipCodeObj(zipCodeItem) {
        return { zipCode: this.getZipCode(zipCodeItem), cityStateZipCode: zipCodeItem };
    }
    getZipCodeItem(zipCode, stateName) {
        return zipCode + ' - ' + this.getCityFromZip(zipCode, stateName) + ', ' + stateName;
    }
    getCitiesOfAState(stateName) {
        let stateElement = statesJsonObject.find(el => el.name == stateName);
        return Object.keys(stateElement.cities);
    }
    getZipCodesOfAState(stateName) {
        let stateElement = statesJsonObject.find(el => el.name == stateName);
        let tempZips = [];
        Object.values(stateElement.cities).forEach(el => el.forEach(zip => {
            tempZips.push(zip)
        }))
        return tempZips;
    }
    getZipCodesOfACity(cityName, stateName) {
        var tempZips = [];
        let stateElement = statesJsonObject.find(el => el.name == stateName);
        return stateElement.cities[cityName];
    }
    getCityFromZip(zipCode, stateName) {
        let stateElement = statesJsonObject.find(el => el.name == stateName);
        var city;
        Object.keys(stateElement.cities).forEach(element => {
            if (stateElement.cities[element].indexOf(zipCode) > -1) {
                city = element;
            }
        });
        return city;
    }
    closeStateFilter = () => {
        this.setState({ isStateFilterOpen: false });
    }
    openStatesFilter = () => {
        this.setState({ isStateFilterOpen: true });
    }

    closeCitiesFilter = () => {
        this.setState({ isCityFilterOpen: false });
    }
    openCitiesFilter = () => {
        this.setState({ isCityFilterOpen: true });
    }

    closeZipFilter = () => {
        this.setState({ isZipFilterOpen: false });
    }
    openZipFilter = () => {
        this.setState({ isZipFilterOpen: true });
    }

    openAutoSelectedZipCodesPopup = () => {
        var tempArr = this.state.autoSelectedZipCodesArr;
        this.setState({ isAutoSelectedZipCodesPopupOpen: true, tempAutoZips: tempArr,allInitialSelectedZips:tempArr });
    }
    closeAutoSelectedZipCodesPopup = () => {
        var tempArr = this.state.tempAutoZips;
        this.setState({ isAutoSelectedZipCodesPopupOpen: false, autoSelectedZipCodesArr: tempArr });
    }
    openAutoSelectedCitiesPopup = () => {
        var tempArr = this.state.autoSelectedCitiesArr;
        this.setState({ isAutoSelectedCitiesPopupOpen: true, tempAutoCities: tempArr,allInitialAutoSelectedCities:tempArr });
    }
    closeAutoSelectedCitiesPopup = () => {
        var tempArr = this.state.tempAutoCities;
        this.setState({ isAutoSelectedCitiesPopupOpen: false, autoSelectedCitiesArr: tempArr });
    }

    selectState = stateName => {
        let tempArr = JSON.parse(JSON.stringify(this.state.selectedStatesArr));
        let tempCitiesArr = JSON.parse(JSON.stringify(this.state.autoSelectedCitiesArr));
        let tempZipsArr = JSON.parse(JSON.stringify(this.state.autoSelectedZipCodesArr));

        let citiesForThisState = this.getCitiesOfAState(stateName).map(el => el + ', ' + stateName);
        let zipCodesForThisState = this.getZipCodesOfAState(stateName).map(el => this.getZipCodeItem(el, stateName));
        if (tempArr.indexOf(stateName) > -1) {
            let i = tempArr.indexOf(stateName);
            tempArr.splice(i, 1);
            var newCityArr = [];
            var newZipArr = [];
            tempCitiesArr.forEach(el => {
                if (!el.includes(stateName)) {
                    newCityArr.push(el)
                }
            });
            tempZipsArr.forEach(el => {
                if (!el.includes(stateName)) {
                    newZipArr.push(el)
                }
            });
            this.setState({ selectedStatesArr: tempArr, autoSelectedZipCodesArr: newZipArr, autoSelectedCitiesArr: newCityArr })
        } else {
            tempArr.push(stateName);
            citiesForThisState.forEach(el => tempCitiesArr.push(el))
            zipCodesForThisState.forEach(el => tempZipsArr.push(el))
            this.setState({ selectedStatesArr: tempArr, autoSelectedZipCodesArr: tempZipsArr, autoSelectedCitiesArr: tempCitiesArr })
        }
    }
   
    selectCity = cityItem => {
        let cityName = this.getCityName(cityItem);
        let stateName = cityItem.split(', ')[1];
        let tempArr = this.state.otherSelectedCitiesArr;
        let tempZipsArr = this.state.autoSelectedZipCodesArr;
        let zipCodesForThisCity = this.getZipCodesOfACity(cityName, stateName).map(el => this.getZipCodeItem(el, stateName));
        if (tempArr.indexOf(cityItem) > -1) {
            let i = tempArr.indexOf(cityItem);
            var newZipsArr = [];
            tempArr.splice(i, 1);
            tempZipsArr.forEach(el => {
                if (!zipCodesForThisCity.includes(el)) {
                    newZipsArr.push(el)
                }
            });
            this.setState({ otherSelectedCitiesArr: tempArr, autoSelectedZipCodesArr: newZipsArr })
        } else {
            if (this.state.autoSelectedCitiesArr.includes(cityItem)) {
                CommonTasks._displayToast("City is already auto selcted!")
                return false;
            }
            tempArr.push(cityItem)
            zipCodesForThisCity.forEach(el => tempZipsArr.push(el))
            this.setState({ otherSelectedCitiesArr: tempArr, autoSelectedZipCodesArr: tempZipsArr })
        }
    }
    selectZipCode = zipCodeItem => {
        let tempArr = this.state.otherSelectedZipCodesArr;
        if (tempArr.indexOf(zipCodeItem) > -1) {
            let i = tempArr.indexOf(zipCodeItem);
            tempArr.splice(i, 1);
            this.setState({ otherSelectedZipCodesArr: tempArr })
        } else {
            if (this.state.autoSelectedZipCodesArr.includes(zipCodeItem)) {
                CommonTasks._displayToast("Zip code is already auto selcted!")
                return false;
            }
            tempArr.push(zipCodeItem)
            this.setState({ otherSelectedZipCodesArr: tempArr })
        }
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <MyStatusBar backgroundColor={colors.colorPrimary} />
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.navigate('SigninWithEmailScreen')}>
                        <Ionicons name="ios-arrow-round-back" style={styles.headerImage} />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Business Service Area</Text>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.apiUpdateBusiness()}>
                        <Ionicons name="ios-checkmark" style={styles.headerImage} />
                    </TouchableOpacity>
                </View>
                <ScrollView contentContainerStyle={styles.main_scroll}>
                    <InputHeading text={'Add By State'} />
                    <InputContainer data={this.state.selectedStatesArr} removeItem={this.removeState} onClick={this.openStatesFilter} />
                    <SeperatorLine />

                    <InputHeading text={'Add By City'} />
                    <InputContainer
                        data={this.state.otherSelectedCitiesArr}
                        removeItem={this.removeCity} onClick={this.openCitiesFilter} />
                    <Text style={styles.auto_selected_zip_msg_txt}>{this.state.autoSelectedCitiesArr.length} Cities automatically selected based on state</Text>
                    <DropDownButton text={'Show Selected Cities'} onClick={this.openAutoSelectedCitiesPopup} />
                    <SeperatorLine />

                    <InputHeading text={'Add By Zip Code'} />
                    <InputContainer
                        data={this.state.otherSelectedZipCodesArr}
                        removeItem={this.removeZipCode} onClick={this.openZipFilter} />
                    <Text style={styles.auto_selected_zip_msg_txt}>{this.state.autoSelectedZipCodesArr.length} Zip Codes selected based on state or city</Text>
                    <DropDownButton text={'Show Selected Zip Codes'} onClick={this.openAutoSelectedZipCodesPopup} />

                    <FilterModal ngIf={this.state.isStateFilterOpen}
                        onClose={this.closeStateFilter}
                        placeholderText={'Type to search state name '}
                        textVal={this.state.searchOtherStateText}
                        searchFilterFunction={this.filterStates}
                        marginTop={100} arrData={this.state.selectedStatesArr}
                        searchData={this.state.serachStatesArr}
                        chooseItem={this.selectState}
                    />
                    <FilterModal ngIf={this.state.isCityFilterOpen}
                        placeholderText={'Type to search city name '}
                        onClose={this.closeCitiesFilter}
                        textVal={this.state.searchOtherCityText}
                        searchFilterFunction={this.filterCities}
                        marginTop={100}
                        arrData={this.state.otherSelectedCitiesArr}
                        searchData={this.state.serachCitiesArr}
                        chooseItem={this.selectCity}
                    />
                    <FilterModal ngIf={this.state.isZipFilterOpen}
                        placeholderText={'Type to search zip code '}
                        onClose={this.closeZipFilter}
                        textVal={this.state.searchOtherZipText}
                        searchFilterFunction={this.filterZipCodes}
                        marginTop={100}
                        arrData={this.state.otherSelectedZipCodesArr}
                        searchData={this.state.serachZipCodesArr}
                        chooseItem={this.selectZipCode}
                    />
                    <DropDownPopup
                        ngIf={this.state.isAutoSelectedCitiesPopupOpen}
                        onClose={this.closeAutoSelectedCitiesPopup}
                        searchFilterFunction={this.searchAutoSelectedCityWithTerm}
                        inputText={this.state.searchAutoSelectedCityTerm}
                        data={this.state.autoSelectedCitiesArr} />
                    <DropDownPopup
                        ngIf={this.state.isAutoSelectedZipCodesPopupOpen}
                        onClose={this.closeAutoSelectedZipCodesPopup}
                        searchFilterFunction={this.searchAutoSelectedZipCodeWithTerm}
                        inputText={this.state.searchAutoSelectedZipCodeTerm}
                        data={this.state.autoSelectedZipCodesArr.filter(el => el.includes(this.state.searchAutoSelectedZipCodeTerm))} />
                    <KeyboardSpacer />
                </ScrollView>
                <LightBackground
                    ngIf={this.state.isStateFilterOpen ||
                        this.state.isAutoSelectedCitiesPopupOpen ||
                        this.state.isAutoSelectedZipCodesPopupOpen ||
                        this.state.isCityFilterOpen ||
                        this.state.isZipFilterOpen
                    }
                />
                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.colorPrimary
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: colors.colorPrimary,
        paddingVertical: 10,
        alignItems: 'center',
    },

    backContainer: {
        paddingHorizontal: 30
    },

    headerImage: {
        fontSize: 30,
        color: colors.colorWhite,
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: Fonts.ralewayBold,
        color: colors.colorWhite,
        fontSize: 18
    },
    main_scroll: {
        flexGrow: 1,
        height: Dimension.height - 100,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        backgroundColor: colors.colorWhite,
        paddingHorizontal: 30,
        paddingTop: 20
    },
    auto_selected_zip_msg_txt: {
        fontSize: 12,
        fontFamily: Fonts.ralewayMedium,
        color: colors.colorPrimary,
        width: '100%'
    },
    activity_sub: {
        position: 'absolute',
        top: Dimension.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

})

