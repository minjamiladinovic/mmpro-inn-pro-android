import React, { Component } from "react";
import {
    SafeAreaView,
    ScrollView,
    Linking,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    StatusBar,
    RefreshControl,
    FlatList
} from "react-native";

import firebase from "react-native-firebase";

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { CachedImage } from 'react-native-cached-image';
import Feather from 'react-native-vector-icons/Feather';
import { SegmentedControls } from 'react-native-radio-buttons';
import Footer from '../footer/footer';
import { DotsLoader, RotationHoleLoader, CirclesLoader, BubblesLoader } from 'react-native-indicator';


const screenSize = Dimensions.get('window');
const options = [
    "Inbox",
    "Archive",
];

const db = firebase.firestore();

export default class MessageListing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: 'Inbox',
            chatUser: [],
            uid: this.props.navigation.state.params.uid,
            chatUser_inbox: [],
            chatUser_archive: [],
            chatUsr_details: [],
            isLoading: true,
            inbox_show: false,
            refreshing: false,
        }
    }

    componentDidMount() {
        this._getMessages();
        // firebase.analytics().setCurrentScreen("Messages", "Messages");
    }

    setSelectedOption(selectedOption) {
        if (selectedOption.trim() == "Inbox") {
            this.setState({
                inbox_show: true,
                selectedOption: selectedOption
            });
        }
        else {
            this.setState({
                inbox_show: false,
                selectedOption: selectedOption
            });
        }
    }

    _getMessages() {
        var inbox = db.collection("messages").doc(this.state.uid).collection("inbox");
        var archive = db.collection("messages").doc(this.state.uid).collection("archive");

        inbox.get().then((data) => {
            data.docs.map(doc => {
                //console.log(doc.data());
                var obj_id = doc.id;
                var dateValue = new Date(doc.data().chat[0].createdAt);
                var timeNumber = dateValue.valueOf();
                var obj = {
                    "id": doc.id,
                    "date": timeNumber,
                    "message":doc.data().chat[0].description
                };
                //console.log("obj" + obj);
                //this.state.chatUser_inbox.push(obj_id);
                this.state.chatUser_inbox.push(obj);

                //console.log("inbox user" + this.state.chatUser_inbox);
            })
            var dbData = this.state.chatUser_inbox;
            dbData = dbData.sort((a, b) => (b.date > a.date) ? 1 : ((a.date > b.date) ? -1 : 0));
            this.setState({
                chatUser_inbox: dbData
            },this._getInboxDetails.bind(this));
            // for (var i = 0; i < this.state.chatUser_inbox.length; i++) {
            //     var docRef2 = db.collection("users").doc(this.state.chatUser_inbox[i].id);
            //     this._getDetails(docRef2, "yes");
            // }

        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

        archive.get().then((data) => {
            this.setState({ isLoading: true });
            data.docs.map(doc => {
               // console.log(doc.data());
                var obj_id = doc.id;
                var dateValue = new Date(doc.data().chat[0].createdAt);
                var timeNumber = dateValue.valueOf();

                var obj = {
                    "id": doc.id,
                    "date": timeNumber,
                    "message":doc.data().chat[0].description
                };
               // console.log("obj" + obj);
                //this.state.chatUser_archive.push(obj_id);
                this.state.chatUser_archive.push(obj);
              //  console.log("archive user" + JSON.stringify(this.state.chatUser_archive));
            })

            var dbData_archive = this.state.chatUser_archive;
            dbData_archive =dbData_archive.sort((a, b) => (b.date > a.date) ? 1 : ((a.date > b.date) ? -1 : 0));
            this.setState({
                chatUser_archive: dbData_archive
            },this._getAcriveDetails.bind(this));

            // for (var i = 0; i < this.state.chatUser_archive.length; i++) {
            //     var docRef2 = db.collection("users").doc(this.state.chatUser_archive[i].id);
            //     this._getDetails(docRef2, "yes");
            // }
            
        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

    }

    _getInboxDetails() {
        for (var i = 0; i < this.state.chatUser_inbox.length; i++) {
            var docRef2 = db.collection("users").doc(this.state.chatUser_inbox[i].id);
            this._getDetails(docRef2, "yes");
        }
    }
    _getAcriveDetails() {
        for (var i = 0; i < this.state.chatUser_archive.length; i++) {
            var docRef2 = db.collection("users").doc(this.state.chatUser_archive[i].id);
            this._getDetails(docRef2, "yes");
        }
    }

    _getDetails(docRef2, status) {
        if (status.trim() == "yes") {
            this.setState({
                isLoading: true
            });
        }


        if (status == "yes") {
            docRef2.get().then((doc) => {
                if (doc.exists) {
                   // console.log("Document data:", doc.data());
                    var obj = doc.data();
                    this.state.chatUsr_details.push(obj);
                   // console.log("chat user details" + this.state.chatUsr_details);

                    var inbox_len = this.state.chatUser_inbox.length;
                    var archive_len = this.state.chatUser_archive.length;
                    var len = inbox_len + archive_len;

                 //   console.log("total len" + len);
                 //   console.log("chat len" + this.state.chatUsr_details.length);
                    if (this.state.chatUsr_details.length == len) {
                        this.setState({
                            isLoading: false,
                            refreshing: false,
                            inbox_show: true
                        });
                    }
                }
            }).catch((error) => {
                this.setState({
                    isLoading: false
                });
                console.log("Error getting document:", error);
            });
        }

        else {
            //var chat=[];
            docRef2.get().then((doc) => {
                if (doc.exists) {
                  //  console.log("Document data:", doc.data());
                    var obj = doc.data();
                    this.state.chatUsr_details.push(obj);
                  //  console.log("chat user details" + this.state.chatUsr_details);

                    var inbox_len = this.state.chatUser_inbox.length;
                    var archive_len = this.state.chatUser_archive.length;
                    var len = inbox_len + archive_len;

               //     console.log("total len" + len);
                //    console.log("chat len" + this.state.chatUsr_details.length);
                    if (this.state.chatUsr_details.length > len) {
                        this.setState({
                            isLoading: false,
                            refreshing: false,
                            inbox_show: true
                        });
                    }
                }
            }).catch((error) => {
                this.setState({
                    isLoading: false
                });
               // console.log("Error getting document:", error);
            });
        }

    }


    _stopLoading() {
       // console.log("chat user details" + this.state.chatUsr_details);

        var inbox_len = this.state.chatUser_inbox.length;
        var archive_len = this.state.chatUser_archive.length;
        var len = inbox_len + archive_len;
      //  console.log("total len" + len);
    //    console.log("chat len" + this.state.chatUsr_details.length);
        if (this.state.chatUsr_details.length == len) {
            this.setState({
                isLoading: false,
                refreshing: false,
                inbox_show: true
            });
        }
    }
    _getName(item) {
     //  console.log("item" + item.id);
        var name = this._bidName(item.id);
        // var business_name = this._bidderBusiness(item);
     //   console.log("get name" + name);
        return (
            <View style={{ marginLeft: 10 }}>
                <Text style={{ fontSize: 18, color: "black", fontWeight: "600" }}>{name}</Text>
                <Text style={{ fontSize: 14, color: "#827e7e", marginTop: 5, fontWeight: "600" }}>{item.message}</Text>
            </View>
        )
    }


    _bidName(id) {
      //  console.log("id" + id);
        var name = "";
        for (var i = 0; i < this.state.chatUsr_details.length; i++) {
       //     console.log("uid" + this.state.chatUsr_details[i].uid + "id" + id);
            if (this.state.chatUsr_details[i].uid.trim() == id) {
                name = this.state.chatUsr_details[i].firstname + " " + this.state.chatUsr_details[i].lastname;
            }
        }
      //  console.log("get name" + name);
        return name;
    }

    _getUserImage(item) {
        url = this._bidderImage(item.id);
        if (url == "" || url == undefined) {
            return (
                <TouchableOpacity>
                    <Image
                        source={require('../../assets/images/user.png')}
                        style={styles.avatorImage}
                    />
                </TouchableOpacity>
            )
        }
        else {
            return (
                <TouchableOpacity>
                    <CachedImage
                        source={{ uri: url }}
                        style={styles.avatorImage}
                    />
                </TouchableOpacity>
            )
        }

    }

    _bidderImage(id) {
        var url = "";
        for (var i = 0; i < this.state.chatUsr_details.length; i++) {
        //    console.log("uid" + this.state.chatUsr_details[i].uid);
            if (this.state.chatUsr_details[i].uid == id) {
                url = this.state.chatUsr_details[i].imageLink;
            }
        }
        return url;
    }

    _bidderBusiness(id) {

        // console.log("id"+id);
      //  console.log("details" + JSON.stringify(this.state.bidderDetails));
        var businessName = "";
        for (var i = 0; i < this.state.chatUsr_details.length; i++) {
       //     console.log("uid" + this.state.chatUsr_details[i].uid + "id" + id);
            if (this.state.chatUsr_details[i].uid == id) {
                if (this.state.chatUsr_details[i].BusinessInfo.CName != undefined) {
        //            console.log("get name" + this.state.chatUsr_details[i].BusinessInfo.CName);
                    businessName = this.state.chatUsr_details[i].BusinessInfo.CName;
                }

            }
        }
      //  console.log("get name" + businessName);
        return businessName;
    }


    _goToChat(item, status) {
        var name = this._bidName(item.id);
        // var business_name = this._bidderBusiness(item);
        var url = this._bidderImage(item.id);

        this.props.navigation.navigate("ProjectMessageScreen",
            {
                toChatId: item.id,
                fromChatId: this.state.uid,
                ProjectOwnerName: name,
                imageLink: url,
                navigateFrom: "message_listings",
                message_status: status,
                Uid: this.state.uid,
            });
    }


    _onRefresh = () => {
        this.setState({ refreshing: true });
        this._getMessages_refresh();
    }

    _getMessages_refresh() {
        //alert();
        var array_inbox = [];

        var inbox = db.collection("messages").doc(this.state.uid).collection("inbox");
        var archive = db.collection("messages").doc(this.state.uid).collection("archive");

        inbox.get().then((data) => {
            data.docs.map(doc => {
         //       console.log(doc.data());
                var obj_id = doc.id;
           //     console.log("obj_id" + doc.id);
                array_inbox.push(obj_id);
                //this.state.chatUser_inbox.push(obj_id);
                //console.log("inbox user" + this.state.chatUser_inbox);
            })
            this.setState({
                chatUser_inbox: array_inbox
            }, this._getInboxDetails.bind(this))


            //this.setState({ isLoading: false });
            //console.log("succss");
        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

        var array_archive = [];

        archive.get().then((data) => {
            //this.setState({ isLoading: true });
            data.docs.map(doc => {
           //     console.log(doc.data());
                var obj_id = doc.id;
           //     console.log("obj_id" + doc.id);
                //this.state.chatUser_archive.push(obj_id);
                array_archive.push(obj_id);
                //console.log("archive user" + this.state.chatUser_archive);
            })

            this.setState({
                chatUser_archive: array_archive,
                //chatUsr_details:[]
            }, this._getAcriveDetails.bind(this));


            // this.setState({ isLoading: false });
            //console.log("succss");
        }).catch((err) => {
            this.setState({ isLoading: false });
            console.log("err");
            console.log(err)
        })

    }

    // _getInboxDetails() {
    //     //alert("hi");
    //     for (var i = 0; i < this.state.chatUser_inbox.length; i++) {
    //         var docRef2 = db.collection("users").doc(this.state.chatUser_inbox[i]);
    //         this._getDetails(docRef2, "no");

    //     }
    // }
    // _getAcriveDetails() {
    //     //alert("hi");
    //     for (var i = 0; i < this.state.chatUser_archive.length; i++) {
    //         var docRef2 = db.collection("users").doc(this.state.chatUser_archive[i]);
    //         this._getDetails(docRef2, "no");
    //     }
    // }


    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#3A4958"
                    barStyle="light-content"
                />

                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.backButton} />
                    </TouchableOpacity>

                    <Text style={{ color: "#ffff", fontWeight: "600", fontSize: 20 }}>Messages</Text>

                    <View style={{ width: 30, height: 40 }} />
                </View>
                {(this.state.isLoading == true) ?
                    (
                        <View style={styles.activity_main}>
                            <View style={styles.activity_sub}>
                                <BubblesLoader size={50} dotRadius={5} color='black' />
                            </View>
                        </View>
                    )
                    :
                    (
                        <View>
                            <View style={{ backgroundColor: "#ffff" }}>
                                <View style={{
                                    borderBottomColor: "#bbc1c8",
                                    borderBottomWidth: 0.8,
                                    width: screenSize.width,
                                    alignSelf: "center",
                                    padding: 10, flexDirection: "row",
                                    borderRadius: 3,
                                    marginTop: 10,
                                    marginBottom: 10,
                                    height: 50
                                }}>
                                    <Feather name='search' color='#969da8' size={30} />
                                    <TextInput
                                        style={styles.textStyle}
                                        keyboardType='default'
                                        underlineColorAndroid="transparent"
                                        placeholder="Search"
                                        placeholderTextColor="#ababab"
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        onChangeText={(input) => this.setState({ message: input })}
                                        ref={((input) => this.inputMessage = input)}
                                    >
                                    </TextInput>
                                </View>
                            </View>
                            <View style={[styles.descriptionView, { marginTop: 10 }]}>
                                <SegmentedControls
                                    tint={'#3A4958'}
                                    selectedTint={'white'}
                                    backTint={'#fff'}
                                    options={options}
                                    allowFontScaling={false} // default: true
                                    onSelection={this.setSelectedOption.bind(this)}
                                    selectedOption={this.state.selectedOption}
                                    optionStyle={{ fontSize: 16 }}
                                    optionContainerStyle={{ flex: 1 }}
                                />
                            </View>

                            {/* <View style={styles.flatListView}>
                                    <View style={styles.messageView}>
                                        <Image
                                            source={require('../../assets/images/user.png')}
                                            style={styles.avatorImage}
                                        />
                                        <View style={{ marginLeft: 10 }}>
                                            <Text style={{ fontSize: 16, color: "black", fontWeight: "600" }}>Justin W </Text>
                                            <Text style={{ fontSize: 12, color: "#827e7e", marginTop: 5, fontWeight: "600" }}>LANDSCALLING-SERVICES</Text>
                                        </View>

                                        <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text>
                                    </View>
                                </View> */}

                            {(this.state.inbox_show == true) ?
                                (<FlatList
                                    data={this.state.chatUser_inbox}
                                    renderItem={({ item, index }) =>
                                        (
                                            <TouchableOpacity style={styles.flatListView}
                                                onPress={() => this._goToChat(item, "inbox")}
                                            >
                                                <View style={styles.messageView}>

                                                    {this._getUserImage(item)}
                                                    {this._getName(item)}

                                                    {/* <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text> */}
                                                </View>
                                            </TouchableOpacity>
                                        )}
                                    keyExtractor={item => item.id}
                                />)
                                :
                                null}
                            {(this.state.selectedOption == "Archive") ?
                                (<FlatList
                                    data={this.state.chatUser_archive}
                                    renderItem={({ item, index }) =>
                                        (
                                            <TouchableOpacity style={styles.flatListView}
                                                onPress={() => this._goToChat(item, "archive")}>
                                                <View style={styles.messageView}>

                                                    {this._getUserImage(item)}
                                                    {this._getName(item)}

                                                    {/* <Text style={{ fontSize: 12, color: '#a09a9a', marginTop: -20, marginLeft: 15 }}>12th jan 2018</Text> */}
                                                </View>
                                            </TouchableOpacity>
                                        )}
                                    keyExtractor={item => item.id}
                                />)
                                :
                                null}
                        </View>
                    )}
                <Footer itemColor='message'></Footer>
            </View>

        );
    }
}

const styles = {
    container: {
        flex: 1,
        //backgroundColor: '#fff'
    },

    textStyle: {
        width: screenSize.width - 60 - 10 - 20,
        marginLeft: 10,
        height: 30,
        padding: 0,
        margin: 0,
        flex: 1
    },
    descriptionView: {
        backgroundColor: "#ffff",
        elevation: 5,
        padding: 15,
        width: screenSize.width,
    },

    backButton: {
        fontSize: 30,
        color: '#ffff',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    messageView: {
        flexDirection: 'row',
        backgroundColor: "#ffff",
        width: screenSize.width,
        height: 80,
        paddingHorizontal: 10,
        alignItems: "center",
        marginBottom: 10,
        marginTop: 10
    },
    flatListView:
    {
        marginTop: 10,
        backgroundColor: "#e5e5ea"
    },
    headerContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 20 : 5,
        paddingHorizontal: 5,
        height: 60
    },
    activity_main: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 5,
        //elevation: 5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
    },

    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        //backgroundColor: '#ffff',
        //width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        //height: 50,
        //borderRadius: 10
    },

};

