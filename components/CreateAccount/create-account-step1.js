// Create account page 1
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/create-account.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert,
    BackHandler,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';
import Tooltip from 'react-native-walkthrough-tooltip';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import NumberFormat from 'react-number-format';
import { TextInputMask } from 'react-native-masked-text';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class CreateAccountStep1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedField: '',
            fName: "",
            lName: "",
            email: "",
            phone: "",
            password: "",
            conPassword: "",
            isLoading: false,
            toolTipVisible: false,
            uid: "",
            phoneNumberFormat: "",
            loginStatus: 'n',
        }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        //ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    gotoNextStep() {
        // this.props.navigation.navigate('CreateAccountStep2Screen');
        this._formValidation();
    }

    _formValidation() {

        if (this.state.fName === '' || this.state.fName.trim() === '') {
            CommonTasks._displayToast("Please Enter Your First Name");
            this.inputFname.focus();
            return false;
        }
        if (this.state.lName === '' || this.state.lName.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Last Name");
            this.inputLname.focus();
            return false;
        }

        if (this.state.email === '' || this.state.email.trim() === '') {
            CommonTasks._displayToast("Please Enter Your email id");
            this.inputemail.focus();
            return false;
        }
        else {
            var response = CommonTasks._verifyEmail(this.state.email);
            if (response === false) {
                CommonTasks._displayToast("Please enter valid email address");
                this.inputemail.focus();
                return false;
            }
        }

        if (this.state.phone === '' || this.state.phone.trim() === '') {
            CommonTasks._displayToast("Please Enter Your phone number");
            this.inputPhone.focus();
            return false;
        }

        if (this.state.password === '' || this.state.password.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Password");
            this.inputPassword.focus();
            return false;
        }

        if (this.state.conPassword.trim() != this.state.password.trim()) {
            CommonTasks._displayToast("Please Enter Same Password");
            this.inputConPassword.focus();
            return false;
        }
        var phone = this.state.phone;
        if (this.state.phone.includes("+")) {
            //alert(true);
        }
        else {
            //alert(false);
            if (phone.length == 10) {
                this.setState({
                    phone: "+1" + phone
                })
            }
            else if (phone.length == 11) {
                this.setState({
                    phone: "+" + phone
                })
            }

        }

        this.handleSignUp();
    }

    handleSignUp = () => {
        this.setState({ isLoading: true })
        firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((data) => {
                this.setState({ uid: data.user.uid });
                console.log(JSON.stringify(data));
                console.log(data.user.uid);
                db.collection("users").doc(data.user.uid).set({
                    firstname: this.state.fName,
                    lastname: this.state.lName,
                    email: this.state.email,
                    phone: this.state.phone,
                    imageLink: "",
                    //password:this.state.password,
                    uid: data.user.uid,
                    createdAt: new Date(),
                    phVerify_status: 'N',
                    SubcriptionType: "Free",
                    userType:'PRO'
                }).then((user) => {
                    this.setState({ isLoading: false });
                    console.log("user data" + user);
                    CommonTasks._displayToast("You have successfully registerd to Pro In Your Pocket for Pros");
                    this.props.navigation.replace('CreateAccountStep2Screen', { phone: this.state.phone, uid: this.state.uid,phoneNumberFormat:this.state.phoneNumberFormat ,loginStatus: this.state.loginStatus});
                }).catch((error) => {
                    this.setState({ isLoading: false });
                    console.log("user error" + error)
                })
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log(error);
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode + "-" + errorMessage);
                CommonTasks._displayToast(errorMessage);
            })
    }

  

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer} onPress={() => this.props.navigation.navigate('SigninWithEmailScreen')}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Sign In</Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Create Account</Text>
                    <Text style={styles.nextText} onPress={() => this.gotoNextStep()}>Next</Text>
                </View>

                <ScrollView keyboardShouldPersistTaps="always">
                    <View style={styles.inputContainer}>
                        <View style={styles.textboxContainer}>
                            <TextInput
                                style={styles.textInput}
                                keyboardType='default'
                                underlineColorAndroid="transparent"
                                placeholder="First Name"
                                placeholderTextColor="#ababab"
                                returnKeyType='next'
                                autoCorrect={false}
                                autoCapitalize="words"
                                onSubmitEditing={() => this.inputLname.focus()}
                                onChangeText={(input) => this.setState({ fName: input })}
                                ref={((input) => this.inputFname = input)}
                                onFocus={() => this.setState({ selectedField: 'fname' })}
                            />
                            {this.state.selectedField == 'fname' ? (
                                <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => {
                                    this.inputFname.clear()
                                    this.setState({ fName: "" })
                                }} />
                            ) : null
                            }
                        </View>
                        <View style={styles.textboxContainer}>
                            <TextInput
                                style={styles.textInput}
                                keyboardType='default'
                                underlineColorAndroid="transparent"
                                placeholder="Last Name"
                                placeholderTextColor="#ababab"
                                returnKeyType='next'
                                autoCorrect={false}
                                autoCapitalize="words"
                                onSubmitEditing={() => this.inputPhone.focus()}
                                onChangeText={(input) => this.setState({ lName: input })}
                                ref={((input) => this.inputLname = input)}
                                onFocus={() => this.setState({ selectedField: 'lname' })}
                            />
                            {this.state.selectedField == 'lname' ? (
                                <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => {
                                    this.inputLname.clear();
                                    this.setState({ lName: "" });
                                }} />
                            ) : null
                            }
                        </View>

                        <View style={styles.textboxContainer}>
                            <TextInput
                                style={styles.textInput}
                                keyboardType='default'
                                underlineColorAndroid="transparent"
                                placeholder="Email"
                                placeholderTextColor="#ababab"
                                returnKeyType='next'
                                autoCorrect={false}
                                autoCapitalize="none"
                                onSubmitEditing={() => this.inputPassword.focus()}
                                onChangeText={(input) => this.setState({ email: input })}
                                ref={((input) => this.inputemail = input)}
                                onFocus={() => this.setState({ selectedField: 'email' })}
                            />
                            {this.state.selectedField == 'email' ? (
                                <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => {
                                    this.inputemail.clear();
                                    this.setState({ email: "" });
                                }}

                                />
                            ) : null
                            }
                        </View>

                       


                        <View style={styles.textboxContainer}>
                            <TextInputMask
                                placeholder="Phone number"
                                returnKeyType='next'
                                onSubmitEditing={() => this.inputPassword.focus()}
                                value={this.state.phoneNumberFormat}
                                refInput={ref => (this.inputPhone = ref)}
                                onChangeText={(phoneNumberFormat) => {
                                    let phoneNumber = phoneNumberFormat.toString().replace(/\D+/g, '');
                                    this.setState({ phoneNumberFormat: phoneNumberFormat, phone: phoneNumber })
                                }}
                                onFocus={() => this.setState({ selectedField: 'phone' })}
                                type={'cel-phone'}
                                maxLength={this.state.phoneNumberFormat.toString().startsWith("1") ? 18 : 16}
                                options={
                                    this.state.phone.startsWith("1") ?
                                        {
                                            dddMask: '9 (999) 999 - '
                                        } : {
                                            dddMask: '(999) 999 - '
                                        }
                                }
                                style={styles.textInput}
                            />
                            {this.state.selectedField == 'phone' ? (
                                <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => {
                                    this.inputPhone.clear();
                                    this.setState({ phone: "", phoneNumberFormat: "" });
                                }} />
                            ) : null
                            }
                        </View>


                        <View style={styles.textboxContainer}>
                            <TextInput
                                style={styles.textInput}
                                secureTextEntry={true}
                                keyboardType='default'
                                underlineColorAndroid="transparent"
                                placeholder="Password"
                                placeholderTextColor="#ababab"
                                returnKeyType='next'
                                autoCorrect={false}
                                autoCapitalize="none"
                                onSubmitEditing={() => this.inputConPassword.focus()}
                                onChangeText={(input) => this.setState({ password: input })}
                                ref={((input) => this.inputPassword = input)}
                                onFocus={() => this.setState({ selectedField: 'password' })}
                            />
                            {this.state.selectedField == 'password' ? (
                                <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => {
                                    this.inputPassword.clear();
                                    this.setState({ password: "" });
                                }}
                                />
                            ) : null
                            }
                        </View>
                        <View style={styles.textboxContainer}>
                            <TextInput
                                style={styles.textInput}
                                secureTextEntry={true}
                                keyboardType='default'
                                underlineColorAndroid="transparent"
                                placeholder="Confirm password"
                                placeholderTextColor="#ababab"
                                returnKeyType='done'
                                autoCorrect={false}
                                autoCapitalize="none"
                                // onSubmitEditing={() => this.inputPassword.focus()}
                                onChangeText={(input) => this.setState({ conPassword: input })}
                                ref={((input) => this.inputConPassword = input)}
                                onFocus={() => this.setState({ selectedField: 'conpassword' })}
                            />
                            {this.state.selectedField == 'conpassword' ? (
                                <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => {
                                    this.inputConPassword.clear();
                                    this.setState({ conPassword: "" });
                                }} />
                            ) : null
                            }
                        </View>
                    </View>
                    <KeyboardSpacer />
                </ScrollView>
                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>
                    )
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff'
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // borderBottomColor: '#8e8e93',
        // borderBottomWidth: 0.7,
        // paddingLeft : 15,
        // marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17,
        flex: 1
    },

    inputContainer: {
        backgroundColor: 'white',
        marginTop: 20
    },

    textboxContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        marginLeft: 15,
    },

    cancelIcon: {
        fontSize: 20,
        marginRight: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})