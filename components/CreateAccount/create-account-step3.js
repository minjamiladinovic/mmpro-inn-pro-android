// Create account page 3
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/business-info.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    BackHandler,
} from 'react-native';

import { Picker, Item } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { TextInputMask } from 'react-native-masked-text';
import states from "../../assets/files/states";

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class CreateAccountStep3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            businessType: false,
            individualType: false,
            businessText: "",
            type_business: "",
            selectedState: '',
            selectedCity: '',
            uid: this.props.navigation.state.params.uid,
            loginStatus: this.props.navigation.state.params.loginStatus,
            companyName: "",
            myPhone: "",
            myEmail: "",
            companyAddress1: "",
            companyAddress2: "",
            city: "",
            postcode: "",
            phoneNumberFormat: "",
            business: {
                "BusinessType": "Individual",
                "CName": "Techno",
                "CPhone": "8013341607",
                "CEmail": "sk@yopmail.com",
                "CAddress1": "Kolkata",
                "CAddress2": "",
                "CCity": "Kol",
                "CState": "Albama",
                "CPin": "700125"
            },
            isLoading: false,
            cities: [],
            zipcodeCity: [],
            zipcodeState: [],
            showCity: false
        }
    }

    componentDidMount() {
        console.log("STATES" + states);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        // ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }
    _businessCheck() {
        if (this.state.businessType == false) {
            this.setState({
                businessText: "Business",
                businessType: true,
                individualType: false
            })
        }
    }

    _indCheck() {
        if (this.state.individualType == false) {
            this.setState({
                businessText: "Individual",
                businessType: false,
                individualType: true
            })
        }
    }

    onValueChange(value) {
        this.setState({
            selectedState: value,
            showCity: true,
            //selectedCity:this.state.cities[0].city
        });
        for (var i = 0; i < states.length; i++) {
            if (states[i].name.trim() == value.trim()) {
                var ctites_zip = [];
                for (var key in states[i].cities) {
                    if (states[i].cities.hasOwnProperty(key)) {
                        var obj = {};
                        obj.city = key;
                        obj.zipcode = states[i].cities[key];
                        ctites_zip.push(obj);
                    }
                }
                this.setState({
                    cities: ctites_zip,
                    // zipcodes:zipcodes
                })

                var zipCode = [];
                for (var i = 0; i < ctites_zip.length; i++) {
                    var obj = {};
                    obj.zipCode = ctites_zip[i].zipcode[0];
                    obj.show = false;
                    zipCode.push(obj)
                }

                this.setState({ zipcodeState: zipCode })
            }
        }

    }

    onValueChangeCity(value) {
        console.log("zip" + this.state.zipcodeState);

        this.setState({
            selectedCity: value
        });
        var tempArr = [];
        for (var i = 0; i < this.state.cities.length; i++) {
            if (this.state.cities[i].city.trim() == value.trim()) {
                this.state.cities[i].zipcode.forEach(element => {
                    let tempObj = {};
                    tempObj.zipCode = element;
                    tempObj.show = true;
                    tempArr.push(tempObj);
                });
                this.setState({ zipcodeCity: this.state.cities[i].zipcode })
            }
        }
        this.setState({ zipcodeState: tempArr })
        console.log(this.state.zipcodeCity, this.state.zipcodeState)
    }

    gotoNextStep() {
        this._formValidation();
    }

    _formValidation() {

        if (this.state.businessText === "" || this.state.businessText.trim() === " ") {
            CommonTasks._displayToast("Please Choose your Business Type");
            return false;
        }
        if (this.state.companyName === '' || this.state.companyName.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company Name");
            this.inputComName.focus();
            return false;
        }
        if (this.state.myPhone === '' || this.state.myPhone.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's Contact Number");
            this.inputComPhone.focus();
            return false;
        }
        if (this.state.myEmail === '' || this.state.myEmail.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's Email");
            this.inputComEmail.focus();
            return false;
        }
        if (this.state.companyAddress1 === '' || this.state.companyAddress1.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Company's Address");
            this.inputComAddr1.focus();
            return false;
        }

        // if (this.state.selectedState === '' || this.state.selectedState.trim() === 'Select State') {
        //     CommonTasks._displayToast("Please Enter Your Company's State");
        //     return false;
        // }

        // if (this.state.selectedCity === '' || this.state.selectedCity.trim() === 'Select City') {
        //     CommonTasks._displayToast("Please Enter Your Company's City");
        //     //this.inputCity.focus();
        //     return false;
        // }

        this._addBusiness();

    }

    _addBusiness() {
        this.setState({ isLoading: true });
        console.log(this.state.zipcodeState);
        db.collection("users").doc(this.state.uid).set({
            BusinessInfo:
            {
                "BusinessType": this.state.businessText,
                "CName": this.state.companyName,
                "CPhone": this.state.myPhone,
                "CEmail": this.state.myEmail,
                "CAddress1": this.state.companyAddress1,
                "CAddress2": this.state.companyAddress2,
                // "CCity": this.state.selectedCity,
                // "CState": this.state.selectedState,
                // "cityList": this.state.cities,
                // "CPin": this.state.zipcodeCity,
                // "StateZip": this.state.zipcodeState,
                // "SelectedZip": this.state.zipcodeCity
            }
        }, { merge: true }).then((user) => {
            this.setState({ isLoading: false });
            console.log("user data" + user);
            // this.props.navigation.replace('CreateAccountStep4Screen', { uid: this.state.uid, loginStatus: this.state.loginStatus });
            this.props.navigation.replace('BusinessServiceAreaAdd', { uid: this.state.uid, loginStatus: this.state.loginStatus });
        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
            var errorCode = err.code;
            var errorMessage = err.message;
            console.log(errorCode + "-" + errorMessage);
            CommonTasks._displayToast(errorMessage);
        })
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.navigate('SigninWithEmailScreen')}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Sign Out</Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Business Info</Text>
                    <Text style={styles.nextText}
                        onPress={() => this.gotoNextStep()}
                    >Next</Text>
                </View>
                <ScrollView>
                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={styles.businessText}>BUSINESS TYPE</Text>
                    </View>

                    <View style={styles.businessTypeContainer}>
                        <TouchableOpacity style={styles.infoText} onPress={() => this._businessCheck()}>
                            <Text style={styles.optionText}>Business</Text>
                            {(this.state.businessType == true) ?
                                (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                :
                                null}
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.infoText} onPress={() => this._indCheck()}>
                            <Text style={styles.optionText}>Individual</Text>
                            {(this.state.individualType == true) ?
                                (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                :
                                null}
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={styles.businessText}>BUSINESS CONTACT</Text>
                    </View>

                    <View style={styles.verificationContainer}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="My Company"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="words"
                            onSubmitEditing={() => this.inputComPhone.focus()}
                            onChangeText={(input) => this.setState({ companyName: input })}
                            ref={((input) => this.inputComName = input)}
                        />

                       

                        <TextInputMask
                            placeholder="Phone number"
                            returnKeyType='next'
                            value={this.state.phoneNumberFormat}
                            onSubmitEditing={() => this.inputComEmail.focus()}
                            refInput={((input) => this.inputComPhone = input)}
                            onChangeText={(phoneNumberFormat) => {
                                let phoneNumber = phoneNumberFormat.toString().replace(/\D+/g, '');
                                this.setState({ phoneNumberFormat: phoneNumberFormat, myPhone: phoneNumber })
                            }}
                            type={'cel-phone'}
                            maxLength={this.state.phoneNumberFormat.toString().startsWith("1") ? 18 : 16}
                            options={
                                this.state.myPhone.startsWith("1") ?
                                    {
                                        dddMask: '9 (999) 999 - '
                                    } :
                                    {
                                        dddMask: '(999) 999 - '
                                    }
                            }
                            style={styles.textInput}
                        />
                        <TextInput
                            style={styles.textInput}
                            keyboardType='email-address'
                            underlineColorAndroid="transparent"
                            placeholder="My Email"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.inputComAddr1.focus()}
                            onChangeText={(input) => this.setState({ myEmail: input })}
                            ref={((input) => this.inputComEmail = input)}
                        />
                    </View>

                    <View style={{ marginTop: 20, marginLeft: 10 }}>
                        <Text style={styles.businessText}>BUSINESS ADDRESS</Text>
                    </View>

                    <View style={styles.verificationContainer}>

                        {/* <View style={styles.pickerStyle}>
                            <Picker
                                mode="dropdown"
                                iosHeader="States"
                                style={{ width: screenSize.width - 15, }}
                                selectedValue={this.state.selectedState}
                                onValueChange={this.onValueChange.bind(this)}
                            >
                                {
                                    states.map((s, key) => <Picker.Item key={s.name} value={s.name} label={s.name} />)
                                }
                            </Picker>
                        </View>

                        {(this.state.showCity == true) ?
                            (<View style={styles.pickerStyle}>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="States"
                                    style={{ width: screenSize.width - 15, }}
                                    selectedValue={this.state.selectedCity}
                                    onValueChange={this.onValueChangeCity.bind(this)}
                                >
                                    {
                                        //CommonTasks.stateList.map((s, key) => <Picker.Item key={s.abbreviation} value={s.name} label={s.name} />)

                                        this.state.cities.map((s, key) => <Picker.Item key={s.city} value={s.city} label={s.city} />)
                                    }
                                </Picker>
                            </View>)
                            :
                            null} */}
                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="line 1"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.inputComAddr2.focus()}
                            onChangeText={(input) => this.setState({ companyAddress1: input })}
                            ref={((input) => this.inputComAddr1 = input)}
                        />

                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="line 2"
                            placeholderTextColor="#ababab"
                            //returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            // onSubmitEditing={() => this.inputCity.focus()}
                            onChangeText={(input) => this.setState({ companyAddress2: input })}
                            ref={((input) => this.inputComAddr2 = input)}
                        />

                        
                    </View>
                    <KeyboardSpacer />
                </ScrollView>

                {(this.state.isLoading == true) ?
                    (
                        <View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                            <View style={styles.activity_sub}>
                                <ActivityIndicator
                                    size="large"
                                    color='#D0D3D4'
                                    style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                                />
                            </View>
                        </View>
                    )
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 15
    },

    businessText: {
        fontFamily: regularText,
        fontSize: 16,
        marginBottom: 6
    },

    businessTypeContainer: {
        backgroundColor: 'white'
    },

    infoText: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 45
    },

    optionText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#1e1e1e'
    },

    optionImage: {
        fontSize: 34,
        color: '#007aff',
        marginRight: 14
    },

    verificationContainer: {
        backgroundColor: 'white',
        // marginVertical: 20
    },

    textInput: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17
    },

    pickerStyle: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        marginLeft: 13,
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})