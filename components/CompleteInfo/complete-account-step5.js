// Create account page 2
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/verify-phone-number.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    Keyboard,
    StatusBar,
    FlatList,
    Alert,
    BackHandler,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import Feather from 'react-native-vector-icons/Feather';
import DeviceInfo from 'react-native-device-info';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();



export default class CompleteAccountStep5 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            summary: '',
            service: [],
            isLoading: true,
            serviceValue: [],
            uid: this.props.navigation.state.params.uid,
            // loginStatus : this.props.navigation.state.params.loginStatus,
            fcm_Token: '',
            searchText: '',
            searchArray: [],
            cancel: false,
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
         AsyncStorage.getItem('fcmToken').then(value => this.setState({ fcm_Token: value }));
        var serviceData = CommonTasks.serviceList;
        var service = [];


        for (var data of serviceData) {
            data.show = false;
            service.push(data);
        }
        this.setState({
            service: service
        }, this._stopLoading.bind(this))
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        //ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }
    _stopLoading() {
        this.setState({
            isLoading: false
        })
        console.log(this.state.service);
    }

    _checkService(index, name, id) {
        var newArray = this.state.service.slice();
        newArray[index] = {
            name: newArray[index].name,
            id: newArray[index].id,
            show: newArray[index].show == false ? true : false,
        };
        console.log("new data array" + newArray);
        this.setState({
            service: newArray,
        })
        var obj = {}
        obj.name = name;
        obj.id = id;

        var serviceCheck = false;

        this.state.serviceValue.forEach(function (item) {

            if (item.id == id) {
                serviceCheck = true;
            }

        })

        if (serviceCheck == true) {
            var array = this.state.serviceValue;
            array.forEach(function (element, i, object) {
                console.log("service element" + element);
                if (element.id == id) {
                    //alert("del");
                    object.splice(i, 1);
                }
            });
            this.setState({
                serviceValue: array
            })
        }
        else if (serviceCheck == false) {
            this.state.serviceValue.push(obj);
        }
    }

    _navigateProjectSearchScreen() {
        AsyncStorage.setItem('user_id', this.state.uid);
        AsyncStorage.setItem('user_status', "1");
        AsyncStorage.setItem('loginStatus', 'f');
        this.props.navigation.replace('ProjectSearchScreen', { uid: this.state.uid })
    }


    async  _saveTokenFirestore(uid) {
        await firebase.firestore().collection("sp_tokens").doc(this.state.fcm_Token).set({
            uid: uid,
            fcm_token: this.state.fcm_Token,
            device_id: DeviceInfo.getUniqueID(),
            device_brand: DeviceInfo.getBrand(),
            deviceModel: DeviceInfo.getModel()
        }).then((user) => {
            console.log("token  data written successfully");

        }).catch((error) => {
            console.log("Error writing token  data ")
        })
    }


    _navigationAccountComplete() {
        if (this.state.loginStatus == 'f') {
            this._navigateProjectSearchScreen()
            this._saveTokenFirestore(this.state.uid)
        }
        else
            this.props.navigation.replace('SigninWithEmailScreen')

    }

    gotoNextStep() {
        if(this.state.serviceValue.length==0)
        {
            CommonTasks._displayToast("Please Select Your Services");
            return false;
        }
        else {
            this.setState({ isLoading: true });
            db.collection("users").doc(this.state.uid).set({
                ServiceType: this.state.serviceValue
            }, { merge: true }).then((user) => {
                this.setState({ isLoading: false });
                console.log("user data" + user);
                CommonTasks._displayToast("Account updated Sucessfully");
                this.props.navigation.replace('ProjectSearchScreen', { uid: this.state.uid })
                //this.props.navigation.pop();
            }).catch((error) => {
                this.setState({ isLoading: false });
                console.log("user error" + error);
                var errorCode = err.code;
                var errorMessage = err.message;
                console.log(errorCode + "-" + errorMessage);
                CommonTasks._displayToast(errorMessage);
            })
        }


        //this.props.navigation.navigate('CreateAccountStep5Screen');

    }


    _searchServices(text) {
        //console.log(text==''?this.setState({cancel : false}): this.setState({cancel : true}))
        this.setState({searchText : text})
        var s=this.state.service.slice();
        var searchResult=[];
        var i=0;
        if(text!='')
        {
          s.forEach((data) =>
        {
            if(data.name.toLowerCase().indexOf(text.toLowerCase())!=-1 && i<5)
            {
                searchResult.push(data);
                i++;
            }
            if(i==4)
            {
                this.setState({searchArray: searchResult})
            }
        })  
        }
        
        this.setState({searchArray: searchResult})
    }

    checkServices() {
        if(this.state.serviceValue.length==0)
        {
            CommonTasks._displayToast("Atleast select one of the services");
        }
        else
        {
            this.gotoNextStep()
        }
    }


    _doneSearch() {
        this.setState({ searchText: '', searchArray: [] })
        Keyboard.dismiss();
    }



    clearText() {
        //this.inputMessage.setNativeProps({text: ''});
        this.setState({ searchText: '', searchArray: [] })
        Keyboard.dismiss();
    }



    _checkService1(name, id) {
        var index;
        var newArray = this.state.service.slice();
        var newArray1 = []
        newArray.forEach((data) => {
            if (data.name == name) {
                var obj = data
                obj.show = data.show ? false : true;
                newArray1.push(obj)
            }
            else {
                var obj = data

                newArray1.push(obj)
            }
        })
        // newArray[index] = {
        //     name: newArray[index].name,
        //     show: newArray[index].show == false ? true : false,
        // };
        //console.log("new data array" + newArray);
        this.setState({
            service: newArray1,
        })
        var obj = {}
        obj.name = name;
        obj.id = id;
        var serviceCheck = false;

        this.state.serviceValue.forEach(function (item) {

            if (item.id == id) {
                serviceCheck = true;
            }

        })

        if (serviceCheck == true) {
            var array = this.state.serviceValue;
            array.forEach(function (element, i, object) {
                console.log("service element" + element);
                if (element.id == id) {
                    //alert("del");
                    object.splice(i, 1);
                }
            });
            this.setState({
                serviceValue: array
            })
        }
        else if (serviceCheck == false) {
            this.state.serviceValue.push(obj);
        }


        //   if (this.state.serviceValue.indexOf(name) === -1) {
        //       //this.state.serviceValue.push(name);
        //       this.state.serviceValue.push(obj);
        //   }
        //   else {
        //       var array = this.state.serviceValue;
        //       array.forEach(function (element, i, object) {
        //           console.log(element);
        //           if (element.name.trim() == name.trim()) {
        //               //alert("del");
        //               object.splice(i, 1);
        //           }
        //       });
        //       this.setState({
        //           serviceValue: array
        //       })
        //   }
        this._doneSearch();
        //  console.log("service" + this.state.serviceValue);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.replace('SigninWithEmailScreen')}>
                        <Text style={styles.signinText}>Sign Out</Text>
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Services</Text>
                    <Text style={styles.nextText} onPress={() => this.gotoNextStep()}>Next</Text>
                </View>
                <View style={{flexDirection :  'row',alignItems : 'center',marginVertical : 3}}>

                    <TextInput style = {{borderBottomWidth : 0.5,width : screenSize.width-50 ,padding : -10,paddingLeft : 15}}
                        underlineColorAndroid='rgba(200, 200, 200, 0)'
                        onChangeText={(text) => this._searchServices(text)}
                        keyboardType='default'
                        value={this.state.searchText}
                        placeholder="Search"
                        placeholderTextColor="#ababab"
                        autoCorrect={false}
                        autoCapitalize="none"
                        autoComplete='off'
                        ref={((input) => this.inputMessage = input)}>
                    </TextInput>
                    {(this.state.searchText=='')?
                     <TouchableOpacity>
                        <Feather name='search' size={23 } style={{padding : 4,paddingLeft : 10,}} ></Feather>
                     </TouchableOpacity>
                     :
                     <TouchableOpacity onPress = {() => this.clearText()}>
                        <MaterialIcons name='cancel' size={23 } style={{padding : 4,paddingLeft : 10,}} ></MaterialIcons>
                     </TouchableOpacity>
                    }
                    
                </View>


                {(this.state.searchArray.length)?
                (<View style={{width : screenSize.width, backgroundColor : '#fff',maxHeight : screenSize.height-120,backgroundColor : '#fff', paddingHorizontal : 10,elevation : 3,paddingBottom : 40 }}>
                    <FlatList
                        keyboardShouldPersistTaps={true}
                        data={this.state.searchArray}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity style={styles.itemContainer}
                                onPress={() => this._checkService1(item.name,item.id)}>
                                <Text style={styles.itemText}>{item.name}</Text>
                                {(item.show == true) ?
                                    (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                    :
                                    null}
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item.name}
                    />
                    
                    
                </View>)
                :
                null}


                {(this.state.searchArray.length == 0)?
                (<View style={styles.verificationContainer}>
                    <FlatList
                        data={this.state.service}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity style={styles.itemContainer}
                                onPress={() => this._checkService(index, item.name,item.id)}>
                                <Text style={styles.itemText}>{item.name}</Text>
                                {(item.show == true) ?
                                    (<Ionicons name="ios-checkmark" style={styles.optionImage} />)
                                    :
                                    null}
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item.name}
                    />
                </View>)
                :
                null}
                {(this.state.isLoading == true) ?
                    (<View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                    <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>
                    </View>
                    )
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17,
        textAlignVertical: 'top',
        height: 100
    },

    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 0,
        elevation: 1,
        maxHeight : screenSize.height-102
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginHorizontal: 15
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },

    itemContainer: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        // paddingLeft : 15,
        marginLeft: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    itemText: {
        fontFamily: regularText,
        fontSize: 18,
        paddingVertical: 7
    },

    optionImage: {
        fontSize: 34,
        color: '#007aff',
        marginRight: 14
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});