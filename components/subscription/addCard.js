import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    FlatList,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import { TextInputMask } from 'react-native-masked-text';
import CommonTasks from '../../common-tasks/common-tasks';
import Stripe from 'react-native-stripe-api';


const regularText = "SF-Pro-Display-Regular";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();


export default class AddCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: "",
            isLoading: false,
            isLoading1: false,
            Data: [],
            notificationData: [],
            notificationDataLength: 0,
            phoneNumberFormat: '',
            phone: '',
            verfication_statement: false,
            selectedField: '',
            CardHolderName: '',
            cvv: '',
            MM: '',
            YY: '',
            projectId: this.props.navigation.state.params.projectId,
            owner: this.props.navigation.state.params.ownerId,
            bidderId: this.props.navigation.state.params.bidderId,
            description: this.props.navigation.state.params.description,
            serviceId: this.props.navigation.state.params.serviceId,
            createdAt: this.props.navigation.state.params.createdAt,
            bidderIdList: this.props.navigation.state.params.bidderIdList ? this.props.navigation.state.params.bidderIdList : [],
            imageLink: this.props.navigation.state.params.imageLink,
            ownerName: this.props.navigation.state.params.ownerName,
            postalCode: this.props.navigation.state.params.postalCode,
            timeString: this.props.navigation.state.params.timeString,
            Pimage: this.props.navigation.state.params.Pimage,
            navigateTo: this.props.navigation.state.params.navigateTo,
            projectStatus:this.props.navigation.state.params.projectStatus,
            bidPrice:this.props.navigation.state.params.bidPrice,
            oldPrice:this.props.navigation.state.params.oldPrice,
            SubcriptionStatus:this.props.navigation.state.params.SubcriptionStatus,
            cardArr: [],
            viewCard: false

        }
    }

    componentDidMount() {
        this._getCard()
    }

    _getCard() {
        this.setState({ isLoading1: true });
        AsyncStorage.getItem('user_id').then(value => {
            var docRef = db.collection("cardDetails").doc(value);
            docRef.get().then((doc) => {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    this.setState({
                        cardArr: doc.data().card,
                        isLoading1: false,
                    })
                }
                else
                {
                    this.setState({ isLoading1: false,viewCard:true });
                }
            }).catch((eror) => {
                this.setState({ isLoading1: false,viewCard:true });
                console.log("Error getting document:", error);
            });
        }


        );



    }

    componentWillMount() {
        AsyncStorage.getItem('user_id').then(value => this.setState({ uid: value }));
    }

    _stopLoading() {
        //console.log('details'+JSON.stringify(this.state.pendingDataMod))
        this.setState({ isLoading: false });
    }

    setvalue(CardHolderName) {
        const upper = CardHolderName.toUpperCase();
        this.setState({ CardHolderName: upper });
    }


    async  _payNow() {
        // const apiKey = 'pk_test_qYKaD4TLqQdNtStg3FZ237uL';
        const apiKey = "pk_live_YNqLZAs0t3ofJf698pnNFKUT";

        const client = new Stripe(apiKey);

        this.setState({ CardHolderName: this.state.CardHolderName.toUpperCase() })
        if (this.state.MM.length == 1) {
            this.setState({ MM: '0' + this.state.MM })
        }
        if (this.state.phone.length == 0) {
            CommonTasks._displayToast("Please Enter a Card Number");
        }
        else if (this.state.phone.length != 16) {
            CommonTasks._displayToast("Please Enter a correct Card Number");
        }
        else if (this.state.cvv.length == 0) {
            CommonTasks._displayToast("Please Enter CVV Code");
        }
        else if (this.state.cvv.length != 3) {
            CommonTasks._displayToast("Please Enter a correct CVV Code");
        }
        else if (this.state.CardHolderName.length == 0) {
            CommonTasks._displayToast("Please Enter Card Holder Name");
        }
        else if (this.state.MM > 12) {
            CommonTasks._displayToast("Please Enter Correct Expiry Month");
        }
        else if (this.state.YY.length == 0) {
            CommonTasks._displayToast("Please Enter  Expiry Year");
        }
        else if (this.state.YY < 19) {
            CommonTasks._displayToast("Please Enter Correct Expiry Year");
        }
        else {
            this.setState({ isLoading: true });
            console.log('no: ' + this.state.phone +
                'cvv: ' + this.state.cvv +
                'name: ' + this.state.CardHolderName +
                'mm: ' + this.state.MM +
                'yy: ' + this.state.YY)
            // Create a Stripe token with new card infos
            client.createToken({ number: String(this.state.phone), exp_month: String(this.state.MM), exp_year: String(this.state.YY), cvc: String(this.state.cvv) })
                .then((token) => {
                    //console.log("date"+Date.today().next().month());
                    // alert(token.error);
                    if (token.error == undefined) {
                        console.log("token" + JSON.stringify(token));
                        var transaction = db.collection("paymentsHistory").doc(this.state.uid);
                        transaction.delete().then(response => {
                            var payment = db.collection("payments").doc(this.state.uid).collection("transactionHistory").doc();
                            payment.set({
                                token: token,
                                uid: this.state.uid,
                                amount: 24,
                                currency: "usd"
                            }).then((response) => {
                                // alert("done");
                                setTimeout(() => {
                                    this.setState({
                                        isLoading: false
                                    }, this._checkPayment())
                                }, 20000);
                            }).catch((error) => {
                                console.log("user error" + error)
                            });

                        })
                            .catch((err) => {
                                console.log(err)
                                alert('create token err')
                            })
                    }
                    else {
                        //CommonTasks._displayToast(token.error.code);
                        alert(token.error.code);
                    }
                })
        }
    }




    async  _payNowNew() {
        //const apiKey = 'pk_test_qYKaD4TLqQdNtStg3FZ237uL';
        const apiKey = "pk_live_YNqLZAs0t3ofJf698pnNFKUT";

        const client = new Stripe(apiKey);

        this.setState({ CardHolderName: this.state.CardHolderName.toUpperCase() })
        if (this.state.MM.length == 1) {
            this.setState({ MM: '0' + this.state.MM })
        }
        if (this.state.phone.length == 0) {
            CommonTasks._displayToast("Please Enter a Card Number");
        }
        else if (this.state.phone.length != 16) {
            CommonTasks._displayToast("Please Enter a correct Card Number");
        }
        else if (this.state.cvv.length == 0) {
            CommonTasks._displayToast("Please Enter CVV Code");
        }
        else if (this.state.cvv.length != 3) {
            CommonTasks._displayToast("Please Enter a correct CVV Code");
        }
        else if (this.state.CardHolderName.length == 0) {
            CommonTasks._displayToast("Please Enter Card Holder Name");
        }
        else if (this.state.MM > 12) {
            CommonTasks._displayToast("Please Enter Correct Expiry Month");
        }
        else if (this.state.YY.length == 0) {
            CommonTasks._displayToast("Please Enter  Expiry Year");
        }
        else if (this.state.YY < 19) {
            CommonTasks._displayToast("Please Enter Correct Expiry Year");
        }
        else {
            this.setState({ isLoading: true });
            console.log('no: ' + this.state.phone +
                'cvv: ' + this.state.cvv +
                'name: ' + this.state.CardHolderName +
                'mm: ' + this.state.MM +
                'yy: ' + this.state.YY)
            // Create a Stripe token with new card infos
            client.createToken({ number: String(this.state.phone), exp_month: String(this.state.MM), exp_year: String(this.state.YY), cvc: String(this.state.cvv) })
                .then((token) => {
                    //console.log("date"+Date.today().next().month());
                    //alert(JSON.stringify(token))
                    // alert(token.error);
                    if (token.error == undefined) {
                        console.log("token" + JSON.stringify(token));
                        this.setState({ isLoading: true });
                        var cardDetails = [];
                        var obj = { "token": token };
                        cardDetails.push(obj);
                        var transaction = db.collection("cardDetails").doc(this.state.uid);
                        transaction
                            .set({
                                card: cardDetails
                            })
                            .then((response) => {
                                if(this.state.cardArr.length==0)
                                {
                                    this._stripeMonthly()
                                }
                                else
                                {
                                    this._stripeReMonthly()
                                }
                                

                            })
                    }
                    else {
                        //CommonTasks._displayToast(token.error.code);
                        alert(token.error.code);
                    }
                })
        }
    }


    async _stripeMonthly() {
        return fetch('https://us-central1-pro-in-your-pocket.cloudfunctions.net/monthlySubscriptions?docId=' + this.state.uid,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            })
            .then(response => response.json())
            .then(json => {
                console.log(json);
                //alert(JSON.stringify(json));
                if (json.status == "success") {
                    this.setState({ isLoading: false });
                    AsyncStorage.setItem("SubcriptionType", "paid");
                    //alert(this.state.navigateTo);
                    if (this.state.navigateTo.trim() === "place-bid") {
                        this.props.navigation.replace('ProjectPlaceBidScreen',
                            {
                                projectId: this.state.projectId,
                                ownerId: this.state.ownerId,
                                description: this.state.description,
                                serviceId: this.state.serviceId,
                                createdAt: this.state.createdAt,
                                bidderIdList: this.state.bidderIdList,
                                imageLink: this.state.imageLink,
                                ownerName: this.state.ownerName,
                                postalCode: this.state.postalCode,
                                timeString: this.state.timeString,
                                Pimage: this.state.Pimage,
                                bidderId: this.state.bidderId,
                            })
                    }
                    else if(this.state.navigateTo.trim() === "edit-bid")
                    {
                        this.props.navigation.replace('ProjectEditBidScreen',
                        {
                            projectId: this.state.projectId,
                            ownerId: this.state.ownerId,
                            description: this.state.description,
                            serviceId: this.state.serviceId,
                            createdAt: this.state.createdAt,
                            bidderIdList: this.state.bidderIdList,
                            imageLink: this.state.imageLink,
                            ownerName: this.state.ownerName,
                            postalCode: this.state.postalCode,
                            timeString: this.state.timeString,
                            Pimage: this.state.Pimage,
                            projectStatus: this.state.projectStatus,
                            bidPrice:this.state.bidPrice,
                            bidderId: this.state.bidderId,
                            oldPrice:this.state.oldPrice

                        })
                    }
                    else {
                        this.props.navigation.navigate('MyAccountScreen', { uid: this.state.uid })
                    }

                }
                else {
                    this.setState({ isLoading: false });
                    alert("oops!! Transaction falied.Try again.. ");
                }
            })
    }
    
    async _stripeReMonthly() {
        return fetch('https://us-central1-pro-in-your-pocket.cloudfunctions.net/reMonthlySubscriptionsWithNewCard?docId=' + this.state.uid,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            })
            .then(response => response.json())
            .then(json => {
                console.log(json);
                //alert(JSON.stringify(json));
                if (json.status == "success") {
                    this.setState({ isLoading: false });
                    AsyncStorage.setItem("SubcriptionType", "paid");
                    if (this.state.navigateTo.trim() === "place-bid") {
                        this.props.navigation.replace('ProjectPlaceBidScreen',
                            {
                                projectId: this.state.projectId,
                                ownerId: this.state.ownerId,
                                description: this.state.description,
                                serviceId: this.state.serviceId,
                                createdAt: this.state.createdAt,
                                bidderIdList: this.state.bidderIdList,
                                imageLink: this.state.imageLink,
                                ownerName: this.state.ownerName,
                                postalCode: this.state.postalCode,
                                timeString: this.state.timeString,
                                Pimage: this.state.Pimage,
                                bidderId: this.state.bidderId,
                            })
                    }
                    else if(this.state.navigateTo.trim() === "edit-bid")
                    {
                        this.props.navigation.replace('ProjectEditBidScreen',
                        {
                            projectId: this.state.projectId,
                            ownerId: this.state.ownerId,
                            description: this.state.description,
                            serviceId: this.state.serviceId,
                            createdAt: this.state.createdAt,
                            bidderIdList: this.state.bidderIdList,
                            imageLink: this.state.imageLink,
                            ownerName: this.state.ownerName,
                            postalCode: this.state.postalCode,
                            timeString: this.state.timeString,
                            Pimage: this.state.Pimage,
                            projectStatus: this.state.projectStatus,
                            bidPrice:this.state.bidPrice,
                            bidderId: this.state.bidderId,

                        })
                    }
                    else {
                        this.props.navigation.navigate('MyAccountScreen', { uid: this.state.uid })
                    }
                }
                else {
                    this.setState({ isLoading: false });
                    alert("oops!! Transaction falied.Try again.. ");
                }
            })
    }

    _checkPayment() {
        this.setState({
            phoneNumberFormat: '',
            phone: '',
            CardHolderName: '',
            cvv: '',
            MM: '',
            YY: '',
        })
        var payment = db.collection("paymentsHistory").doc(this.state.uid);
        payment.get().then((doc) => {
            console.log("hiiiii");
            if (doc.exists) {
                console.log("payment data" + doc.data());
                if (doc.data().transaction.status == "succeeded") {
                    AsyncStorage.setItem("SubscriptionEndDate", doc.data().SubscriptionEndDate.toString());
                    AsyncStorage.setItem("SubcriptionType", doc.data().SubcriptionType);

                    CommonTasks._displayToastStripe("Transaction Successful !!!");
                    if (this.state.navigateTo.trim() === "place-bid") {
                        this.props.navigation.replace('ProjectPlaceBidScreen',
                            {
                                projectId: this.state.projectId,
                                ownerId: this.state.ownerId,
                                description: this.state.description,
                                serviceId: this.state.serviceId,
                                createdAt: this.state.createdAt,
                                bidderIdList: this.state.bidderIdList,
                                imageLink: this.state.imageLink,
                                ownerName: this.state.ownerName,
                                postalCode: this.state.postalCode,
                                timeString: this.state.timeString,
                                Pimage: this.state.Pimage,
                                bidderId: this.state.bidderId,
                            })
                    }
                    else if(this.state.navigateTo.trim() === "edit-bid")
                    {
                        this.props.navigation.replace('ProjectEditBidScreen',
                        {
                            projectId: this.state.projectId,
                            ownerId: this.state.ownerId,
                            description: this.state.description,
                            serviceId: this.state.serviceId,
                            createdAt: this.state.createdAt,
                            bidderIdList: this.state.bidderIdList,
                            imageLink: this.state.imageLink,
                            ownerName: this.state.ownerName,
                            postalCode: this.state.postalCode,
                            timeString: this.state.timeString,
                            Pimage: this.state.Pimage,
                            projectStatus: this.state.projectStatus,
                            bidPrice:this.state.bidPrice,
                            bidderId: this.state.bidderId,

                        })
                    }
                    else {
                        this.props.navigation.navigate('MyAccountScreen', { uid: this.state.uid })
                    }

                }
                else {
                    alert("oops!! Transaction falied.Try again.. ")
                }
            }
        })
    }

    onValueChange(value) {
        this.setState({
            MM: value
        });
    }


    _buySubscriptionUseCard() {
        this.setState({ isLoading:true });
        return fetch('https://us-central1-pro-in-your-pocket.cloudfunctions.net/reMonthlySubscriptions?docId=' + this.state.uid,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            })
            .then(response => response.json())
            .then(json => {
                console.log(json);
                //this.setState({isLoading:false});
                //alert(JSON.stringify(json));
                if (json.status == "success") {
                    this.setState({ isLoading: false });
                    AsyncStorage.setItem("SubcriptionType", "paid");
                    if (this.state.navigateTo.trim() === "place-bid") {
                        this.props.navigation.replace('ProjectPlaceBidScreen',
                            {
                                projectId: this.state.projectId,
                                ownerId: this.state.ownerId,
                                description: this.state.description,
                                serviceId: this.state.serviceId,
                                createdAt: this.state.createdAt,
                                bidderIdList: this.state.bidderIdList,
                                imageLink: this.state.imageLink,
                                ownerName: this.state.ownerName,
                                postalCode: this.state.postalCode,
                                timeString: this.state.timeString,
                                Pimage: this.state.Pimage,
                                bidderId: this.state.bidderId,
                            })
                    }
                    else if(this.state.navigateTo.trim() === "edit-bid")
                    {
                        this.props.navigation.replace('ProjectEditBidScreen',
                        {
                            projectId: this.state.projectId,
                            ownerId: this.state.ownerId,
                            description: this.state.description,
                            serviceId: this.state.serviceId,
                            createdAt: this.state.createdAt,
                            bidderIdList: this.state.bidderIdList,
                            imageLink: this.state.imageLink,
                            ownerName: this.state.ownerName,
                            postalCode: this.state.postalCode,
                            timeString: this.state.timeString,
                            Pimage: this.state.Pimage,
                            projectStatus: this.state.projectStatus,
                            bidPrice:this.state.bidPrice,
                            bidderId: this.state.bidderId,

                        })
                    }
                    else {
                        this.props.navigation.navigate('MyAccountScreen', { uid: this.state.uid })
                    }

                }
                else {
                    this.setState({ isLoading: false });
                    alert("oops!! Transaction falied.Try again.. ");
                }
            })
    }

    render() {

        return (

            <View style={styles.container}>

                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <MaterialCommunityIcons style={{ paddingLeft: 6 }} name="chevron-left" color="white" size={28} />
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Monthly Subscription</Text>
                    <View></View>
                </View>

                {(this.state.isLoading1 == false) ?
                    (<ScrollView>
                        <View style={styles.SubscriptionPlan}>
                            <Text style={styles.SubscriptionPlanText}>Subscription Plan</Text>
                        </View>


                        <Text style={styles.priceText}>$ <Text style={styles.priceValueText}>24.00</Text><Text style={{ fontSize: 17 }}>/Month</Text></Text>

                        {(this.state.SubcriptionStatus!='cancelled')?
                        (<Text style={{
                            textAlign: 'center',
                            fontFamily: 'regularText',
                            fontSize: 13,
                            marginBottom: 20,
                            marginHorizontal: 20,
                            fontWeight: "300"
                        }}>
                            After 30 days the $24/Month subscription starts automatically
                        </Text>)
                        :
                        null}


                        {(!this.state.viewCard) ?
                            (<View>
                                {(this.state.cardArr.length > 0) ?
                                    (<View style={styles.cardListingView}>
                                        <Text>****</Text>
                                        <Text>****</Text>
                                        <Text>****</Text>
                                        <Text>{this.state.cardArr[0].token.card.last4}</Text>

                                        <TouchableOpacity
                                            onPress={() => this._buySubscriptionUseCard()}
                                            style={{ borderColor: '#3A4958', borderWidth: 2, paddingHorizontal: 10, borderRadius: 5 }}>
                                            <Text style={{ fontFamily: 'RegularText', fontSize: 13, color: '#3A4958' }}>Use this Card</Text>
                                        </TouchableOpacity>
                                    </View>)
                                    :
                                    null}

                                {(!this.state.viewCard) ?
                                    (<TouchableOpacity
                                        onPress={() =>
                                            this.state.viewCard == false ?
                                                this.setState({
                                                    viewCard: true
                                                })
                                                :
                                                this.setState({
                                                    viewCard: false
                                                })
                                        }
                                        style={[styles.cardListingView, { justifyContent: 'flex-start', alignItems: "center" }]}>
                                        <MaterialIcons name="add-circle-outline" color="#3A4958" size={20} />
                                        <Text style={{ fontFamily: 'RegularText', fontSize: 13, color: '#3A4958', marginLeft: 10 }}>Use Another Card</Text>
                                    </TouchableOpacity>)
                                    :
                                    null}
                            </View>)
                            :
                            null}

                        {(this.state.viewCard) ?
                            (<View style={styles.cardView}>

                                <View style={{ flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.preText}>Card Number</Text>
                                        <View style={[styles.cardnumberView, { borderColor: this.state.selectedField == 'cardNo' ? '#3A4958' : '#DDDDDD', }]}>
                                            <Image source={require('../../assets/images/card.png')} style={styles.cardImage} />
                                            <TextInputMask
                                                returnKeyType="next"
                                                value={this.state.phoneNumberFormat}
                                                onChangeText={phoneNumberFormat => {
                                                    let phoneNumber = phoneNumberFormat
                                                        .toString()
                                                        .replace(/\D+/g, "");
                                                    this.setState({
                                                        phoneNumberFormat: phoneNumberFormat,
                                                        phone: phoneNumber,
                                                        verfication_statement: false
                                                    });
                                                }}
                                                onFocus={() => this.setState({ selectedField: 'cardNo' })}
                                                type={"credit-card"}
                                                maxLength={19}
                                                options={
                                                    this.state.phone
                                                        ? {
                                                            dddMask: "9999 9999 9999 9999"
                                                        }
                                                        : {
                                                            dddMask: "9999 9999 9999 9999"
                                                        }
                                                }
                                                style={styles.textInputCardNumber}
                                                onSubmitEditing={() => this.cvv.focus()}
                                            />
                                        </View>
                                    </View>
                                    <View>
                                        <Text style={styles.preText}>CVV</Text>
                                        <View style={[styles.cvvnumberView, { borderColor: this.state.selectedField == 'CVV' ? '#3A4958' : '#DDDDDD', }]}>
                                            <Image source={require('../../assets/images/cvv.png')} style={styles.cardImage} />
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                autoCorrect={false}
                                                keyboardType='number-pad'
                                                maxLength={3}
                                                returnKeyType="next"
                                                value={this.state.cvv}
                                                onChangeText={(cvv) => this.setState({ cvv: cvv })}
                                                ref={input => (this.cvv = input)}
                                                style={{ width: 35 }}
                                                onFocus={() => this.setState({ selectedField: 'CVV' })}
                                                onSubmitEditing={() => this.chname.focus()}
                                            />
                                            {/* <TextInput
                                    style={styles.textInput}
                                    keyboardType="numeric"
                                    underlineColorAndroid="transparent"
                                    placeholder="Verification code"
                                    placeholderTextColor="#ababab"
                                    returnKeyType="done"
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    // onSubmitEditing={() => this.inputLname.focus()}
                                    onChangeText={input => this.setState({ code: input })}
                                    ref={input => (this.inputCode = input)}
                                /> */}
                                        </View>
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                    <View>
                                        <Text style={styles.preText}>Name on Card</Text>
                                        <View style={[styles.NameonCardView, { borderColor: this.state.selectedField == 'cardName' ? '#3A4958' : '#DDDDDD', }]}>

                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                style={styles.nameoncardtxt}

                                                onChangeText={(e) => this.setState({ CardHolderName: e })}
                                                keyboardType='default'
                                                value={this.state.CardHolderName}
                                                placeholderTextColor="#ababab"
                                                returnKeyType='next'
                                                autoCorrect={false}
                                                onSubmitEditing={() => this.mm.focus()}
                                                //  onChangeText={(input) => this.setState({ CardHolderName : input})}
                                                ref={((input) => this.chname = input)}
                                                onFocus={() => this.setState({ selectedField: 'cardName' })}
                                            />

                                        </View>
                                    </View>

                                    <View>
                                        <Text style={styles.preText}>MM</Text>
                                        <View style={[styles.MMView, { borderColor: this.state.selectedField == 'mm' ? '#3A4958' : '#DDDDDD', }]}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                style={styles.mmyy}
                                                maxLength={2}
                                                onChangeText={(e) => this.setState({ MM: e })}
                                                keyboardType='decimal-pad'
                                                value={this.state.MM}
                                                placeholderTextColor="#ababab"
                                                returnKeyType='next'
                                                autoCorrect={false}
                                                onSubmitEditing={() => this.yy.focus()}
                                                //  onChangeText={(input) => this.setState({ CardHolderName : input})}
                                                ref={((input) => this.mm = input)}
                                                onFocus={() => this.setState({ selectedField: 'mm' })}
                                            />

                                        </View>
                                    </View>

                                    <View>
                                        <Text style={styles.preText}>YY</Text>
                                        <View style={[styles.YYView, { borderColor: this.state.selectedField == 'yy' ? '#3A4958' : '#DDDDDD', }]}>
                                            <TextInput
                                                underlineColorAndroid="transparent"
                                                style={styles.mmyy}
                                                maxLength={2}
                                                onChangeText={(e) => this.setState({ YY: e })}
                                                keyboardType='decimal-pad'
                                                value={this.state.YY}
                                                placeholderTextColor="#ababab"
                                                returnKeyType='done'
                                                autoCorrect={false}
                                                ref={((input) => this.yy = input)}
                                                onFocus={() => this.setState({ selectedField: 'yy' })}
                                            />
                                        </View>
                                    </View>
                                </View>
                                <View style={{ width: screenSize.width - 40, justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity style={styles.buyNow}
                                        onPress={() =>
                                            //this._payNow()
                                            this._payNowNew()
                                            //this._check()
                                        }
                                    >
                                        <Text style={styles.buyNowText}>Pay Now</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>)
                            :
                            null}

                        <View style={{ height: 200 }}>

                        </View>

                    </ScrollView>)
                    :
                    null}

                {this.state.isLoading == true ? (
                    <View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={[styles.activity_sub, { flexDirection: "row" }]}>
                            <ActivityIndicator
                                size="large"
                                color="black"
                                style={{
                                    justifyContent: "center",
                                    alignItems: "center",
                                    height: 50
                                }}
                            />
                            <Text style={{ color: "#ffff", marginLeft: 10 }}>Please wait...</Text>
                        </View>
                    </View>

                ) : null}

                {this.state.isLoading1 == true ? (
                    <View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={[styles.activity_sub, { flexDirection: "row" }]}>
                            <ActivityIndicator
                                size="large"
                                color="black"
                                style={{
                                    justifyContent: "center",
                                    alignItems: "center",
                                    height: 50
                                }}
                            />
                            <Text style={{ color: "#ffff", marginLeft: 10 }}>Please wait...</Text>
                        </View>
                    </View>

                ) : null}


            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        //backgroundColor: '#F0EFF5'
        backgroundColor: '#ffff'
    },
    cardView: {
        paddingHorizontal: 10,
        paddingVertical: 20,
        //backgroundColor: '#F0EFF5',
        backgroundColor: '#fff',
        elevation: 2,
        width: screenSize.width - 20,
        margin: 10,
        alignSelf: "center",
        height: 300,
        marginBottom: 50
    },
    buyNow: {
        backgroundColor: '#3A4958',
        paddingVertical: 10,
        paddingHorizontal: 50,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 30,
        maxWidth: 200
    },
    buyNowText: {
        fontFamily: 'regularText',
        fontSize: 21,
        color: '#fff'
    },
    nameoncardtxt: {
        fontFamily: 'regularText',
        fontSize: 15,
        width: 200,
        color: '#111'
    },
    cardnumberView: {

        borderWidth: 1,
        flexDirection: 'row',
        height: 45,
        paddingHorizontal: 10,
        alignItems: 'center',
        marginTop: 5,
        width: screenSize.width * .6,
        marginRight: 10
    },
    cvvnumberView: {

        borderWidth: 1,
        flexDirection: 'row',
        height: 45,
        paddingHorizontal: 10,
        alignItems: 'center',
        marginTop: 5,
        width: screenSize.width * .25
    },
    NameonCardView: {
        //borderColor : '#DDDDDD',
        borderWidth: 1,
        flexDirection: 'row',
        height: 45,
        paddingHorizontal: 10,
        alignItems: 'center',
        marginTop: 5,
        width: screenSize.width * .60,
        marginRight: 10
    },
    MMView: {
        //borderColor : '#DDDDDD',
        borderWidth: 1,
        flexDirection: 'row',
        height: 45,
        paddingHorizontal: 5,
        alignItems: 'center',
        marginTop: 5,
        width: screenSize.width * .11,
        marginRight: 10
    },
    YYView: {
        //borderColor : '#DDDDDD',
        borderWidth: 1,
        flexDirection: 'row',
        height: 45,
        paddingHorizontal: 5,
        alignItems: 'center',
        marginTop: 5,
        width: screenSize.width * .11
    },
    headerContainer: {
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 50 : 30,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: -5
    },
    cardImage: {
        marginRight: 5,
        width: 30,
        resizeMode: 'contain',

    },
    headerText: {
        fontFamily: boldText,
        color: 'white',
        fontSize: 18,
        marginLeft: -40
    },
    textInputCardNumber: {
        fontFamily: regularText,
        fontSize: 15,
        paddingLeft: 3,
        width: 180
    },
    preText: {
        fontFamily: regularText,
        fontSize: 14,
        color: '#807C7C'
    },
    timeText: {
        fontFamily: regularText,
        fontSize: 14,
        marginTop: 10
    },
    activity_sub: {
        paddingHorizontal: 10,
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'grey',
        // width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },
    cardListingView: {
        marginVertical: 10,
        width: screenSize.width - 20,
        paddingVertical: 10,
        paddingHorizontal: 20,
        flexDirection: "row",
        backgroundColor: "#ffff",
        elevation: 2,
        alignSelf: "center",
        justifyContent: "space-between"
    },
    SubscriptionPlan: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#000',
        paddingBottom: 10,
        marginBottom: 15,
        width: 200,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: 'center',
        margin: 10
    },
    SubscriptionPlanText: {
        fontFamily: 'regularText',
        fontSize: 15,
        fontWeight: 'bold',
    },
    priceText: {
        alignSelf: "center",
        fontFamily: 'regularText',
        fontSize: 40,
        fontWeight: 'bold',
        marginBottom: 10
    },
    priceValueText: {
        alignSelf: "center",
        fontFamily: 'regularText',
        color: '#3A4958'
    },
};


