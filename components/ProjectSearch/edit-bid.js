
/* Project place bid page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/feed-project-detail-typing-bid.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    BackHandler,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';
import Footer from '../footer/footer';
import {
    CachedImage,
} from 'react-native-cached-image';
import { NavigationActions, StackActions } from 'react-navigation';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const italicText = "SF-Pro-Display-LightItalic";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class ProjectEditBid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price: '',
            projectId: this.props.navigation.state.params.projectId,
            ownerId: this.props.navigation.state.params.ownerId,
            bidderId: this.props.navigation.state.params.bidderId,
            description: this.props.navigation.state.params.description,
            serviceId: this.props.navigation.state.params.serviceId,
            bidPrice: this.props.navigation.state.params.bidPrice,
            createdAt: this.props.navigation.state.params.createdAt,
            bidderIdList: this.props.navigation.state.params.bidderIdList,
            serviceData: CommonTasks.serviceList,
            imageLink: this.props.navigation.state.params.imageLink,
            ownerName: this.props.navigation.state.params.ownerName,
            postalCode: this.props.navigation.state.params.postalCode,
            timeString: this.props.navigation.state.params.timeString,
            Pimage: this.props.navigation.state.params.Pimage,
            navi: this.props.navigation.state.params.navi,
            projectStatus: this.props.navigation.state.params.projectStatus,
            user_id: "",
            editedAt: '',
            oldPrice: this.props.navigation.state.params.oldPrice,

        }
    }



    componentDidMount() {
        AsyncStorage.getItem('user_id').then(value => this.setState({ user_id: value }));
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        (this.state.navi == 'bd') ? this._gotoBidPlacedScreen() : this._gotoBidsPendingScreen()
        return true;
    }

    _getServiceName(id) {
        for (var i = 0; i < this.state.serviceData.length; i++) {
            if (this.state.serviceData[i].id == id) {
                return (
                    <Text style={styles.titleText}>{this.state.serviceData[i].name}</Text>
                )
            }
        }

    }

    _getImage(id) {

        //var serviceData = CommonTasks.serviceList;
        if (id == "") {
            return (
                <View style={{ backgroundColor: '#fff', paddingHorizontal: 10, paddingBottom: 10 }}>
                    <Image
                        source={require('../../assets/images/house.png')}
                        style={styles.propertyImage}
                    />
                </View>
            )
        }

        else {
            for (var i = 0; i < this.state.serviceData.length; i++) {
                if (this.state.serviceData[i].id == id) {
                    return (
                        <View style={{ backgroundColor: '#fff', paddingHorizontal: 10, paddingBottom: 10 }}>
                            <CachedImage
                                source={(this.state.serviceData[i].service_url == '') ? require('../../assets/images/house.png') : { uri: this.state.serviceData[i].service_url }}
                                style={styles.propertyImage}
                            />
                        </View>
                    )
                }
            }
        }

    }

    _showDate(date) {
        var dateData = date.split(" ")[0];

        var dateObject = new Date(Date.parse(dateData));

        var dateReadable = dateObject.toDateString().trim();

        var time = date.split(" ")[1];

        return (
            <View>
                <Text style={styles.timeText}>{dateReadable}  {this.state.timeString}</Text>
            </View>
        )
    }

    _placeBid() {
        //console.log("owner id"+this.state.ownerId);

        var subscription = false;
        AsyncStorage.getItem('SubcriptionType').then(value => {
            var SubcriptionType = value;
            if (SubcriptionType == 'Free' || SubcriptionType == 'paid') {
                subscription = false;
            }
            else if (SubcriptionType == 'Pending') {
                subscription = true;
            }

            if (subscription == false) {
            if (this.state.bidPrice === "" || this.state.bidPrice.trim() == "") {

            }
            else if (this.state.bidPrice == this.state.oldPrice) {
                CommonTasks._displayToast("Please a different amount for placed bid");
                this.inputPrice.focus();
                return false;
            }


            var currentTime = new Date();
            var month = currentTime.getMonth() + 1;
            var day = currentTime.getDate();
            var year = currentTime.getFullYear();
            var createBid = month + "/" + day + "/" + year;
            var hours = currentTime.getHours();
            var minutes = currentTime.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            hours = hours < 10 ? '0' + hours : hours;
            var strTime = hours + ':' + minutes + ' ' + ampm;

            this.setState({
                editedAt: createBid
            })

            this.setState({ isLoading: true });




            db.collection("place_bid").doc(this.state.projectId + "_" + this.state.bidderId)
                .update({
                    ownerId: this.state.ownerId,
                    bidderId: this.state.bidderId,
                    projectId: this.state.projectId,
                    bidPrice: this.state.bidPrice,
                    description: this.state.description,
                    serviceId: this.state.serviceId,
                    createdAt: createBid,
                    createdTimeStamp: currentTime.valueOf(),
                    createdTime: strTime,
                }).then((user) => {
                    //this.setState({ isLoading: false });
                    console.log("user data" + user);
                    CommonTasks._displayToast("Bid Edited");
                    this.state.navi == 'bp' ?
                        this._gotoBidsPendingScreen()
                        :
                        this._gotoBidPlacedScreen()
                }).catch((error) => {
                    this.setState({ isLoading: false });
                    console.log("user error" + error)
                })
            }
            else if (subscription == true) {
                
                this.setState({

                })
                this.props.navigation.replace('BuySubscriptionScreen', {
                    projectId: this.state.projectId,
                    ownerId: this.state.ownerId,
                    description: this.state.description,
                    serviceId: this.state.serviceId,
                    createdAt: this.state.createdAt,
                    bidderIdList: this.state.bidderIdList,
                    imageLink: this.state.imageLink,
                    ownerName: this.state.ownerName,
                    postalCode: this.state.postalCode,
                    timeString: this.state.timeString,
                    Pimage: this.state.Pimage,
                    bidderId:this.state.bidderId,
                    bidPrice: this.state.bidPrice,
                    projectStatus: this.state.projectStatus,
                    oldPrice: this.state.oldPrice,
                    navigateTo: "edit-bid"

                });
            }
        })
    }

    _gotoBidPlacedScreen() {
        this.props.navigation.replace('ProjectBidPlacedScreen', {
            bidId: this.state.projectId + "_" + this.state.bidderId,
            bidPrice: this.state.bidPrice,
            description: this.state.description,
            serviceId: this.state.serviceId,
            projectId: this.state.projectId,
            createdAt: this.state.createdAt,
            bidderId: this.state.bidderId,
            bidderIdList: this.state.bidderIdList,
            ownerId: this.state.ownerId,
            ownerName: this.state.ownerName,
            imageLink: this.state.imageLink,
            postalCode: this.state.postalCode,
            timeString: this.state.timeString,
            Pimage: this.state.Pimage,
            projectStatus: this.state.projectStatus
        });
    }


    _gotoBidsPendingScreen() {
        this.props.navigation.replace('BidsPendingScreen', { uid: this.state.bidderId, selectedOption: 'Pending' });
    }


    _gotoCustomerProfile() {
        // console.log('hi')
        this.props.navigation.navigate('CustomerProfileScreen',
            {
                profileId: this.state.ownerId,
                profileName: this.state.ownerName,
                profileImage: this.state.imageLink
            })
    }



    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.topContainer}>
                        <TouchableOpacity style={{ width: 30 }}
                            // onPress={() => this.props.navigation.goBack()}
                            onPress={() => (this.state.navi == 'bd') ? this._gotoBidPlacedScreen() : this._gotoBidsPendingScreen()}
                        >
                            <Ionicons name="ios-arrow-back" style={styles.backButton} />
                        </TouchableOpacity>
                        <View style={styles.avatorContainer}>
                            <TouchableOpacity onPress={() => this._gotoCustomerProfile()}>
                                <CachedImage
                                    source={this.state.imageLink ?
                                        { uri: this.state.imageLink }
                                        :
                                        require('../../assets/images/userOwner.png')}
                                    style={styles.avatorImage}
                                />
                            </TouchableOpacity>

                            <View style={{ marginLeft: 10 }}>
                                <Text style={styles.titleText}><Text onPress={() => this._gotoCustomerProfile()}>{this.state.ownerName}</Text>&nbsp;
                                <Text style={{ fontWeight: '100', }}>requested</Text>&nbsp;
                                {this._getServiceName(this.state.serviceId)} <Text style={{ color: 'grey' }}>{this.state.postalCode ? 'in ' + this.state.postalCode : null}</Text>

                                </Text>
                                {/* <Text style={styles.timeText}>Tuesday 10:54 am</Text> */}
                                {this._showDate(this.state.createdAt)}
                            </View>
                        </View>
                    </View>

                    <View style={{ backgroundColor: '#fff', marginVertical: 0.5 }}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.dollerText}>$</Text>
                            <TextInput
                                style={styles.textInput}
                                keyboardType='numeric'
                                underlineColorAndroid="transparent"
                                placeholder="Enter a bid"
                                placeholderTextColor="#ababab"
                                returnKeyType='done'
                                autoCorrect={false}
                                autoCapitalize="none"
                                value={this.state.bidPrice}
                                // onSubmitEditing={() => this.inputLname.focus()}
                                onChangeText={(input) => this.setState({ bidPrice: input })}
                                ref={((input) => this.inputPrice = input)}
                            />
                        </View>
                        <TouchableOpacity style={styles.placebidButton}
                            //onPress={() => this.gotoBidPlaced()}
                            onPress={() => this._placeBid()}
                        >
                            <Text style={styles.placebidText}>Edit Bid</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ backgroundColor: '#fff' }}>
                        <Text style={styles.descriptionText}>{this.state.description}</Text>
                    </View>
                    {/* <View style={{ backgroundColor: '#fff', paddingHorizontal: 10, paddingBottom: 10 }}>
                        <Image
                            source={require('../../assets/images/house.png')}
                            style={styles.propertyImage}
                        />
                    </View> */}


                    {this.state.Pimage ?
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={{ flexDirection: "row" }}>
                                {
                                    this.state.Pimage.map((item, index) => (
                                        <View style={{ width: screenSize.width, height: 210, backgroundColor: '#fff', justifyContent: "center", alignItems: "center", paddingBottom: 10 }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ImageViewScreen', { image_uri: item })} key={index}>
                                                <CachedImage
                                                    source={{ uri: item }}
                                                    style={styles.propertyImage}
                                                />
                                            </TouchableOpacity>
                                        </View>

                                    ))
                                }
                            </View>
                        </ScrollView>



                        :
                        this._getImage(this.state.serviceId)}



                    {(this.state.bidderIdList != null) ?
                        (
                            <View style={styles.bidContainer}>
                                <Text style={styles.bidText}>{this.state.bidderIdList.length} bids</Text>
                            </View>
                        )
                        :
                        null}
                </ScrollView>

                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}


                <Footer itemColor='globe'></Footer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },

    backButton: {
        fontSize: 30,
        color: '#1e1e1e',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 100
    },

    avatorContainer: {
        flexDirection: 'row',
        marginTop: 10,
        padding: 10
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    inputContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        //    backgroundColor: 'white',        
    },

    textInput: {
        flex: 1,
        fontFamily: italicText,
        fontSize: 20,
    },

    dollerText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        fontSize: 22,
        marginHorizontal: 10
    },

    placebidButton: {
        backgroundColor: '#4cd964',
        marginHorizontal: 15,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        borderRadius: 25
    },

    placebidText: {
        color: 'white',
        fontFamily: regularText,
        fontSize: 22
    },

    descriptionText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        marginVertical: 10,
        fontSize: 15,
        marginHorizontal: 10
    },

    propertyImage: {
        width: screenSize.width - 20,
        height: 200,
        resizeMode: 'cover'
    },

    bidText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 16
    },

    bidContainer: {
        // marginTop: 10,
        // paddingTop: 5,
        marginTop: 1,
        padding: 10,
        backgroundColor: '#fff'
    },

    footerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        // alignItems : 'center',
        height: 60,
        backgroundColor: '#fff'
    },

    footerItem: {
        width: screenSize.width / 4,
        alignItems: 'center',
        justifyContent: 'center'
    },

    footerImage: {
        fontSize: 35
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});