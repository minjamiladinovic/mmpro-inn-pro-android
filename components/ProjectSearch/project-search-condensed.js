
/* Project search condensed page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/project-search-condensed.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Footer from '../footer/footer';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class ProjectSearchCondensed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#3A4958"
                    barStyle="light-content"
                />
                <View style={styles.headerContainer}>
                    <MaterialCommunityIcons name="settings" style={styles.headerIcon} />
                    <Image
                        source={require('../../assets/images/logo.png')}
                        style={{ width: 150, height: 50, alignSelf: 'center', resizeMode: 'contain' }}
                    />
                    <MaterialCommunityIcons name="menu" style={styles.headerIcon} />
                </View>

                <ScrollView>
                    <View style={styles.itemContainer}>
                        <View style={styles.imageHeadingContainer}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png' }}
                                    style={styles.avatorImage}
                                />
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={styles.titleText}>Justin Waite&nbsp;
                                <Text style={{ fontWeight: '100' }}>requested</Text>&nbsp;
                                    Landscaping - Pavers
                                </Text>
                                    <Text style={styles.timeText}>Tuesday 10:54 am</Text>
                                </View>
                            </View>
                            <Text style={styles.descriptionText}>I have about 100 sqrt of pavers that need to be laid in my</Text>
                            <View style={styles.bidContainer}>
                                <Text style={styles.bidText}>1 bids</Text>
                            </View>
                        </View>
                    </View>

                    <View style={styles.itemContainer}>
                        <View style={styles.imageHeadingContainer}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={{ uri: 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png' }}
                                    style={styles.avatorImage}
                                />
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={styles.titleText}>Devid Solheim&nbsp;
                                <Text style={{ fontWeight: '100' }}>requested</Text>&nbsp;
                                    Landscaping - Lawn
                                </Text>
                                    <Text style={styles.timeText}>Monday 11:00 am</Text>
                                </View>
                            </View>
                            <Text style={styles.descriptionText}>We have a .25 acre yard that needs to be maintained.</Text>
                            <View style={styles.bidContainer}>
                                <Text style={styles.bidText}>0 bids</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <Footer iconColor='globe'></Footer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    headerContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#3A4958',
        paddingTop: Platform.OS === 'ios' ? 20 : 5,
        paddingHorizontal: 5
    },

    headerIcon: {
        color: '#fff',
        fontSize: 32
    },

    itemContainer: {
        backgroundColor: 'white',
        marginTop: 10,
        padding: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 95
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    descriptionText: {
        fontFamily: regularText,
        fontSize: 16,
        marginVertical: 10,
        color: '#1e1e1e'
    },

    bidText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 16
    },

    bidContainer: {
        marginTop: 10,
        paddingTop: 5,
        borderTopColor: '#ecebf0',
        borderTopWidth: 0.5
    },

    
})