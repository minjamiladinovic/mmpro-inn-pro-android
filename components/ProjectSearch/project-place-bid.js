
/* Project place bid page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/feed-project-detail-typing-bid.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    FlatList,
    Linking,
    StatusBar,
    Keyboard
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';
import Footer from '../footer/footer';
import {
    CachedImage,
} from 'react-native-cached-image';
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';
import colors from '../../common-tasks/values/colors';
import VerifiedLabel from '../IndependentComponent/verifiedLabel';

const hasNotch = DeviceInfo.hasNotch();
const model = DeviceInfo.getModel();

const regularText = "Lato-Regular";
const boldText = "Lato-Bold";
const italicText = "Lato-LightItalic";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();
const MyStatusBar = ({ backgroundColor, ...props }) => (
    <View style={[{ height: hasNotch ? (model == 'iPhone XR' ? 33 : 30) : 20 }, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class ProjectPlaceBid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            price: '',
            projectId: this.props.navigation.state.params.projectId,
            owner: this.props.navigation.state.params.ownerId,
            bidderId: this.props.navigation.state.params.bidderId,
            description: this.props.navigation.state.params.description,
            serviceId: this.props.navigation.state.params.serviceId,
            createdAt: this.props.navigation.state.params.createdAt,
            serviceData: CommonTasks.serviceList,
            bidderIdList: this.props.navigation.state.params.bidderIdList ? this.props.navigation.state.params.bidderIdList : [],
            imageLink: this.props.navigation.state.params.imageLink,
            ownerName: this.props.navigation.state.params.ownerName,
            postalCode: this.props.navigation.state.params.postalCode,
            timeString: this.props.navigation.state.params.timeString,
            Pimage: this.props.navigation.state.params.Pimage,
            projectVerified: this.props.navigation.state.params.projectVerified,
            email_verify_status: this.props.navigation.state.params.email_verify_status,
            user_id: "",
            bidderIdList1: [],
            isLoading: false,
            SubcriptionType: "",
            SubscriptionEndDate: "",
        }
    }



    componentDidMount() {
        AsyncStorage.getItem('user_id').then(value => this.setState({ user_id: value }));
        AsyncStorage.getItem('SubcriptionType').then(value => this.setState({ SubcriptionType: value }));
    }

    gotoBidPlaced() {
        this.props.navigation.navigate('ProjectBidPlacedScreen');
    }

    _getServiceName(id) {
        for (var i = 0; i < this.state.serviceData.length; i++) {
            if (this.state.serviceData[i].id == id) {
                return (
                    <Text style={styles.titleText}>{this.state.serviceData[i].name}</Text>
                )
            }
        }

    }


    _getImage(id) {

        //var serviceData = CommonTasks.serviceList;
        if (id == "") {
            return (
                <View style={{ backgroundColor: '#fff', paddingHorizontal: 10, paddingBottom: 10 }}>
                    <Image
                        source={require('../../assets/images/house.png')}
                        style={styles.propertyImage}
                    />
                </View>
            )
        }

        else {
            for (var i = 0; i < this.state.serviceData.length; i++) {
                if (this.state.serviceData[i].id == id) {
                    return (
                        <View style={{ backgroundColor: '#fff', paddingHorizontal: 10, paddingBottom: 10 }}>
                            <CachedImage
                                source={(this.state.serviceData[i].service_url == '') ? require('../../assets/images/house.png') : { uri: this.state.serviceData[i].service_url }}
                                style={styles.propertyImage}
                            />
                        </View>
                    )
                }
            }
        }

    }

    _showDate(date) {
        var dateData = date.split(" ")[0];

        var dateObject = new Date(Date.parse(dateData));

        var dateReadable = dateObject.toDateString().trim();

        var time = date.split(" ")[1];

        return (
            <View>
                <Text style={styles.timeText}>{dateReadable}  {this.state.timeString}</Text>
            </View>
        )
    }


    _checkSubscription() {
        var d = new Date();
        var today = d.getTime();
        //alert('today : '+today+'\nend : '+Number(this.state.SubscriptionEndDate)+'\ntype : '+this.state.SubcriptionType)
        if (this.state.SubcriptionType == "Free") {
            return false;
        }
        else if (this.state.SubcriptionType != "paid") {
            return true;
        }
        else {
            return false;
        }
    }

    _placeBid() {

        var subscription = false;
        AsyncStorage.getItem('SubcriptionType').then(value => {
            var SubcriptionType = value;
            if (SubcriptionType == 'Free' || SubcriptionType == 'paid') {
                subscription = false;
            }
            else if (SubcriptionType == 'Pending') {
                subscription = true;
            }
            if (subscription == false) {
                if (this.state.price === "" || this.state.price.trim() == "") {
                    CommonTasks._displayToast("Please enter valid amount for placed bid");
                    this.inputPrice.focus();
                    return false;
                }
                var currentTime = new Date();
                var month = currentTime.getMonth() + 1;
                var day = currentTime.getDate();
                var year = currentTime.getFullYear();
                var createBid = month + "/" + day + "/" + year;
                var hours = currentTime.getHours();
                var minutes = currentTime.getMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0' + minutes : minutes;
                hours = hours < 10 ? '0' + hours : hours;
                var strTime = hours + ':' + minutes + ' ' + ampm;

                console.log('state bidderid list : ' + this.state.bidderIdList)

                this.setState({ isLoading: true });
                var bidderList = this.state.bidderIdList;
                console.log('state bidder list : ' + bidderList)
                if (bidderList.indexOf(this.state.bidderId) == -1) {
                    bidderList.push(this.state.bidderId);
                }
                console.log('state bidder list updated: ' + bidderList)

                db.collection("place_bid").doc(this.state.projectId + "_" + this.state.bidderId).set({
                    ownerId: this.state.owner,
                    bidderId: this.state.bidderId,
                    projectId: this.state.projectId,
                    bidPrice: this.state.price,
                    description: this.state.description,
                    serviceId: this.state.serviceId,
                    createdAt: createBid,
                    createdTimeStamp: currentTime.valueOf(),
                    createdTime: strTime,
                    projectStatus: 'Pending'
                }).then((user) => {
                    //this.setState({ isLoading: false });
                    console.log("user data" + user);
                    db.collection("projects").doc(this.state.projectId).update({
                        bidderIdList: bidderList
                    }).then((user) => {

                        if (this.state.SubcriptionType == "Free") {
                            //alert("hi");
                            db.collection("users").doc(this.state.user_id).update({
                                SubcriptionType: "Pending",
                            }).then((user) => {
                                AsyncStorage.setItem("SubcriptionType", "Pending");
                                this.setState({
                                    isLoading: false,
                                    bidderIdList: bidderList
                                });
                            })
                        }
                        else {
                            this.setState({
                                isLoading: false,
                                bidderIdList: bidderList
                            });
                        }
                        //alert("hi");

                        this.props.navigation.replace('ProjectBidPlacedScreen', {
                            bidId: this.state.projectId + "_" + this.state.bidderId,
                            bidPrice: this.state.price,
                            bidderId: this.state.bidderId,
                            bidderIdList: bidderList,
                            projectId: this.state.projectId,
                            ownerId: this.state.owner,
                            ownerName: this.state.ownerName,
                            imageLink: this.state.imageLink ? this.state.imageLink : null,
                            description: this.state.description,
                            serviceId: this.state.serviceId,
                            createdAt: this.state.createdAt,
                            postalCode: this.state.postalCode,
                            timeString: this.state.timeString,
                            Pimage: this.state.Pimage ? this.state.Pimage : null,
                            projectStatus: 'Pending',
                            navigateFrom: 'placeBid',
                        });
                        console.log("user data" + user);
                    }).catch((error) => {
                        this.setState({ isLoading: false });
                        console.log("user error" + error)
                    })
                }).catch((error) => {
                    this.setState({ isLoading: false });
                    alert(error);
                    console.log("user error" + error)
                })
            }
            else if (subscription == true) {
                this.props.navigation.replace('BuySubscriptionScreen', {
                    projectId: this.state.projectId,
                    ownerId: this.state.ownerId,
                    description: this.state.description,
                    serviceId: this.state.serviceId,
                    createdAt: this.state.createdAt,
                    bidderIdList: this.state.bidderIdList,
                    imageLink: this.state.imageLink,
                    ownerName: this.state.ownerName,
                    postalCode: this.state.postalCode,
                    timeString: this.state.timeString,
                    Pimage: this.state.Pimage,
                    bidderId: this.state.uid,
                    navigateTo: "place-bid"

                });
            }
        })
    }




    _gotoCustomerProfile() {
        // console.log('hi')
        this.props.navigation.navigate('CustomerProfileScreen',
            {
                profileId: this.state.owner,
                profileName: this.state.ownerName,
                profileImage: this.state.imageLink,
                email_verify_status: this.state.email_verify_status
            })
    }



    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#3A4958" barStyle="light-content" />
                <ScrollView>
                    <View style={styles.topContainer}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("ProjectSearchScreen", { uid: this.state.user_id })}
                        >
                            <Ionicons name="ios-arrow-back" style={styles.backButton} />
                        </TouchableOpacity>
                        <View style={styles.avatorContainer}>

                            <TouchableOpacity onPress={() => this._gotoCustomerProfile()}>
                                <CachedImage
                                    source={this.state.imageLink ? {
                                        uri: this.state.imageLink
                                    }
                                        :
                                        require('../../assets/images/userOwner.png')}
                                    style={styles.avatorImage}
                                />
                            </TouchableOpacity>
                            <View style={{ marginLeft: 5, width: screenSize.width - 140 }}>
                                <Text style={styles.titleText}><Text onPress={() => this._gotoCustomerProfile()}>{this.state.ownerName}</Text>&nbsp;
                                <Text style={{ fontWeight: '100', }}>requested</Text>&nbsp;
                                {this._getServiceName(this.state.serviceId)} <Text style={{ color: 'grey' }}>{this.state.postalCode ? 'in ' + this.state.postalCode : null}</Text>
                                </Text>
                                {/* <Text style={styles.timeText}>Tuesday 10:54 am</Text> */}
                                {this._showDate(this.state.createdAt)}
                            </View>
                        </View>
                    </View>

                    <View style={{ backgroundColor: '#fff', marginVertical: 0.5 }}>
                        <View style={styles.inputContainer}>
                            <Text style={styles.dollerText}>$</Text>
                            <TextInput
                                style={styles.textInput}
                                keyboardType='numeric'
                                underlineColorAndroid="transparent"
                                placeholder="Enter a bid"
                                placeholderTextColor="#ababab"
                                returnKeyType='done'
                                autoCorrect={false}
                                autoCapitalize="none"
                                //onSubmitEditing={() => Keyboard.dismiss()}
                                onChangeText={(input) => this.setState({ price: input })}
                                ref={((input) => this.inputPrice = input)}
                            />
                        </View>
                        <TouchableOpacity activeOpacity={0.7}
                            style={styles.placebidButton}
                            //onPress={() => this.gotoBidPlaced()}
                            onPress={() => this._placeBid()}
                        >
                            <Text style={styles.placebidText}>Place Bid</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ backgroundColor: '#fff' }}>
                        <Text style={styles.descriptionText}>{this.state.description}</Text>
                        <VerifiedLabel projectVerified={this.state.projectVerified} type={'Project'} />
                        <VerifiedLabel projectVerified={this.state.email_verify_status} type={'User'} />
                    </View>

                    {this.state.Pimage ?
                        <>
                            <ScrollView
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            >
                                <View style={{ flexDirection: "row" }}>
                                    {this.state.Pimage.map((item, index) => (
                                        <View
                                            style={{
                                                width: screenSize.width,
                                                height: 210,
                                                backgroundColor: "#fff",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                paddingBottom: 10
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() =>
                                                    this.props.navigation.navigate("ImageViewScreen", {
                                                        image_uri: item
                                                    })
                                                }
                                                key={index}
                                            >
                                                <CachedImage
                                                    source={{ uri: item }}
                                                    style={styles.propertyImage}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    ))}
                                </View>
                            </ScrollView>
                            <ScrollView
                                horizontal={true}
                                contentContainerStyle={{ minWidth: screenSize.width }}>
                                {this.state.Pimage.map((item, index) => (
                                    <View
                                        style={{
                                            width: 120,
                                            height: 120,
                                            backgroundColor: "#fff",
                                            justifyContent: "center",
                                            alignItems: "center",
                                        }}
                                    >
                                        <TouchableOpacity
                                            onPress={() =>
                                                this.props.navigation.navigate("ImageViewScreen", {
                                                    image_uri: item
                                                })
                                            }
                                            key={index}
                                        >
                                            <CachedImage
                                                source={{ uri: item }}
                                                style={{ height: 100, width: 100, borderColor: colors.colorPrimary, borderWidth: 1 }}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                ))}
                            </ScrollView>
                        </>
                        :
                        this._getImage(this.state.serviceId)}
                    {(this.state.bidderIdList != null) ?
                        (
                            <View style={styles.bidContainer}>
                                <Text style={styles.bidText}>{this.state.bidderIdList.length} bids</Text>
                            </View>
                        )
                        :
                        null}
                </ScrollView>

                {(this.state.isLoading == true) ?
                    (<View style={{ position: 'absolute', height: screenSize.height, width: screenSize.width, backgroundColor: 'rgba(255, 255, 255, 0.2)' }}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>
                    </View>)
                    : null}

                <Footer itemColor='globe'></Footer>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee'
    },

    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },

    backButton: {
        fontSize: 30,
        color: '#1e1e1e',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 100
    },

    avatorContainer: {
        flexDirection: 'row',
        marginTop: 10,
        padding: 10
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    inputContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },

    textInput: {
        flex: 1,
        fontFamily: italicText,
        fontSize: 20,
        paddingVertical: 15
    },

    dollerText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        fontSize: 22,
        marginHorizontal: 10
    },

    placebidButton: {
        backgroundColor: '#4cd964',
        marginHorizontal: 15,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        borderRadius: 25
    },

    placebidText: {
        color: 'white',
        fontFamily: regularText,
        fontSize: 22
    },

    descriptionText: {
        fontFamily: regularText,
        color: '#1e1e1e',
        marginVertical: 10,
        fontSize: 15,
        marginHorizontal: 10
    },
    propertyImage: {
        width: screenSize.width - 20,
        height: 200,
        resizeMode: 'cover',
        paddingHorizontal: 10,
    },
    bidText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 16
    },

    bidContainer: {
        marginTop: 1,
        padding: 10,
        backgroundColor: '#fff'
    },


    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});