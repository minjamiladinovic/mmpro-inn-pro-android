
/* Project search page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/feed-project-detail-deave-review-rating-selected.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import StarRating from 'react-native-star-rating';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';


const regularText = "SF-Pro-Display-Regular";
const italicText = "SF-Pro-Display-LightItalic";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class ProjectReview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 0,
            message: "",
            projectId: this.props.navigation.state.params.projectId,
            projectDescription : this.props.navigation.state.params.projectDescription,
            projectOwner: this.props.navigation.state.params.projectOwner,
            formReviewId: this.props.navigation.state.params.formReviewId,
            serviceId: this.props.navigation.state.params.serviceId,
            createdAt: this.props.navigation.state.params.createdAt,
            imageLink : this.props.navigation.state.params.imageLink,
            ownerName: this.props.navigation.state.params.ownerName,
            postalCode : this.props.navigation.state.params.postalCode,
            timeString: this.props.navigation.state.params.timeString,
            serviceData: CommonTasks.serviceList,
            isLoading: false,
            user_id: ""
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('user_id').then(value => this.setState({ user_id: value }));
    }
    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    _getServiceNameOnly(id) {
        for (var i = 0; i < this.state.serviceData.length; i++) {
            if (this.state.serviceData[i].id == id) {
                return (
                    this.state.serviceData[i].name
                )
            }
        }

    }
    _getServiceName(id) {
        for (var i = 0; i < this.state.serviceData.length; i++) {
            if (this.state.serviceData[i].id == id) {
                return (
                    <Text style={styles.titleText}>{this.state.serviceData[i].name}</Text>
                )
            }
        }

    }

    _showDate(date) {
        var dateData = date.split(" ")[0];

        var dateObject = new Date(Date.parse(dateData));

        var dateReadable = dateObject.toDateString().trim();

        var time = date.split(" ")[1];

        return (
            <View>
                <Text style={styles.timeText}>{dateReadable}  {this.state.timeString}</Text>
            </View>
        )
    }


    

    _sendReview() {
        //alert();
       // console.log(this.state.formReviewId);
        var d=new Date();
        var ts= d.valueOf();
        // var day=d.getDate();
        // var mon=d.getMonth() + 1;
        // var year=d.getFullYear();
        // var hours=d.getHours();
        // var minutes=d.getMinutes();
        // var ampm = hours >= 12 ? 'pm' : 'am';
        //         hours = hours % 12;
        //         hours = hours ? hours : 12; // the hour '0' should be '12'
        //         minutes = minutes < 10 ? '0'+minutes : minutes;
        //         hours = hours < 10 ? '0'+hours : hours;
        // var strTime = hours + ':' + minutes + ' ' + ampm;
        // var date=day+'/'+mon+'/'+year;
        if (this.state.starCount == 0) {
            CommonTasks._displayToast("Please give your rating");
            return false;
        }
        if (this.state.message== "") {
            CommonTasks._displayToast("Please leave a message");
            return false;
        }
        this.setState({ isLoading: true });
        db.collection("review").doc(this.state.projectId + "_" + this.state.formReviewId).set({
            projectId: this.state.projectId,
            projectDescription : this.state.projectDescription,
            ownerId : this.state.projectOwner,
            userId: this.state.formReviewId,
            rating: this.state.starCount,
            review: this.state.message,
            timeStamp : ts,
            serviceId : this.state.serviceId,
            serviceName: this._getServiceNameOnly(this.state.serviceId),
            postalCode : this.state.postalCode,

        }).then((success) => {
            //this.setState({ isLoading: false });
            db.collection("place_bid").doc(this.state.projectId+'_'+this.state.formReviewId)
            .update({
                projectStatus: "Completed",
                reviewedByPro : 'y' 
            }).then((success) => {
                db.collection("projects").doc(this.state.projectId).update({
                    projectStatus: "Completed",
                    completedBy : 'bidder',
                    hiredId : this.state.formReviewId
                }).then((user) => {
                    
                    CommonTasks._displayToast("This project has been completed");
                    this._onget();
                }).catch((error) => {
                    this.setState({ isLoading: false });
               //     console.log("user error" + error)
                })
            }).catch((error) => {
                this.setState({ isLoading: false });
                console.error("Error completing  document: ", error);
                this._onget();
            });
            CommonTasks._displayToast("Project Successfully Completed");
            // db.collection("projects").doc(this.state.projectId).update(
            //     {
            //         reviewStatus: 'y',
            //         reviewInfo:
            //         {
            //             "reviewerId": this.state.formReviewId,
            //             "description": this.state.message,
            //             "rating": this.state.starCount,
            //             reviewDate:date,
            //             reviewedAt:strTime,
            //             reviewTimeStamp: new Date(),
            //         }
            //     }
            // ).then((success)=>
            //     {this.props.navigation.replace('BidsPendingScreen',{uid: this.state.user_id , selectedOption : 'Completed'});}
            // ).catch((error) => {
            //     console.log("user error" + error);
            //     this.props.navigation.replace('BidsPendingScreen',{uid: this.state.user_id, selectedOption : 'Completed'});
            // })
            this.props.navigation.replace('BidsPendingScreen',{uid: this.state.user_id , selectedOption : 'Completed'});
            

        }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error)
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.topContainer}>
                        <TouchableOpacity onPress={() => this.props.navigation.replace('BidsPendingScreen',{uid: this.state.user_id, selectedOption : 'Hired'})}>
                            <Ionicons name="ios-arrow-back" style={styles.backButton} />
                        </TouchableOpacity>
                        <View style={styles.avatorContainer}>
                            <Image
                                source={this.state.imageLink?{
                                uri: this.state.imageLink }
                                :
                                require('../../assets/images/userOwner.png')}
                                style={styles.avatorImage}
                            />
                            <View style={{ marginLeft: 10 }}>
                                <Text style={styles.titleText}>{this.state.ownerName}&nbsp;
                                <Text style={{ fontWeight: '100', }}>requested</Text>&nbsp;
                                {this._getServiceName(this.state.serviceId)} <Text style={{color : 'grey'}}>{this.state.postalCode?'in '+this.state.postalCode : null}</Text>
                                </Text>
                                {this._showDate(this.state.createdAt)}
                                {/* <Text style={styles.timeText}>Tuesday 10:54 am</Text> */}
                            </View>
                        </View>
                    </View>

                    <View style={styles.starContainer}>
                        <Text style={styles.rateText}>Rate your customer</Text>
                        <StarRating
                            disabled={false}
                            emptyStar={'ios-star-outline'}
                            fullStar={'ios-star'}
                            halfStar={'ios-star-half'}
                            iconSet={'Ionicons'}
                            maxStars={5}
                            rating={this.state.starCount}
                            selectedStar={(rating) => this.onStarRatingPress(rating)}
                            fullStarColor={'#007aff'}
                            emptyStarColor={'#007aff'}
                            containerStyle={{ marginRight: 140 }}
                        // starSize={30}
                        />
                    </View>

                    <View style={styles.reviewContainer}>
                        <TextInput
                            style={styles.textInput}
                            multiline={true}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="Enter a message"
                            placeholderTextColor="#ababab"
                            returnKeyType='send'
                            autoCorrect={false}
                            autoCapitalize="sentences"
                            // onSubmitEditing={() => this.inputPassword.focus()}
                            onChangeText={(input) => this.setState({ message: input })}
                            ref={((input) => this.inputMessage = input)}
                        />
                    </View>

                    <TouchableOpacity activeOpacity={0.7}
                    style={styles.placeReviewContainer}
                        onPress={() => this._sendReview()}
                    >
                        <Text style={styles.reviewText}>Leave Review</Text>
                    </TouchableOpacity>
                </ScrollView>

                {(this.state.isLoading == true) ?
                    (
                        <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                         <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>   
                        </View>)
                    : null}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },

    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },

    backButton: {
        fontSize: 30,
        color: '#1e1e1e',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 100
    },

    avatorContainer: {
        flexDirection: 'row',
        marginTop: 10,
        padding: 10
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    starContainer: {
        paddingLeft: 10,
        marginTop: 10,
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.5,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        paddingVertical: 10
    },

    rateText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#8e8e93',
        marginBottom: 5
    },

    textInput: {
        marginLeft: 10,
        fontFamily: italicText,
        fontSize: 16,
        flex: 1,
    },

    reviewContainer: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
    },

    placeReviewContainer: {
        backgroundColor: '#3A4958',
        marginHorizontal: 10,
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,
        borderRadius: 25,
    },

    reviewText: {
        color: '#fff',
        fontFamily: regularText,
        fontSize: 22
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})