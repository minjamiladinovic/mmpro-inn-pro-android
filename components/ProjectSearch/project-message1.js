
/* Project bid placed page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/project-messages-empty.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    FlatList,
    Keyboard
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Footer from '../footer/footer';
import Entypo from 'react-native-vector-icons/Entypo';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
// import { DotsLoader, RotationHoleLoader, CirclesLoader, BubblesLoader } from 'react-native-indicator';
import { CachedImage } from 'react-native-cached-image';


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const italicText = "SF-Pro-Display-LightItalic";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class ProjectMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show_archive: false,
            ProjectOwnerName: this.props.navigation.state.params.ProjectOwnerName,
            uid: this.props.navigation.state.params.Uid,
            toChatId: this.props.navigation.state.params.toChatId,
            fromChatId: this.props.navigation.state.params.fromChatId,
            navigateFrom: this.props.navigation.state.params.navigateFrom,
            message_status: this.props.navigation.state.params.message_status,
            imageLink: this.props.navigation.state.params.imageLink,
            serviceData: CommonTasks.serviceList,
            show_archive: false,
            message: "",
            isLoading: false,
            isLoading1: true,
            chatArray: [],
            isLoadingMore: false,
            isarchive: false,
            reciver_message_status:"",
        }
    }

    componentDidMount() {
        console.log("Project owner name" + this.state.ProjectCreateDate);
        console.log("uid" + this.state.uid);
        console.log("to chat id" + this.state.toChatId);
        console.log("from chat id" + this.state.fromChatId);
        this._getMessages();

    }


    _showArchive() {
        if (this.state.show_archive == false) {
            this.setState({
                show_archive: true
            })
        }
        else {
            this.setState({
                show_archive: false
            })
        }

    }
    _getMessages() {
        //alert();
        //this.setState({ isLoading1:true });
        var docRef = db.collection("messages").doc(this.state.uid).collection(this.state.message_status).doc(this.state.toChatId);

        if (this.state.navigateFrom.trim() == "project_details") {
            var docRef2 = db.collection("messages").doc(this.state.uid).collection("archive").doc(this.state.toChatId);
            docRef2.get().then((doc) => {
                if (doc.exists) {
                    this.setState({
                        chatArray: doc.data().chat,
                        message_status: "archive"
                    });
                }
                else {
                    docRef.get().then((doc) => {
                        if (doc.exists) {
                            this.setState({
                                chatArray: doc.data().chat
                            });
                            console.log("chat array" + doc.data().chat);
                        } else {
                            // doc.data() will be undefined in this case
                            console.log("No such document!");
                        }
                        this.setState({ isLoading1: false });
                    }).catch((eror) => {
                        this.setState({ isLoading1: false });
                        console.log("Error getting document:", error);
                    });
                }
                this.setState({ isLoading1: false });
            }).catch((eror) => {
                this.setState({ isLoading1: false });
                console.log("Error getting document:", error);
            });
        }

        else {
            docRef.get().then((doc) => {
                if (doc.exists) {
                    this.setState({
                        chatArray: doc.data().chat
                    });
                    console.log("chat array" + doc.data().chat);
                } else {
                    // doc.data() will be undefined in this case
                    console.log("No such document!");
                }
                this.setState({ isLoading1: false });
            }).catch((eror) => {
                this.setState({ isLoading1: false });
                console.log("Error getting document:", error);
            });
        }

        this._checkReciverStatus();

    }
    _checkReciverStatus()
    {
        this.setState({ isLoading1:true });
        var docRef = db.collection("messages").doc(this.state.toChatId).collection("archive").doc(this.state.uid);
        docRef.get().then((doc) => {
            if (doc.exists) {
                this.setState({
                    reciver_message_status: "archive"
                });
            }
            else
            {
                this.setState({
                    reciver_message_status: "inbox"
                });
            }
            this.setState({ isLoading1:false });
        }).catch((eror) => {
            this.setState({ isLoading1: false });
            console.log("Error getting document:", error);
        });
    }

    _getMessage_afterSend()
    {
      this.setState({ isLoading1:true});
      var docRef = db.collection("messages").doc(this.state.uid).collection(this.state.message_status).doc(this.state.toChatId);
      docRef.get().then((doc) => {
        if (doc.exists) {
          this.setState({
            chatArray: doc.data().chat
          });
          console.log("chat array" + doc.data().chat);
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
        this.setState({ isLoading1: false });
      }).catch((eror) => {
        this.setState({ isLoading1: false });
        console.log("Error getting document:", error);
      });
    }
    
    // _sendMessage() {

    //     this.setState({ isLoading: true });
    //     var message_id = this.state.fromChatId + "-" + this.state.toChatId;//customer-serviceprovider

    //     var obj = {
    //         "message_id": message_id,
    //         "sender_id": this.state.fromChatId,
    //         "reciver_id": this.state.toChatId,
    //         "description": this.state.message,
    //         "status": "inbox",
    //         "createdAt": new Date()
    //     };
    //     var array = this.state.chatArray;
    //     array.unshift(obj);

    //     db.collection("messages").doc(this.state.uid)
    //         .collection(this.state.message_status)
    //         .doc(this.state.toChatId)
    //         .set({
    //             chat: array
    //         }).then((user) => {
    //             db.collection("messages").doc(this.state.toChatId)
    //                 .collection(this.state.reciver_message_status)
    //                 .doc(this.state.fromChatId)
    //                 .set({
    //                     chat: array
    //                 }).then((user) => {
    //                     console.log("success");
    //                     this.inputMessage.setNativeProps({ text: "" });
    //                     Keyboard.dismiss();
    //                     this._getMessage_afterSend();
    //                     this.setState({ isLoading: false });
    //                 }).catch((error) => {
    //                     this.setState({ isLoading: false });
    //                     console.log("user error" + error)
    //                 })
    //         }).catch((error) => {
    //             this.setState({ isLoading: false });
    //             console.log("user error" + error)
    //         })
    // }


    _sendMessage() {

        this.setState({ isLoading: true });
        var message_id = this.state.fromChatId + "-" + this.state.toChatId;//customer-serviceprovider
    
        var obj = {
          "message_id": message_id,
          "sender_id": this.state.fromChatId,
          "reciver_id": this.state.toChatId,
          "description": this.state.message,
          "status": "inbox",
          "createdAt": new Date()
        };
        var array = this.state.chatArray;
        array.unshift(obj);
    
        db.collection("messages").doc(this.state.uid)
          .collection(this.state.message_status)
          .doc(this.state.toChatId)
          .set({
            chat: array
          }).then((user) => {
            db.collection("messages").doc(this.state.toChatId)
              .collection(this.state.reciver_message_status)
              .doc(this.state.fromChatId)
              .set({
                chat: array
              }).then((user) => {
    
                db.collection("messages_function").doc(this.state.fromChatId + "_" + this.state.toChatId)
                  .update({
                    reciver_id: this.state.toChatId,
                    sender_id: this.state.fromChatId,
                    message: this.state.message,
                    reciver_msj_status: this.state.reciver_message_status,
                    create_message_time: new Date(),
                    reciver_status:"Customer"
                  }).then((user) => {
                    console.log("success");
                    this.inputMessage.setNativeProps({ text: "" });
                    Keyboard.dismiss();
                    this._getMessage_afterSend();
                    this.setState({ isLoading: false });
    
                  }).catch((error) => {
                    db.collection("messages_function").doc(this.state.fromChatId + "_" + this.state.toChatId)
                      .set({
                        reciver_id: this.state.toChatId,
                        sender_id: this.state.fromChatId,
                        message: this.state.message,
                        reciver_msj_status: this.state.reciver_message_status,
                        create_message_time: new Date(),
                        reciver_status:"Customer"
                      }).then((user) => {
                        console.log("success");
                        this.inputMessage.setNativeProps({ text: "" });
                        Keyboard.dismiss();
                        this._getMessage_afterSend();
                        this.setState({ isLoading: false });
                      }).catch((error) => {
                        this.setState({ isLoading: false });
                        console.log("user error" + error)
                      })
                  })
                console.log("success");
                this.inputMessage.setNativeProps({ text: "" });
                Keyboard.dismiss();
                this._getMessage_afterSend();
                this.setState({ isLoading: false });
              }).catch((error) => {
                this.setState({ isLoading: false });
                console.log("user error" + error)
              })
          }).catch((error) => {
            this.setState({ isLoading: false });
            console.log("user error" + error)
          })
      }

      
    _archiveChat() {
        this.setState({
            show_archive: false,
            isLoading1: true
        });


        db.collection("messages").doc(this.state.uid)
            .collection("inbox").doc(this.state.toChatId).delete().then((data) =>
                db.collection("messages").doc(this.state.uid)
                    .collection("archive")
                    .doc(this.state.toChatId)
                    .set({
                        chat: this.state.chatArray
                    }).then((user) => {
                        this.setState({
                            isLoading1: false,
                            //isarchive: true,
                            message_status: "archive"
                            // show_archive: false
                        });
                        //this.props.navigation.navigate("Messages");
                    }).catch((error) => {
                        this.setState({ isLoading1: false });
                        console.log("user error" + error)
                    }))
    }





    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={[styles.topContainer, { zIndex: 0,borderBottomColor:'#8e8e93',borderBottomWidth:0.4, }]}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Ionicons name="ios-arrow-back" style={styles.backButton} />
                        </TouchableOpacity>
                        <View style={styles.avatorContainer}>
                            {(this.state.imageLink == "" || this.state.imageLink == undefined || this.state.imageLink == null) ?
                                (
                                    <Image
                                        source={require('../../assets/images/userOwner.png')}
                                        style={styles.avatorImage}
                                    />
                                )
                                :
                                (
                                    <CachedImage
                                        source={{ uri: this.state.imageLink }}
                                        style={styles.avatorImage}
                                    />
                                )}

                            <View style={{ marginLeft: 10 }}>
                                {/* <Text style={styles.titleText}>{this.state.ProjectOwnerName}</Text> */}

                                <View style={{ flexDirection: "row", justifyContent: "space-between", width: screenSize.width - 130, marginTop: 15 }}>
                                    {/* {this._showDate(this.state.ProjectCreateDate)} */}
                                    <Text style={styles.titleText}>{this.state.ProjectOwnerName}</Text>

                                    {(this.state.message_status == "inbox") ?
                                        (<TouchableOpacity
                                            onPress={() => this._showArchive()}>
                                            <Entypo name='dots-three-vertical' color='#969da8' size={30} />
                                        </TouchableOpacity>)
                                        :
                                        null}
                                </View>

                                {(this.state.show_archive == true) ?
                                    (<TouchableOpacity
                                        style={styles.archiveMenu}
                                        onPress={() => this._archiveChat()}
                                    >
                                        <Text style={{ color: "black", fontWeight: "600", fontSize: 16 }}>Archive</Text>
                                    </TouchableOpacity>)
                                    :
                                    null}

                            </View>
                        </View>
                    </View>

                    {/* <View style={[styles.bidPlaceContainer, { zIndex: 0 }]}>
                        <Text style={styles.bidText}>You have placed a bid for ${this.state.BidPrice}.</Text>
                    </View> */}

                    <FlatList
                        data={this.state.chatArray}
                        inverted={true}
                        ListHeaderComponent={() =>
                            <View style={{ height: 100 }}></View>
                        }
                        renderItem={({ item, index }) => (
                            <View>
                                {/* Message section start */}

                                {/* sender section */}
                                {(item.sender_id == this.state.uid) ?
                                    (<View style={[styles.senderMessageContainer]}>
                                        <Text style={styles.sendMessageText}>{item.description}</Text>
                                    </View>)
                                    :
                                    (<View style={styles.receiverMessageContainer}>
                                        <Text style={styles.receiveMessageText}>{item.description}</Text>
                                    </View>)}
                                {/* receiver section */}
                                {/* Message section end */}
                            </View>
                        )}
                        //onEndReached={this._onEndReached.bind(this)}
                        ListFooterComponent={() => {
                            <View style={{ height: 110 }}></View>
                            return (
                                this.state.isLoadingMore &&
                                <View style={{ flex: 1, padding: 10 }}>
                                    <ActivityIndicator size="small" />
                                </View>
                            );
                        }}
                        keyExtractor={item => item.sender_id}
                    />


                </ScrollView>

                {
                    (this.state.isLoading1 == true) ?
                    (<View style={styles.activity_main}>
                    </View>)
                        :
                        (<View>
                            {(this.state.isarchive == false) ?
                                (<View style={styles.messageContainer}>
                                    <TextInput
                                        style={styles.textInput}
                                        multiline={true}
                                        keyboardType='default'
                                        underlineColorAndroid="transparent"
                                        placeholder="Enter a message"
                                        placeholderTextColor="#ababab"
                                        returnKeyType='send'
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        // onSubmitEditing={() => this.inputPassword.focus()}
                                        onChangeText={(input) => this.setState({ message: input })}
                                        ref={((input) => this.inputMessage = input)}
                                    />

                                    {(this.state.isLoading == true) ?
                                        (<View style={{ padding: 5 }}>
                                            <ActivityIndicator size={15} rotationSpeed={800} strokeWidth={10} color='#3A4958' />
                                        </View>)
                                        :
                                        null}

                                    {(this.state.message.trim() != "") ?
                                        (
                                            <TouchableOpacity onPress={() => this._sendMessage()}>
                                                <Text style={styles.sendText}>Send</Text>
                                            </TouchableOpacity>)
                                        :
                                        (<View>
                                            <Text style={styles.sendText}>Send</Text>
                                        </View>
                                        )}

                                </View>)
                                :
                                null}
                        </View>)}

                {/* {(this.state.isLoading1 == true) ?
                    (<View style={styles.activity_sub}>
                        <BubblesLoader size={50} dotRadius={5} color='black' />
                    </View>)
                    :
                    null} */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //zIndex:0
    },

    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center'
    },

    backButton: {
        fontSize: 30,
        color: '#1e1e1e',
        paddingLeft: 10
    },

    avatorImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    titleText: {
        fontFamily: regularText,
        fontWeight: 'bold',
        color: '#1e1e1e',
        fontSize: 16,
        maxWidth: screenSize.width - 100
    },

    avatorContainer: {
        flexDirection: 'row',
        marginTop: 10,
        padding: 10
    },

    timeText: {
        fontFamily: regularText,
        fontSize: 16,
        marginTop: 10
    },

    bidPlaceContainer: {
        paddingHorizontal: 10,
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.7,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        paddingVertical: 15
    },

    bidText: {
        fontFamily: italicText,
        fontSize: 15
    },

    footerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        // alignItems : 'center',
        height: 60,
        backgroundColor: '#fff'
    },

    footerItem: {
        width: screenSize.width / 4,
        alignItems: 'center',
        justifyContent: 'center'
    },

    footerImage: {
        fontSize: 35
    },

    messageContainer: {
        borderTopColor: '#8e8e93',
        borderTopWidth: 0.5,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center'
    },

    textInput: {
        marginLeft: 10,
        fontFamily: italicText,
        fontSize: 16,
        flex: 1
    },

    sendText: {
        fontFamily: regularText,
        color: '#3A4958',
        marginRight: 10,
        fontSize: 18
        // alignSelf: 'flex-end'
    },

    senderMessageContainer: {
        backgroundColor: '#3A4958',
        marginTop: 8,
        marginBottom: 4,
        marginRight: 10,
        maxWidth: screenSize.width - 60,
        alignSelf: 'flex-end',
        borderRadius: 10
    },
    activity_sub1: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'rgba(200, 200, 200, .6)',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },

    sendMessageText: {
        color: '#fff',
        fontFamily: regularText,
        padding: 10
    },

    receiverMessageContainer: {
        backgroundColor: '#e5e5ea',
        marginTop: 8,
        marginBottom: 4,
        marginLeft: 10,
        maxWidth: screenSize.width - 60,
        alignSelf: 'flex-start',
        borderRadius: 10
    },

    receiveMessageText: {
        color: '#1e1e1e',
        fontFamily: regularText,
        padding: 10
    },
    archiveMenu: {
        backgroundColor: "#ffff",
        width: 100,
        height: 40,
        position: "absolute",
        right:30,
        top:30,
        zIndex: 10,
        padding: 10,
        elevation: 5,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: 'center'
    },
    activity_main: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 5,
        ...Platform.select({
          android: { elevation: 5, },
          ios: {
            shadowColor: '#999',
            shadowOffset: {
              width: 0,
              height: 3
            },
            shadowRadius: 5,
            shadowOpacity: 0.5,
          },
        }),
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
      },
    
      activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
          android: { elevation: 5, },
          ios: {
            shadowColor: '#999',
            shadowOffset: {
              width: 0,
              height: 3
            },
            shadowRadius: 5,
            shadowOpacity: 0.5,
          },
        }),
      },
})
