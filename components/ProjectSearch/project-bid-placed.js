/* Project bid placed page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/feed-project-detail-bid-placed.png
 */

import React, { Fragment } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  Button,
  ActivityIndicator,
  Platform,
  Dimensions,
  ImageBackground,
  ScrollView,
  Linking,
  StatusBar,
  Alert
} from "react-native";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import CommonTasks from "../../common-tasks/common-tasks";
import firebase from "react-native-firebase";
import Footer from "../footer/footer";
import { CachedImage } from "react-native-cached-image";
import DeviceInfo from 'react-native-device-info';
import { NavigationActions, StackActions } from 'react-navigation';
import colors from "../../common-tasks/values/colors";
import VerifiedLabel from "../IndependentComponent/verifiedLabel";

const hasNotch = DeviceInfo.hasNotch();
const model = DeviceInfo.getModel();
const regularText = "Lato-Regular";
const boldText = "Lato-Bold";
const italicText = "Lato-LightItalic";
const screenSize = Dimensions.get("window");
const db = firebase.firestore();
const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[{ height: hasNotch ? (model == 'iPhone XR' ? 33 : 30) : 20 }, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class ProjectBidPlaced extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showHideProjectAction: false,
      serviceData: CommonTasks.serviceList,
      bidId: this.props.navigation.state.params.bidId,
      bidPrice: this.props.navigation.state.params.bidPrice,
      bidderId: this.props.navigation.state.params.bidderId,
      bidderIdList: this.props.navigation.state.params.bidderIdList,
      projectId: this.props.navigation.state.params.projectId,
      ownerId: this.props.navigation.state.params.ownerId,
      ownerName: this.props.navigation.state.params.ownerName,
      imageLink: this.props.navigation.state.params.imageLink,
      description: this.props.navigation.state.params.description,
      serviceId: this.props.navigation.state.params.serviceId,
      createdAt: this.props.navigation.state.params.createdAt,
      postalCode: this.props.navigation.state.params.postalCode,
      timeString: this.props.navigation.state.params.timeString,
      Pimage: this.props.navigation.state.params.Pimage,
      projectStatus: this.props.navigation.state.params.projectStatus,
      navigateFrom: this.props.navigation.state.params.navigateFrom,
      projectVerified: this.props.navigation.state.params.projectVerified,
      email_verify_status: this.props.navigation.state.params.email_verify_status,

      user_id: "",
      isLoading: false
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("user_id").then(value =>
      this.setState({ user_id: value })
    );
    console.log("owner id" + this.state.ownerId);
  }

  showActionPopup() {
    this.setState({ showHideProjectAction: true });
  }

  _getServiceName(id) {
    for (var i = 0; i < this.state.serviceData.length; i++) {
      if (this.state.serviceData[i].id == id) {
        return (
          <Text style={styles.titleText}>{this.state.serviceData[i].name}</Text>
        );
      }
    }
  }

  _showDate(date) {
    alert(date);
    var dateData = date.split(" ")[0];

    var dateObject = new Date(Date.parse(dateData));

    var dateReadable = dateObject.toDateString().trim();

    var time = date.split(" ")[1];

    return (
      <View>
        <Text style={styles.timeText}>{dateReadable}</Text>
      </View>
    );
  }

  _deleteBid() {
    Alert.alert(
      "Pro In Your Pocket For Pros",
      "Are you sure to delete this bid?",
      [
        {
          text: "Cancel",
          onPress: () => this.setState({ showHideProjectAction: false }),
          style: "cancel"
        },
        { text: "OK", onPress: () => this._deleteBidFinal() }
      ],
      { cancelable: false }
    );
  }
  _deleteBidFinal() {
    this.setState({ showHideProjectAction: false, isLoading: true });
    console.log("prev array " + this.state.bidderIdList);

    for (var i = 0; i < this.state.bidderIdList.length; i++) {
      if (this.state.bidderIdList[i].trim() === this.state.bidderId.trim()) {
        console.log("bidder list" + this.state.bidderId);
        console.log("array element" + this.state.bidderIdList[i]);
        this.state.bidderIdList.splice(i, 1);
      }
    }

    console.log("del array " + this.state.bidderIdList);

    db.collection("place_bid")
      .doc(this.state.bidId)
      .delete()
      .then(success => {
        console.log("Document successfully deleted!");
        db.collection("projects")
          .doc(this.state.projectId)
          .update({
            bidderIdList: this.state.bidderIdList
          })
          .then(user => {
            this.setState({ isLoading: false });
            this.props.navigation.navigate("ProjectSearchScreen", {
              id: this.state.user_id
            });
            //alert("done");
          })
          .catch(error => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
          });
      })
      .catch(error => {
        this.setState({ isLoading: false });
        console.error("Error removing document: ", error);
      });
  }

  _editBid() {
    this.setState({ showHideProjectAction: false });
    this.props.navigation.replace("ProjectEditBidScreen", {
      projectId: this.state.projectId,
      ownerId: this.state.ownerId,
      bidderId: this.state.bidderId,
      description: this.state.description,
      serviceId: this.state.serviceId,
      bidPrice: this.state.bidPrice,
      createdAt: this.state.createdAt,
      bidderIdList: this.state.bidderIdList,
      imageLink: this.state.imageLink,
      ownerName: this.state.ownerName,
      postalCode: this.state.postalCode,
      timeString: this.state.timeString,
      Pimage: this.state.Pimage,
      projectStatus: this.state.projectStatus,
      navi: "bd"
    });
  }

  _showDate(date) {
    var dateData = date.split(" ")[0];

    var dateObject = new Date(Date.parse(dateData));

    var dateReadable = dateObject.toDateString().trim();

    var time = date.split(" ")[1];

    return (
      <View>
        <Text style={styles.timeText}>
          {dateReadable} {this.state.timeString}
        </Text>
      </View>
    );
  }

  gotoProjectMessage() {
    this.props.navigation.navigate("ProjectMessageScreen", {
      ProjectOwnerName: this.state.ownerName,
      Uid: this.state.user_id,
      toChatId: this.state.ownerId,
      fromChatId: this.state.user_id,
      navigateFrom: "project_details",
      message_status: "inbox",
      imageLink: this.state.imageLink,
      // projectId : this.state.projectId,

      project_id: this.state.projectId
    });
  }

  _completeProject() {
    this.setState({ showHideProjectAction: false, isLoading: true });
    console.log("bidId" + this.state.bidId);
    console.log("project id" + this.state.projectId);

    db.collection("place_bid")
      .doc(this.state.bidId)
      .update({
        projectStatus: "Completed"
      })
      .then(success => {
        db.collection("projects")
          .doc(this.state.projectId)
          .update({
            projectStatus: "Completed",
            completedBy: "bidder",
            hiredId: this.state.bidderId
          })
          .then(user => {
            this.setState({ isLoading: false });
            CommonTasks._displayToast("This project has been completed");
            this.props.navigation.replace("ProjectSearchScreen", {
              id: this.state.user_id
            });
            //alert("done");
          })
          .catch(error => {
            this.setState({ isLoading: false });
            console.log("user error" + error);
          });
      })
      .catch(error => {
        this.setState({ isLoading: false });
        console.error("Error removing document: ", error);
      });
  }

  _getImage(id) {
    //var serviceData = CommonTasks.serviceList;
    if (id == "") {
      return (
        <View
          style={{
            backgroundColor: "#fff",
            paddingHorizontal: 10,
            paddingBottom: 10
          }}
        >
          <Image
            source={require("../../assets/images/house.png")}
            style={styles.propertyImage}
          />
        </View>
      );
    } else {
      for (var i = 0; i < this.state.serviceData.length; i++) {
        if (this.state.serviceData[i].id == id) {
          return (
            <View
              style={{
                backgroundColor: "#fff",
                paddingHorizontal: 10,
                paddingBottom: 10
              }}
            >
              <CachedImage
                source={
                  this.state.serviceData[i].service_url == ""
                    ? require("../../assets/images/house.png")
                    : { uri: this.state.serviceData[i].service_url }
                }
                style={styles.propertyImage}
              />
            </View>
          );
        }
      }
    }
  }

  _gotoCustomerProfile() {
    // console.log('hi')
    this.props.navigation.navigate("CustomerProfileScreen", {
      profileId: this.state.ownerId,
      profileName: this.state.ownerName,
      profileImage: this.state.imageLink,
      email_verify_status: this.state.email_verify_status
    });
  }

  resetStactToProjectSearch() {

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({
        routeName: 'ProjectSearchScreen',
      })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  handleBackButton = () => {
    if (this.state.navigateFrom == 'placeBid') {
      this.resetStactToProjectSearch();
    }
    else {
      this.props.navigation.pop();
    }

    return true;
  }

  render() {
    return (
      <View style={styles.container}>
        <MyStatusBar backgroundColor="#3A4958" barStyle="light-content" />
        {/* Design for custom picker start */}
        {this.state.showHideProjectAction == true ? (
          <View style={styles.pickerContainer}>
            <View style={styles.pickerOptionContainer}>
              <View style={styles.projectActionContainer}>
                <Text style={styles.actionText}>Project Actions</Text>
                {this.state.projectStatus == "Hired" ? (
                  <TouchableOpacity
                    activeOpacity={0.7}
                    style={styles.projectAction}
                    onPress={() => this._completeProject()}
                  >
                    <Text style={styles.optionText}>Complete Project</Text>
                  </TouchableOpacity>
                ) : null}
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={styles.projectAction}
                  onPress={() => this._editBid()}
                >
                  <Text style={styles.optionText}>Edit Bid</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={styles.projectAction}
                  onPress={() => this._deleteBid()}
                >
                  <Text style={[styles.optionText, { color: "red" }]}>
                    Delete Bid
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.cancelBtn}
                onPress={() => this.setState({ showHideProjectAction: false })}
              >
                <Text style={styles.optionText}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : null}
        {/* Design for custom picker end */}

        <ScrollView>
          <View style={styles.topContainer}>
            <TouchableOpacity
              style={{}}
              onPress={() => this.props.navigation.goBack()}
              onPress={() => this.handleBackButton()
              }
            >
              <Ionicons name="ios-arrow-back" style={styles.backButton} />
            </TouchableOpacity>
            <View style={styles.avatorContainer}>
              <TouchableOpacity onPress={() => this._gotoCustomerProfile()}>
                <Image
                  source={
                    this.state.imageLink
                      ? { uri: this.state.imageLink }
                      : require("../../assets/images/userOwner.png")
                  }
                  style={styles.avatorImage}
                />
              </TouchableOpacity>
              <View style={{ marginLeft: 5, width: screenSize.width - 140 }}>
                <Text style={styles.titleText}>
                  <Text onPress={() => this._gotoCustomerProfile()}>
                    {this.state.ownerName}
                  </Text>
                  &nbsp;
                  <Text style={{ fontWeight: "100" }}>requested</Text>&nbsp;
                  {this._getServiceName(this.state.serviceId)}{" "}
                  <Text style={{ color: "grey" }}>
                    {this.state.postalCode
                      ? "in " + this.state.postalCode
                      : null}
                  </Text>
                </Text>
                {this._showDate(this.state.createdAt)}
              </View>
            </View>
          </View>

          <View style={styles.bidContainer}>
            <View style={styles.bidContent}>
              <Text style={[styles.bidText, { marginLeft: 10 }]}>
                Your bid:
              </Text>
              <Text
                style={[styles.bidText, { marginLeft: 10, color: "#4cd964" }]}
              >
                $ {this.state.bidPrice}
              </Text>
            </View>
            <View style={styles.bidContent}>
              <MaterialIcons
                name="message"
                onPress={() => this.gotoProjectMessage()}
                style={[
                  styles.optionImage,
                  { color: "#007aff", marginRight: 15 }
                ]}
              />
              <MaterialCommunityIcons
                onPress={() => this.showActionPopup()}
                name="dots-vertical"
                style={[
                  styles.optionImage,
                  { color: "#1e1e1e", marginRight: 5 }
                ]}
              />
            </View>
          </View>

          <View style={{ backgroundColor: "#fff" }}>
            <Text style={styles.descriptionText}>{this.state.description}</Text>
            <VerifiedLabel projectVerified={this.state.projectVerified} type={'Project'} />
              <VerifiedLabel projectVerified={this.state.email_verify_status} type={'User'} />
          </View>

          {this.state.Pimage ? (
            <>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <View style={{ flexDirection: "row" }}>
                  {this.state.Pimage.map((item, index) => (
                    <View
                      style={{
                        width: screenSize.width,
                        height: 210,
                        backgroundColor: "#fff",
                        justifyContent: "center",
                        alignItems: "center",
                        paddingBottom: 10
                      }}
                    >
                      <TouchableOpacity
                        onPress={() =>
                          this.props.navigation.navigate("ImageViewScreen", {
                            image_uri: item
                          })
                        }
                        key={index}
                      >
                        <CachedImage
                          source={{ uri: item }}
                          style={styles.propertyImage}
                        />
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              </ScrollView>
              <ScrollView
                horizontal={true}
                contentContainerStyle={{ minWidth: screenSize.width }}
                showsHorizontalScrollIndicator={true}>
                {this.state.Pimage.map((item, index) => (
                  <View
                    style={{
                      width: 120,
                      height: 120,
                      backgroundColor: "#fff",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("ImageViewScreen", {
                          image_uri: item
                        })
                      }
                      key={index}
                    >
                      <CachedImage
                        source={{ uri: item }}
                        style={{ height: 100, width: 100, borderColor: colors.colorPrimary, borderWidth: 1 }}
                      />
                    </TouchableOpacity>
                  </View>
                ))}

              </ScrollView>

            </>
          ) : (
              this._getImage(this.state.serviceId)
            )}

          <View style={styles.bidContainer2}>
            <Text style={styles.bidText2}>
              {this.state.bidderIdList.length} bids
            </Text>
          </View>
        </ScrollView>
        {this.state.isLoading == true ? (
          <View
            style={{
              position: "absolute",
              height: screenSize.height,
              width: screenSize.width,
              backgroundColor: "rgba(255, 255, 255, 0.2)"
            }}
          >
            <View style={styles.activity_sub}>
              <ActivityIndicator
                size="large"
                color="#D0D3D4"
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: 50
                }}
              />
            </View>
          </View>
        ) : null}

        <Footer itemColor="globe" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eee'
  },

  topContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center"
  },

  backButton: {
    fontSize: 30,
    color: "#1e1e1e",
    paddingLeft: 10
  },

  avatorImage: {
    width: 70,
    height: 70,
    borderRadius: 35
  },

  titleText: {
    fontFamily: regularText,
    fontWeight: "bold",
    color: "#1e1e1e",
    fontSize: 16,
    maxWidth: screenSize.width - 100
  },

  avatorContainer: {
    flexDirection: "row",
    marginTop: 10,
    padding: 10
  },

  timeText: {
    fontFamily: regularText,
    fontSize: 16,
    marginTop: 10
  },

  bidContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#fff",
    marginVertical: 1,
    paddingVertical: 10
  },

  bidContent: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },

  optionImage: {
    fontSize: 30
  },

  bidText: {
    fontFamily: regularText,
    fontSize: 21
  },

  descriptionText: {
    fontFamily: regularText,
    color: "#1e1e1e",
    marginVertical: 10,
    fontSize: 15,
    marginHorizontal: 10
  },

  propertyImage: {
    width: screenSize.width - 20,
    height: 200,
    resizeMode: "cover"
  },

  bidText2: {
    fontFamily: boldText,
    color: "#1e1e1e",
    fontSize: 16
  },

  bidContainer2: {
    marginTop: 1,
    padding: 10,
    backgroundColor: "#fff"
  },

  pickerContainer: {
    width: screenSize.width,
    height: screenSize.height,
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0 ,0.7)",
    position: "absolute",
    zIndex: 3
  },

  pickerOptionContainer: {
    bottom: 40,
    position: "absolute"
  },

  cancelBtn: {
    backgroundColor: "white",
    width: screenSize.width - 20,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 12
  },

  optionText: {
    fontFamily: regularText,
    color: "#3A4958",
    fontSize: 22,
    fontWeight: "100"
  },

  projectActionContainer: {
    backgroundColor: "#fff",
    marginBottom: 10,
    borderRadius: 12
  },

  actionText: {
    alignSelf: "center",
    paddingVertical: 10,
    fontFamily: regularText,
    fontSize: 15
  },

  projectAction: {
    alignItems: "center",
    justifyContent: "center",
    height: 46,
    borderTopColor: "#8e8e93",
    borderTopWidth: 0.7
  },
  activity_sub: {
    position: "absolute",
    top: screenSize.height / 2,
    backgroundColor: "black",
    width: 50,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 10,
    ...Platform.select({
      android: { elevation: 5 },
      ios: {
        shadowColor: "#999",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5
      }
    }),
    height: 50,
    borderRadius: 10
  }
});
