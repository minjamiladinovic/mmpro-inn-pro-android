import React, { Component } from "react";
import { withNavigation } from "react-navigation";
import { BackHandler } from "react-native";


const { uid } = this.props;
class HandleBack extends Component {
  constructor(props) {
    super(props);
    this.didFocus = props.navigation.addListener("didFocus", payload =>
      BackHandler.addEventListener("hardwareBackPress", this.onBack),
    );
  }

  componentDidMount() {
    this.willBlur = this.props.navigation.addListener("willBlur", payload =>
      BackHandler.removeEventListener("hardwareBackPress", this.onBack),
    );
  }

  onBack = () => {
    // if (this.state.editing) {
    //   Alert.alert(
    //     "You're still editing!",
    //     "Are you sure you want to go home with your edits not saved?",
    //     [
    //       { text: "Keep Editing", onPress: () => {}, style: "cancel" },
    //       { text: "Go Home", onPress: () => this.props.navigation.goBack() },
    //     ],
    //     { cancelable: false },
    //   );
    //   return true;
    // }
  
    // return false;
    this.props.navigation.replace('ProjectSearchScreen',{uid:uid})
  };

  componentWillUnmount() {
    this.didFocus.remove();
    this.willBlur.remove();
    BackHandler.removeEventListener("hardwareBackPress", this.onBack);
  }

  render() {
    return this.props.children;
  }
}

export default withNavigation(HandleBack);