/* Project bids pending page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/bids-pending.png
 */

import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  AsyncStorage,
  Button,
  ActivityIndicator,
  Platform,
  Dimensions,
  FlatList,
  ScrollView,
  Linking,
  Modal,
  StatusBar,
  BackHandler
} from "react-native";

import { SegmentedControls } from "react-native-radio-buttons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import firebase from "react-native-firebase";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Footer from "../footer/footer";
import CommonTasks from "../../common-tasks/common-tasks";
import {
  CachedImage,
} from 'react-native-cached-image';
import { NavigationActions, StackActions } from 'react-navigation';


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const italicText = "SF-Pro-Display-LightItalic";
const screenSize = Dimensions.get("window");
const db = firebase.firestore();

const options = ["Pending", "Hired", "Completed", "Lost"];

export default class BidsPending extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uid: this.props.navigation.state.params.uid,
      selectedOption: this.props.navigation.state.params.selectedOption==''?'Pending':this.props.navigation.state.params.selectedOption,
      bidData: ["1", "2", "3", "4"],
      data: [],
      flagUserData:0,
      isLoading: false,
      isLoadingHired: false,
      isLoadingCompleted: false,
      isLoadingLost: false,
      id: "192",
      data_id: [],
      pendingData: [],
      pendingDataLength:0,
      userData: [],
      pendingDataMod: [],
      serviceData: CommonTasks.serviceList,
      reviewModal : false,
      completeModal : false,
      completeModalData: null,
      reviewModalData: null,
      pendingInfo: [],
      hiredInfo: [],
      completedInfo: [],
      lostInfo: [],
    };
  }

  componentDidMount() {
    this._onget();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = () => {
    this.props.navigation.pop();
     return true;
   }

  componentWillMount() {
    //this._onget();
  }

  _stopLoading() {
      //console.log('details'+JSON.stringify(this.state.pendingDataMod))
    this.setState({ isLoading: false });
  }

  _getServiceName(id) {
    for (var i = 0; i < this.state.serviceData.length; i++) {
      if (this.state.serviceData[i].id == id) {
        return (
          <Text style={styles.titleText}>{this.state.serviceData[i].name}</Text>
        );
      }
    }
  }


  async _onget() {
    this.setState({ isLoading: true });
    
      await db
        .collection("place_bid")
        .where("bidderId", "==", this.state.uid)
        //.where("projectStatus", "==", "Pending")
        .get()
        .then(data => {
          var dbData = [];

          data.forEach(doc => {
            var Bdata = doc.data();
            //console.log("loop :  " + doc.data().ownerId);

            dbData.push(Bdata);
          });
          if(dbData.length!=0)
          {
            this.setState({
            pendingData: dbData,
            pendingDataLength: dbData.length
            },this._getOwnerName.bind(this));
          }
          else { 
            this.setState({ isLoading: false });
          }
             
        })
        .catch(err => {
          this.setState({ isLoading: false });
          console.log('onget : '+err);
        });
    
  }

  
  _showDate(item) {

    var dateObject = new Date(Date.parse(item.createdAt));

    var dateReadable = dateObject.toDateString().trim();

    var timecreated="";
    if(item.createdTime!=undefined)
    {
        timecreated=item.createdTime
    }
    return (
      <View >
        <Text style={styles.timeText}>{dateReadable}   {timecreated}</Text>

      </View>
    );
  }



  _getOwnerName() {
    var dbData = [];
    var pData = this.state.pendingData;

     pData.forEach(data => {
          var name
        db.collection("users").doc(data.ownerId).get()
          .then(doc => {
            //console.log("_Owner Id :  " + data.ownerId);
            if (doc.exists) {
                var userData=data
                
                name = doc.data().firstname.split(' ')
                userData.ownerName=name[1]? name[0]+' '+name[1].charAt(0).toUpperCase():name[0]?name[0]:'Customer';
                userData.ownerName1=doc.data().firstname;
              if(doc.data().imageLink || doc.data().imageLink!='')
                userData.imageLink=doc.data().imageLink;
              
              dbData.push(userData);
              //console.log('hi count '+dbData.length);
              if(dbData.length==this.state.pendingDataLength)
                {
                    this.setState({
                        pendingData: dbData,
                        
                    },this._getProjectDetails.bind(this));
                    //console.log('hi count ');
                }
            }
             else {
                var userData=data
                name = 'Customer';
                //console.log(name);
                userData.ownerName=name;
                userData.ownerName1=name;
                dbData.push(userData);
               // console.log('hi count '+dbData.length);
                if(dbData.length==this.state.pendingDataLength)
                {
                    this.setState({
                        pendingData: dbData,
                        
                    },this._getProjectDetails.bind(this));
                   // console.log('hi count ');
                }
            }
          })
          .catch(err => {
            console.log(err);
            var userData=data
            name = 'Customer';
            //console.log(name);
            userData.ownerName=name;
            userData.ownerName1=name;
            dbData.push(userData);
           // console.log('hi count '+dbData.length);
            if(dbData.length==this.state.pendingDataLength)
            {
                this.setState({
                    pendingData: dbData,
                    
                },this._getProjectDetails.bind(this));
                //console.log('hi count ');
            }
          });
          
          
          
      })
      
      
      
    
  }

  

  _getProjectDetails() {
    var dbData = [];
    var pData = this.state.pendingData;
    console.log('hi');
     pData.forEach(data => {
          var proList
        db.collection("projects").doc(data.projectId).get()
          .then(doc => {
            //console.log("project Id : " + data.projectId);
            if (doc.exists) {
                var projectData=data
                
                proList = doc.data().bidderIdList;
              //console.log('bidder list : '+proList);
              projectData.bidderList=proList;
              projectData.postalCode=doc.data().postalCode;
              projectData.projectCreatedAt=doc.data().createdAt;
              projectData.Pimage=doc.data().Pimage;
              if(projectData.Pimage)
                {
                    if(projectData.Pimage.length==0)
                    {
                      projectData.Pimage=null;
                    }
                }
                else{
                  projectData.Pimage=null;
                }

                var dateValue =new Date(projectData.projectCreatedAt)
                
                var hours = dateValue.getHours();
                var minutes = dateValue.getMinutes();
                var ampm = hours >= 12 ? 'pm' : 'am';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                hours = hours < 10 ? '0'+hours : hours;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                
                projectData.projectTimeString=strTime;

              dbData.push(projectData);
              if(dbData.length==this.state.pendingDataLength)
                {
          //dbData=dbData.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));       
          //dbData=dbData.sort((a,b) => (a.createdTimeStamp > b.createdTimeStamp) ? 1 : ((b.createdTimeStamp > a.createdTimeStamp) ? -1 : 0));       
                    this.setState({
                        pendingData: dbData,
                        
                    },this._divideArray.bind(this));
                    //console.log('hi count ');
                }
            }
             else {
                var projectData=data
                proList = [data.bidderId];
               //console.log('bidder list : '+proList);
                projectData.bidderList=proList;
                dbData.push(projectData);
                if(dbData.length==this.state.pendingDataLength)
                {
          //dbData=dbData.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));        
          //dbData=dbData.sort((a,b) => (a.createdTimeStamp > b.createdTimeStamp) ? 1 : ((b.createdTimeStamp > a.createdTimeStamp) ? -1 : 0));       
                   this.setState({
                        pendingData: dbData,
                        
                    },this._divideArray.bind(this));
                    //console.log('hi count ');
                }
            }
          })
          .catch(err => {
            console.log(err);
            var projectData=data
            proList = [data.bidderId];
            //console.log('bidder list : '+proList);
            projectData.bidderList=proList;
            dbData.push(projectData);
            if(dbData.length==this.state.pendingDataLength)
                {
                  
          //dbData=dbData.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));
          //dbData=dbData.sort((a,b) => (a.createdTimeStamp > b.createdTimeStamp) ? 1 : ((b.createdTimeStamp > a.createdTimeStamp) ? -1 : 0));       
                    this.setState({
                        pendingData: dbData,
                        
                    },this._divideArray.bind(this));
                    //console.log('hi count ');
                }
          });
          
      });
  }



  _divideArray() {
    var mainData= this.state.pendingData;
    var pendingDb= [];
    var hiredDb= [];
    var completeDb= [];
    var lostDb= [];
    var i=0      
    mainData.forEach(data => {
      if(data.projectStatus=='Pending' || data.projectStatus=='NI')
      {
        pendingDb.push(data);
      }
      else if(data.projectStatus=='Hired' || (data.projectStatus=='Completed' && !data.reviewedByPro))
      {
        hiredDb.push(data);
      }
      else if(data.projectStatus=='Completed' && data.reviewedByPro=='y')
      {
        completeDb.push(data);
      }
      else if(data.projectStatus=='Lost')
      {
        lostDb.push(data);
      }
      i+=1;
      if(i==this.state.pendingDataLength) {
        pendingDb=pendingDb.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));       
        hiredDb=hiredDb.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));       
        completeDb=completeDb.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));       
        lostDb=lostDb.sort((a,b) => (b.createdTimeStamp > a.createdTimeStamp) ? 1 : ((a.createdTimeStamp > b.createdTimeStamp) ? -1 : 0));       
        
        this.setState({
          pendingInfo: pendingDb,
          hiredInfo: hiredDb,
          completedInfo: completeDb,
          lostInfo: lostDb,
      },this._stopLoading.bind(this));  
      }

    })
  }


 _gotoEditBidScreen(item){
  //console.log('\n bidder : '+JSON.stringify(item))
  //  console.log('\n projectId: '+item.projectId);
  //  console.log('\n ownerId: '+item.ownerId);
  //  console.log('\n bidderId: '+this.state.uid);
  //  console.log('\n description: '+item.description);
  //  console.log('\n serviceId: '+item.serviceId);
  //  console.log('\n bidPrice: '+item.bidPrice);
  //  console.log('\n createdAt: '+item.projectCreatedAt);
  //  console.log('\n bidderIdList: '+item.bidderList);
  //  console.log('\n imageLink: '+item.imageLink);
  //  console.log('\n ownerName: '+item.ownerName);
  //  console.log('\n postalCode: '+item.postalCode);
  //  console.log('\n timeString: '+item.projectTimeString);
  //  console.log('\n Pimage: '+item.Pimage);

  this.props.navigation.replace('ProjectEditBidScreen',
  {
      projectId: item.projectId,
      ownerId: item.ownerId,
      bidderId: this.state.uid,
      description: item.description,
      serviceId: item.serviceId,
      bidPrice: item.bidPrice,
      createdAt: item.projectCreatedAt,
      bidderIdList: item.bidderList,
      imageLink : item.imageLink,
      ownerName: item.ownerName,
      postalCode : item.postalCode,
      timeString: item.projectTimeString,
      Pimage : item.Pimage,
      navi : 'bp',
      projectStatus : item.projectStatus

  });
  
 }


 _gotoCustomerProfile(item)
 {
   this.props.navigation.navigate('CustomerProfileScreen', {profileId: item.ownerId, profileName : item.ownerName,profileImage: item.imageLink})
 }

 _gotoCustomerProfile1(item)
 {
   this.props.navigation.navigate('CustomerProfileScreen', {profileId: item.ownerId, profileName : item.ownerName1,profileImage: item.imageLink})
 }



  _getPendingData() {
    //console.log(this.state.pendingDataMod[0])
    //if(this.state.flagUserData==1 )
   if(this.state.isLoading==false ){
   return (
      <FlatList
        data={this.state.pendingInfo}
        
        renderItem={({ item }) => (
          <View style={{width:screenSize.width}}>
            <View>
            <View style={styles.itemContainer}>
            <TouchableOpacity onPress={()=>this._gotoEditBidScreen(item)}>
              <View style={styles.imageHeadingContainer}>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity onPress={() => this._gotoCustomerProfile(item)}>
                    <CachedImage
                      source={item.imageLink?{
                        uri:
                          item.imageLink
                      }:
                      require('../../assets/images/userOwner.png')}
                      style={styles.avatorImage}
                    />
                  </TouchableOpacity>
                  <View style={{ marginLeft: 10 }}>
                    <Text style={styles.titleText}>
                    <Text onPress={() => this._gotoCustomerProfile(item)}>{item.ownerName}</Text>&nbsp;
                      <Text style={{ fontSize:14, fontWeight:'normal' }}>requested</Text>&nbsp;
                      {this._getServiceName(item.serviceId)} <Text style={{color : 'grey'}}>{item.postalCode?'in '+item.postalCode : null}</Text>
                    </Text>
                    {this._showDate(item)} 
                  </View>
                </View>
                <Text style={styles.descriptionText}>{item.description}</Text>
              </View>
            </TouchableOpacity>
              
            </View>
            <View style={styles.bidContainer}>
              <View
                style={{ flexDirection: "row", justifyContent: "flex-start" }}
              >
                <Text style={styles.bidText}>{item.bidderList.length} bids  </Text>
                <TouchableOpacity onPress={() => this.gotoProjectMessage(item)}>
                  <View style={{flexDirection : 'row'}}>
                  <MaterialIcons name="message"  style={{fontSize: 20,marginTop : 2}} color='#007aff'/>
                  <Text style={styles.messageText}> Send Message</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: "flex-end" }}>
                <Text style={styles.priceText}>${item.bidPrice}.00</Text>
              </View>
            </View>
            </View>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    );    
   }
    
   
  }



  _showCompleteModal(item)
  {
    this.setState({
      completeModal: true,
      completeModalData: item
    })
    
  }

  // _completeProject1() {
  //   console.log("bidId : "+this.state.completeModalData.projectId+'_'+this.state.completeModalData.bidderId);
  //   console.log("project id : "+this.state.completeModalData.projectId);
  //   console.log("bidder list : "+this.state.completeModalData.bidderList);
  //   db
  //   .collection("place_bid")
  //   .where("projectId", "==", this.state.completeModalData.projectId)
  //   .get()
  //       .then(data => {
  //         var datalength = 0;
  //         data.forEach(doc => {
  //           datalength=datalength+1;
  //         })
  //         console.log("length :  " + datalength);
  //         data.forEach(doc => {
  //           console.log("loop :  " + doc.data().projectStatus);
  //           console.log("loop :  " + doc.data().bidderId);
  //           console.log("loop :  " + doc.data().projectId);

  //           // if(doc.data().projectStatus=='Pending' || doc.data().projectStatus=='NI')
  //           // {
  //           //   db.collection("place_bid").doc(doc.data().projectId + "_" +doc.data().bidderId)
  //           //   .update({
  //           //   projectStatus: "Lost"
  //           //   }).then((user) => {
              
  //           //   }).catch((error) => {
  //           //   this.setState({ isLoading: false });
  //           //   console.log("user error for pending" + error);
  //           //   })
  //           // }
  //         });
          
             
  //       })
  //       .catch(err => {
  //         console.log('onget : '+err);
  //       })
  // }
  

//   _completeProject() {
//     this.setState({ completeModal: false, isLoading: true });
//     console.log("bidId"+this.state.completeModalData.projectId+'_'+this.state.completeModalData.bidderId);
//     console.log("project id"+this.state.completeModalData.projectId);

//     db.collection("place_bid").doc(this.state.completeModalData.projectId+'_'+this.state.completeModalData.bidderId)
//         .update({
//             projectStatus: "Completed"
//         }).then((success) => {
//             db.collection("projects").doc(this.state.completeModalData.projectId).update({
//                 projectStatus: "Completed",
//                 completedBy : 'bidder',
//                 hiredId : this.state.completeModalData.bidderId
//             }).then((user) => {
              
//                 CommonTasks._displayToast("This project has been completed");
//                 this._onget();
//             }).catch((error) => {
//                 this.setState({ isLoading: false });
//                 console.log("user error" + error)
//             })
//         }).catch((error) => {
//             this.setState({ isLoading: false });
//             console.error("Error completing  document: ", error);
//             this._onget();
//         });

// }
_completeProject() {
  this.setState({ completeModal: false});
  this.props.navigation.navigate('ProjectReviewScreen',
          {
              projectId: this.state.completeModalData.projectId,
              projectDescription: this.state.completeModalData.description,
              projectOwner: this.state.completeModalData.ownerId,
              formReviewId:this.state.uid,
              serviceId: this.state.completeModalData.serviceId,
              createdAt: this.state.completeModalData.projectCreatedAt,
              imageLink :this.state.completeModalData.imageLink,
              ownerName: this.state.completeModalData.ownerName1,
              postalCode : this.state.completeModalData.postalCode,
              timeString: this.state.completeModalData.projectTimeString,
          });
  // console.log("bidId"+this.state.completeModalData.projectId+'_'+this.state.completeModalData.bidderId);
  // console.log("project id"+this.state.completeModalData.projectId);

  // db.collection("place_bid").doc(this.state.completeModalData.projectId+'_'+this.state.completeModalData.bidderId)
  //     .update({
  //         projectStatus: "Completed"
  //     }).then((success) => {
  //         db.collection("projects").doc(this.state.completeModalData.projectId).update({
  //             projectStatus: "Completed",
  //             completedBy : 'bidder',
  //             hiredId : this.state.completeModalData.bidderId
  //         }).then((user) => {
            
  //             CommonTasks._displayToast("This project has been completed");
  //             this._onget();
  //         }).catch((error) => {
  //             this.setState({ isLoading: false });
  //             console.log("user error" + error)
  //         })
  //     }).catch((error) => {
  //         this.setState({ isLoading: false });
  //         console.error("Error completing  document: ", error);
  //         this._onget();
  //     });

}

  _getHiredData() {
    //console.log(this.state.pendingDataMod[0])
    //if(this.state.flagUserData==1 )
   if(this.state.isLoading==false ){
   return (
      <FlatList
        data={this.state.hiredInfo}
        
        renderItem={({ item }) => (
          <View style={{width:screenSize.width,elevation : 3}}>
         
            <View style={{width:screenSize.width,elevation : 3}}>
            <View style={styles.itemContainer}>
              <View style={styles.imageHeadingContainer}>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity onPress={() => this._gotoCustomerProfile1(item)}>
                    <CachedImage
                      source={item.imageLink?{
                        uri:
                          item.imageLink
                      }:
                      require('../../assets/images/userOwner.png')}
                      style={styles.avatorImage}
                    />
                  </TouchableOpacity>
                  <View style={{ marginLeft: 10 }}>
                    <Text style={styles.titleText}>
                    <Text onPress={() => this._gotoCustomerProfile1(item)}>{item.ownerName1}</Text>&nbsp;
                      <Text style={{ fontSize:14, fontWeight:'normal' }}>requested</Text>&nbsp;
                      {this._getServiceName(item.serviceId)} <Text style={{color : 'grey'}}>{item.postalCode?'in '+item.postalCode : null}</Text>
                    </Text>
                    {this._showDate(item)} 
                  </View>
                </View>
                <Text style={styles.descriptionText}>{item.description}</Text>
              </View>
            </View>
            <View style={styles.bidContainer}>
              <View
                style={{ flexDirection: "row", justifyContent: "flex-start" }}
              >
                <TouchableOpacity onPress={() => this.gotoProjectMessage1(item)}>
                  <View style={{flexDirection : 'row'}}>
                  <MaterialIcons name="message"  style={{fontSize: 20,marginTop : 2}} color='#007aff'/>
                  <Text style={styles.messageText}> Send Message</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'center',flexDirection : 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/tick.png')}
                   style={{height: 22,width : 22, marginRight : 8 }} /> 
                <Text style={styles.priceText}>${item.bidPrice}.00</Text>
                <MaterialCommunityIcons onPress={() => this._showCompleteModal(item)} name="dots-vertical" size={20} style={ { color: '#1e1e1e', marginRight: 5,paddingLeft:10}} />
              </View>
            </View>
            </View>
          
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    );    
   }
    
   
  }



  _getCompletedData() {
    debugger;
    //console.log(this.state.pendingDataMod[0])
    //if(this.state.flagUserData==1 )
   if(this.state.isLoading==false ){
   return (
      <FlatList
        data={this.state.completedInfo}
        
        renderItem={({ item }) => (
          <View style={{width:screenSize.width}}>
          
            <View>
            <View style={styles.itemContainer}>
              <View style={styles.imageHeadingContainer}>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity onPress={() => this._gotoCustomerProfile1(item)}>
                    <CachedImage
                      source={item.imageLink?{
                        uri:
                          item.imageLink
                      }:
                      require('../../assets/images/userOwner.png')}
                      style={styles.avatorImage}
                    />
                  </TouchableOpacity>
                  <View style={{ marginLeft: 10 }}>
                    <Text style={styles.titleText}>
                    <Text onPress={() => this._gotoCustomerProfile1(item)}>{item.ownerName1}</Text>&nbsp;
                      <Text style={{ fontSize:14, fontWeight:'normal' }}>requested</Text>&nbsp;
                      {this._getServiceName(item.serviceId)} <Text style={{color : 'grey'}}>{item.postalCode?'in '+item.postalCode : null}</Text>
                    </Text>
                    {this._showDate(item)} 
                  </View>
                </View>
                <Text style={styles.descriptionText}>{item.description}</Text>
              </View>
            </View>
            <View style={styles.bidContainer}>
              <View
                style={{ flexDirection: "row", justifyContent: "flex-start" }}
              >
               <TouchableOpacity onPress={() => this.gotoProjectMessage1(item)}>
                  <View style={{flexDirection : 'row'}}>
                  <MaterialIcons name="message"  style={{fontSize: 20,marginTop : 2}} color='#007aff'/>
                  <Text style={styles.messageText}>Send Message</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'center',flexDirection : 'row', alignItems: 'center' }}>
              <Image source={require('../../assets/images/tick.png')}
                   style={{height: 22,width : 22, marginRight : 8 }} /> 
                <Text style={styles.priceText}>${item.bidPrice}.00</Text>
                {/* <MaterialCommunityIcons onPress={() => this._showReviewModal(item)} name="dots-vertical" size={20} style={ { color: '#1e1e1e', marginRight: 5,paddingLeft:10}} /> */}
              </View>
            </View>
            </View>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    );    
   }
    
   
  }



  gotoProjectMessage(item) {
    //console.log(JSON.stringify(item))
    this.props.navigation.navigate('ProjectMessageScreen',
        {
            ProjectOwnerName:item.ownerName,
            Uid:this.state.uid,
            toChatId:item.ownerId,
            fromChatId:this.state.uid,
            navigateFrom:"bids_pending",
            message_status:"inbox",
            imageLink :item.imageLink,
            project_id : item.projectId,
            
        });
}

gotoProjectMessage1(item) {
  console.log(JSON.stringify(item))
  this.props.navigation.navigate('ProjectMessageScreen',
      {
        ProjectOwnerName:item.ownerName1,
        Uid:this.state.uid,
        toChatId:item.ownerId,
        fromChatId:this.state.uid,
        navigateFrom:"message_listings",
        message_status:"inbox",
        imageLink :item.imageLink,
        project_id : item.projectId,
      });
}



  _showReviewModal(item)
  {
    this.setState({
      reviewModal: true,
      reviewModalData: item
    })
    //console.log(JSON.stringify(item))
    
  }



  _getLostData() {
    console.log(this.state.pendingDataMod[0])
    //if(this.state.flagUserData==1 )
   if(this.state.isLoading==false ){
   return (
      <FlatList
        data={this.state.lostInfo}
        
        renderItem={({ item }) => (
          <View style={{width:screenSize.width}}>
          
            <View>
            <View style={styles.itemContainer}>
              <View style={styles.imageHeadingContainer}>
                <View style={{ flexDirection: "row" }}>
                  <TouchableOpacity onPress={() => this._gotoCustomerProfile(item)}>
                    <CachedImage
                      source={item.imageLink?{
                        uri:
                          item.imageLink
                      }:
                      require('../../assets/images/userOwner.png')}
                      style={styles.avatorImage}
                    />
                  </TouchableOpacity>
                  <View style={{ marginLeft: 10 }}>
                    <Text style={styles.titleText}>
                    <Text onPress={() => this._gotoCustomerProfile(item)}>{item.ownerName}</Text>&nbsp;
                      <Text style={{ fontSize:14, fontWeight:'normal' }}>requested</Text>&nbsp;
                      {this._getServiceName(item.serviceId)} <Text style={{color : 'grey'}}>{item.postalCode?'in '+item.postalCode : null}</Text>
                    </Text>
                    {this._showDate(item)} 
                  </View>
                </View>
                <Text style={styles.descriptionText}>{item.description}</Text>
              </View>
            </View>
            <View style={styles.bidContainer}>
              <View
                style={{ flexDirection: "row", justifyContent: "flex-start" }}
              >
              
              </View>
              <View style={{ justifyContent: 'center',flexDirection : 'row', alignItems: 'center' }}>
              
                <Text style={styles.priceLostText}>${item.bidPrice}.00</Text>
              </View>
            </View>
            </View>
          
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    );    
   }
    
   
  }

  setSelectedOption(selectedOption) {
    this.setState({
      selectedOption: selectedOption
    });
  }



  _navigateReviewScreen() { 
    //alert(JSON.stringify(this.state.reviewModalData))
    this.setState({
      reviewModal: false
    });
      this.props.navigation.navigate('ProjectReviewScreen',
          {
              projectId: this.state.reviewModalData.projectId,
              projectDescription: this.state.reviewModalData.description,
              projectOwner: this.state.reviewModalData.ownerId,
              formReviewId:this.state.uid,
              serviceId: this.state.reviewModalData.serviceId,
              createdAt: this.state.reviewModalData.projectCreatedAt,
              imageLink :this.state.reviewModalData.imageLink,
              ownerName: this.state.reviewModalData.ownerName1,
              postalCode : this.state.reviewModalData.postalCode,
              timeString: this.state.reviewModalData.projectTimeString,
          });
  
  }

  render() {
    return (
      
      <View style={styles.container}>
        <View style={styles.optionContainer}>
          <SegmentedControls
            tint={"#3A4958"}
            selectedTint={"white"}
            backTint={"#fff"}
            options={options}
            allowFontScaling={false} // default: true
            onSelection={this.setSelectedOption.bind(this)}
            selectedOption={this.state.selectedOption}
            optionStyle={{ fontFamily: regularText, fontSize: 16 }}
            optionContainerStyle={{ flex: 1 }}
          />
        </View>


        
        <ScrollView style={{ bottom: 60, top: 63, position: "absolute" }}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.reviewModal}
          onRequestClose={() => this.setState({reviewModal: false})}
          style = {{height : 300, width: 300}}
          transparent = {true}>
          <View style={styles.modalview}>

            <View style={styles.modalviewoptions}>

              <View style={[styles.modalviewoptions1]}>
                <Text style={{color: '#999999',fontSize: 12,fontFamily: 'SF-Pro-Display-Regular'}}>Project Actions</Text>
              </View>

              

              <View style={[ styles.commonstyleoption]}>
                <TouchableOpacity onPress={() => this._navigateReviewScreen()} style ={{padding :15}}>
                  <Text style={{color: '#3A4958',fontSize:22,fontFamily: 'SF-Pro-Display-Regular'}}>Review Project</Text>
                </TouchableOpacity>
              </View>

            </View>


            <View style = {styles.modalviewcancel}>
             

              <TouchableOpacity onPress={()=>this.setState({reviewModal: false})}  style ={{padding :15}}>
                <Text style = {{fontSize : 20,color : 'red',fontFamily: 'SF-Pro-Display-Regular'}}>Cancel</Text>
              </TouchableOpacity>

            </View>
            
          </View>
        </Modal>


        {/* complete Modal */}
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.completeModal}
          onRequestClose={() => this.setState({completeModal: false})}
          style = {{height : 300, width: 300}}
          transparent = {true}>
          <View style={styles.modalview}>

            <View style={styles.modalviewoptions}>

              <View style={[styles.modalviewoptions1]}>
                <Text style={{color: '#999999',fontSize: 12,fontFamily: 'SF-Pro-Display-Regular'}}>Project Actions</Text>
              </View>

              

              <View style={[ styles.commonstyleoption]}>
                <TouchableOpacity onPress={() => this._completeProject()} style ={{padding :15}}>
                  <Text style={{color: '#3A4958',fontSize:22,fontFamily: 'SF-Pro-Display-Regular'}}>Complete Project</Text>
                </TouchableOpacity>
              </View>

            </View>


            <View style = {styles.modalviewcancel}>
             

              <TouchableOpacity onPress={()=>this.setState({completeModal: false})}  style ={{padding :15}}>
                <Text style = {{fontSize : 20,color : 'red',fontFamily: 'SF-Pro-Display-Regular'}}>Cancel</Text>
              </TouchableOpacity>

            </View>
            
          </View>
        </Modal>
          {this.state.selectedOption == "Pending" ?
             this._getPendingData()
            : null}

          {this.state.selectedOption == "Hired" ? 
            this._getHiredData()
           :
            null}

          {this.state.selectedOption == "Completed" ?
          this._getCompletedData()
          :
           null}

          {this.state.selectedOption == "Lost" ? 
          this._getLostData()
           :
            null}
        </ScrollView>
        
        {this.state.isLoading == true ? (
          <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
          <View style={styles.activity_sub}>
            <ActivityIndicator
              size="large"
              color="#D0D3D4"
              style={{
                justifyContent: "center",
                alignItems: "center",
                height: 50
              }}
            />
          </View>  
          </View>
          
        ) : null}
        
        <Footer itemColor="hammer" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  modalview : { 
    height : screenSize.height,
    width: screenSize.width ,
    backgroundColor:'rgba(200, 200, 200, .6)',
  },
  modalviewoptions : {
    width : screenSize.width-30,
    height : 120,
    marginLeft : 15,
    backgroundColor : 'white',
    position : 'absolute',
    bottom : 180,
    borderRadius : 15
  },
  commonstyleoption :{
    alignItems : 'center',
    justifyContent : 'center',
    height: 70,
    borderTopWidth:1,
    borderTopColor : '#cccccc'
  },
  modalviewoptions1 :{
    alignItems : 'center',
    justifyContent : 'center',
    height: 45,
  },

  optionContainer: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    backgroundColor: "#fff",
    elevation : 2
  },

  itemContainer: {
    backgroundColor: "white",
    marginTop: 10,
    padding: 10
  },

  avatorImage: {
    width: 70,
    height: 70,
    borderRadius: 35
  },
  timeText: {
    fontFamily: regularText,
    fontSize: 16,
    marginTop: 10
  },
  modalviewcancel : {
    alignItems : 'center',
    justifyContent : 'center',
    width : screenSize.width-30,
    height : 60,
    borderRadius : 15,
    marginLeft : 15,
    position : 'absolute',
    bottom : 100,
    backgroundColor : 'white'
  },

  titleText: {
    fontFamily: regularText,
    fontWeight: "bold",
    color: "#1e1e1e",
    fontSize: 16,
    maxWidth: screenSize.width - 95
  },

  timeText: {
    fontFamily: regularText,
    fontSize: 16,
    marginTop: 10
  },

  descriptionText: {
    fontFamily: regularText,
    fontSize: 16,
    marginVertical: 10,
    color: "#1e1e1e"
  },

  bidContainer: {
    backgroundColor: "#fff",
    marginTop: 1,
    flexDirection: "row",
    padding: 10,
    justifyContent: "space-between",
    height : 40,
    elevation : 2
  },

  bidText: {
    fontFamily: regularText,
    color: "#1e1e1e",
    fontSize: 16
  },

  messageText: {
    fontFamily: boldText,
    color: "#1e1e1e",
    fontSize: 16,
  },

  priceText: {
    fontFamily: boldText,
    color: "#4cd964",
    fontSize: 16
  },
  priceLostText: {
    fontFamily: boldText,
    color: "#aaa",
    fontSize: 16
  },
  activity_sub: {
    position: "absolute",
    top: screenSize.height / 2,
    backgroundColor: "black",
    width: 50,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 10,
    //elevation:5,
    ...Platform.select({
      android: { elevation: 5 },
      ios: {
        shadowColor: "#999",
        shadowOffset: {
          width: 0,
          height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5
      }
    }),
    height: 50,
    borderRadius: 10
  }
});

