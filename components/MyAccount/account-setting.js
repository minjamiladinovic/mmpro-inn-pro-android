
/* My Account Setting page Screen
LINK : http://template1.teexponent.com/pro-in-your-pocket/design/app/settings.png
 */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    BackHandler,
    StatusBar,
    Switch,
    Alert
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';
import { NavigationActions, StackActions } from 'react-navigation';
import Communications from 'react-native-communications';
import DeviceInfo from 'react-native-device-info';
import Rate, { AndroidMarket } from 'react-native-rate'

var feedbackBodyStr = '';
const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class AccountSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            condensedSwitch: false,
            emailNotification: true,
            user_id: '',
            fcmT: '',
            condensedSwitchInitial: '',
            rated: false
        }
    }


    componentDidMount() {
        AsyncStorage.getItem('condensed_mode').then((value) => value ? this.setState({ condensedSwitch: value == '1' ? true : false, condensedSwitchInitial: value == '1' ? true : false }) : null);
        AsyncStorage.getItem('user_id').then(value => this.setState({ user_id: value }));
        AsyncStorage.getItem('fcmToken').then((value) => this.setState({ fcmT: value }));
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        this._prepareFeedbackBodyStr();
    }

    _gotoProjectSearch() {

        this.props.navigation.replace('ProjectSearchScreen', { uid: this.state.user_id })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {

        if (this.state.condensedSwitch == this.state.condensedSwitchInitial) {
            this.props.navigation.pop();
        }
        else {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'ProjectSearchScreen',
                })],
            });
            this.props.navigation.dispatch(resetAction);
        }
        return true;
    }


    gotoNotiSetting() {
        this.props.navigation.navigate('NotificationSettingsScreen');
    }




    _signout() {
        Alert.alert(
            'Pro In Your Pocket For Pros',
            'Are you sure to logout?',
            [
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Ok', onPress: () => this._confirmSignout() },
            ],
            { cancelable: false }
        )
    }
    _confirmSignout() {
        firebase.auth().signOut().then((success) => {
            console.log('Signed Out');
            AsyncStorage.setItem('user_id', "");
            AsyncStorage.setItem('user_status', "");
            console.log('fcm :  ' + this.state.fcmT);
            firebase.firestore().collection("sp_tokens").doc(this.state.fcmT).delete()
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'SigninConfirmScreen',
                })],
            });
            this.props.navigation.dispatch(resetAction);
        })
            .catch((err) => {
                console.error('Sign Out Error' + err);
            });

    }


    _setCondensedMode(value) {
        {
            value == true ?
                AsyncStorage.setItem('condensed_mode', '1')
                :
                AsyncStorage.setItem('condensed_mode', '0')
        }
        this.setState({ condensedSwitch: value })
    }


    _prepareFeedbackBodyStr() {
        var appVersionOnDevice = DeviceInfo.getVersion();
        var udidOfDevice = DeviceInfo.getUniqueID();
        var modelOfDevice = DeviceInfo.getModel();
        var osVersionOfDevice = DeviceInfo.getSystemVersion();
        var deviceManufacturer = DeviceInfo.getManufacturer();
        feedbackBodyStr = '';
        feedbackBodyStr += 'Please share your feedback or questions with us below (eg - tell us what you like, dislike, etc ):\n\n\n';
        feedbackBodyStr += '------------------\nPlease ignore below part it is for our tracking!\n';
        feedbackBodyStr += 'Model : ' + modelOfDevice + '\n';
        if (Platform.OS === 'ios') {
            feedbackBodyStr += 'iOS Version : ' + osVersionOfDevice + '\n';
        }
        else {
            feedbackBodyStr += 'Android Version : ' + osVersionOfDevice + '\n';
        }
        feedbackBodyStr += 'App Version : ' + appVersionOnDevice + '\n';
        feedbackBodyStr += 'Device : ' + deviceManufacturer + '\n';
        // feedbackBodyStr += '';
        // feedbackBodyStr += '';


    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.signinText} onPress={() => this.handleBackButton()}>Close</Text>
                    <Text style={styles.headerText}>Settings</Text>
                    <TouchableOpacity onPress={() => this._signout()}>
                        <Text style={styles.nextText}>Sign Out</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>FEED</Text>
                </View>

                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>Condensed mode</Text>
                    <Switch
                        onValueChange={(value) => this._setCondensedMode(value)}
                        value={this.state.condensedSwitch} />
                </View>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>NOTIFICATIONS</Text>
                </View>
                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>Email notifications</Text>
                    <Switch
                        onValueChange={(value) => this.setState({ emailNotification: value })}
                        value={this.state.emailNotification} />
                </View>
                <TouchableOpacity style={styles.itemContainer} onPress={() => this.gotoNotiSetting()}>
                    <Text style={styles.itemText}>Notification events</Text>
                    <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                </TouchableOpacity>

                <View style={styles.headingContainer}>
                    <Text style={styles.headingText}>HELP</Text>
                </View>

                <TouchableOpacity activeOpacity={0.7} style={styles.itemContainer}

                    onPress={() => {

                        Alert.alert(
                            'Pro In Your Pocket For Pros',
                            'Do you want to Leave a review ?',
                            [
                                {
                                    text: 'OK', onPress: () => {
                                        let options = {
                                            GooglePackageName: "com.proinyourpocket.pros",
                                            preferredAndroidMarket: AndroidMarket.Google,
                                            preferInApp: false,
                                        }
                                        Rate.rate(options, success => {
                                            if (success) {
                                                this.setState({ rated: true })
                                            }
                                        })
                                    }
                                },
                                {text: 'Cancel', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                        );


                    }}


                >
                    <Text style={styles.itemText}>Leave feedback</Text>
                    <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.7} style={styles.itemContainer}
                    onPress={() => (Platform.OS === 'ios') ? (
                        Communications.email(['support@proinyourpocket.com'], null, null, 'Pro In Your Pocket For Pros iPhone App (' + DeviceInfo.getVersion() + ') - ' + DeviceInfo.getUniqueID(), feedbackBodyStr)
                    ) : (
                            Communications.email(['support@proinyourpocket.com'], null, null, 'Pro In Your Pocket For Pros Android App (' + DeviceInfo.getVersion() + ') - ' + DeviceInfo.getUniqueID(), feedbackBodyStr)
                        )}
                >
                    <Text style={styles.itemText}>Contact us</Text>
                    <Ionicons name="ios-arrow-forward" style={styles.optionImage} />
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: regularText,
        fontSize: 18,
        marginRight: 10
    },

    headingContainer: {
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
    },

    headingText: {
        fontFamily: regularText,
        fontSize: 16
    },

    itemContainer: {
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 10,
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
    },

    itemText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#1e1e1e'
    },

    optionImage: {
        fontSize: 20,
        color: '#8e8e93'
    },
})