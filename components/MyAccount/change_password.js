// Create account page 1
// Link : http://template1.teexponent.com/pro-in-your-pocket/design/app/create-account.png

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    BackHandler
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';
import { NavigationActions, StackActions } from 'react-navigation';



const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedField: '',
            uid: this.props.navigation.state.params.uid,
            email: this.props.navigation.state.params.email,
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            isLoading: false,
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.pop();
         return true;
       }

    gotoNextStep() {
        //this.props.navigation.navigate('CreateAccountStep2Screen');
        this._formValidation();
    }
    _formValidation() {
        if (this.state.oldPassword === '' || this.state.oldPassword.trim() === '') {
            CommonTasks._displayToast("Please Enter Your Current Password");
            this.inputPassword.focus();
            return false;
        }
        if (this.state.newPassword === '' || this.state.newPassword.trim() === '') {
            CommonTasks._displayToast("Please Enter a Password to change");
            this.inputNewPassword.focus();
            return false;
        }
        if (this.state.newPassword.trim() != this.state.confirmPassword.trim()) {
            CommonTasks._displayToast("Please Enter Both Password Sme");
            this.inputConPassword.focus();
            return false;
        }
        this._updatePassword();
    }

    _updatePassword() {
        this.setState({ isLoading: true });
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.oldPassword)
            .then((user) => {

                firebase.auth().currentUser.updatePassword(this.state.newPassword).then((data) => {
                    this.setState({ isLoading: false });
                    CommonTasks._displayToast("Password has changed successfully");
                    this.props.navigation.navigate('SigninConfirmScreen');
                    //Do something

                }).catch(function (err) {
                    //Do something
                    console.log(err);
                    this.setState({ isLoading: false });
                });

            })
            .catch((error) => {
                this.setState({ isLoading: false })
                const { code, message } = error;
                console.log("error" + error);
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode + "-" + errorMessage);
                CommonTasks._displayToast(errorMessage);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Back</Text>
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Change Password</Text>

                    <TouchableOpacity
                        onPress={() => this._formValidation()}>
                        <Text style={styles.nextText}>Save</Text>
                    </TouchableOpacity>

                </View>

                <View style={styles.inputContainer}>
                    <View style={styles.textboxContainer}>
                        <TextInput
                            style={styles.textInput}
                            secureTextEntry={true}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="Old Password"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.inputNewPassword.focus()}
                            onChangeText={(input) => this.setState({ oldPassword: input })}
                            ref={((input) => this.inputPassword = input)}
                            onFocus={() => this.setState({ selectedField: 'Opassword' })}
                        />
                        {this.state.selectedField == 'Opassword' ? (
                            <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => this.inputPassword.clear()} />
                        ) : null
                        }
                    </View>
                    <View style={styles.textboxContainer}>
                        <TextInput
                            style={styles.textInput}
                            secureTextEntry={true}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="New Password"
                            placeholderTextColor="#ababab"
                            returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            onSubmitEditing={() => this.inputConPassword.focus()}
                            onChangeText={(input) => this.setState({ newPassword: input })}
                            ref={((input) => this.inputNewPassword = input)}
                            onFocus={() => this.setState({ selectedField: 'Npassword' })}
                        />
                        {this.state.selectedField == 'Npassword' ? (
                            <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => this.inputNewPassword.clear()} />
                        ) : null
                        }
                    </View>
                    <View style={styles.textboxContainer}>
                        <TextInput
                            style={styles.textInput}
                            secureTextEntry={true}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="Confirm password"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            // onSubmitEditing={() => this.inputPassword.focus()}
                            onChangeText={(input) => this.setState({ confirmPassword: input })}
                            ref={((input) => this.inputConPassword = input)}
                            onFocus={() => this.setState({ selectedField: 'conpassword' })}
                        />
                        {this.state.selectedField == 'conpassword' ? (
                            <MaterialIcons name="cancel" style={styles.cancelIcon} onPress={() => this.inputConPassword.clear()} />
                        ) : null
                        }
                    </View>
                </View>

                {(this.state.isLoading == true) ?
                    (
                        <View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                        <View style={styles.activity_sub}>
                        <ActivityIndicator
                            size="large"
                            color='#D0D3D4'
                            style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                        />
                    </View>    
                        </View>)
                    : null}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff'
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        // borderBottomColor: '#8e8e93',
        // borderBottomWidth: 0.7,
        // paddingLeft : 15,
        // marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17,
        flex: 1
    },

    inputContainer: {
        backgroundColor: 'white',
        marginTop: 20
    },

    textboxContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        marginLeft: 15,
    },

    cancelIcon: {
        fontSize: 20,
        marginRight: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
})