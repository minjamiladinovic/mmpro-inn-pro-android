
/* Home Screen page */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import { NavigationActions, StackActions } from 'react-navigation';

const screenSize = Dimensions.get('window');

export default class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        //alert("hi");
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#1e1e1e"
                    barStyle="light-content"
                />

                <View style={{ alignSelf: 'center',}}>
                    <Image
                        style={{ width: (screenSize.width) / 2 + 50, height: (screenSize.height) / 4, resizeMode: "contain" }}
                        source={require('../../assets/images/pro-banner-black.png')}
                    />
                </View>


                <TouchableOpacity
                    style={{
                        alignSelf: 'center', width: screenSize.width - 40,
                        height: 50, borderRadius: 2, backgroundColor: "#3A4958",
                        justifyContent: "center", alignItems: "center", marginTop: 40
                    }}>

                    <Text style={{ color: "#ffff", fontSize: 15, fontFamily: "SF-Pro-Display-Bold" }}>
                        LOG IN WITH FACEBOOK
                </Text>

                </TouchableOpacity>

                <View style={{ padding: 20, flexDirection: "row", marginTop: 10 }}>

                    <View style={{ width: (screenSize.width - 40) / 2 - 30, borderTopWidth: 1, borderTopColor: "#ababab" }}>
                    </View>

                    <Text style={{ fontFamily: "SF-Pro-Display-Regular", fontSize: 15, marginTop: -10, marginLeft: 10, color: "#ababab" }}>OR</Text>

                    <View style={{ width: (screenSize.width - 40) / 2 - 30, borderTopWidth: 1, borderTopColor: "#ababab", marginLeft: 10 }}>
                    </View>
                </View>

                <TouchableOpacity style={{ alignSelf: "center", marginTop: 10 }}
                onPress={()=>this.props.navigation.navigate('SignUpScreen')}>
                    <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Medium" }}>
                        SIGN UP WITH EMAIL
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ position: 'absolute', bottom: 0, width: screenSize.width, height: 40, borderTopWidth: 1, borderTopColor: "#ababab", justifyContent: "center", alignItems: 'center' }}
                onPress={()=>this.props.navigation.navigate('LoginScreen')}>

                    <Text style={{ color: "#ababab", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>
                        Already have an account ?
                        <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Bold",marginLeft:10}}>
                        &nbsp; Log in
                        </Text>
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center'
        //backgroundColor: '#ffff',
    },
    bgStyle: {
        width: screenSize.width,
        height: screenSize.height,
    },
}); 
