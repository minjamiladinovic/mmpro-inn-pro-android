/* Signup Screen page */

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert
} from 'react-native';

import { NavigationActions, StackActions } from 'react-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import CommonTasks from '../../common-tasks/common-tasks';
import firebase from 'react-native-firebase';

const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_status: "",
            user_id: "",
            email_show: false,
            pass_show: false,
            email: "",
            first_show: false,
            last_show: false,
            email_show: false,
            ph_show: false,
            pass_show: false,
            confirm_show: false,
            firstName: "",
            lastName: "",
            email: "",
            phone: "",
            password: "",
            cPassword: "",
            isLoading:false
        }
    }

    _fnameFocus() {
        this.uFirst.focus();
    }
    _lnameFocus() {
        this.uLast.focus();
    }
    _emailFocus() {
        this.uEmail.focus();
    }
    _phoneFocus() {
        this.uPhone.focus();
    }
    _passFocus() {
        this.uPass.focus();
    }

    _cPassfocus() {
        this.cPass.focus();
    }

    _formValidation() {

        if (this.state.firstName === '' || this.state.firstName.trim() === '') {
            this.setState({ first_show: true }, this.firstName_check.bind(this));
            return false;
        }
        if (this.state.lastName === '' || this.state.lastName.trim() === '') {
            this.setState({ last_show: true }, this.lastName_check.bind(this));
            return false;
        }

        if (this.state.email === '' || this.state.email.trim() === '') {
            this.setState({ email_show: true }, this.email_check.bind(this));
            return false;
        }
        else {
            var response = CommonTasks._verifyEmail(this.state.email);
            if (response === false) {
                CommonTasks._displayToast("Please enter valid email address");
                this.uEmail.focus();
                return false;
            }
        }

        if (this.state.phone === '' || this.state.phone.trim() === '') {
            this.setState({ ph_show: true }, this.ph_check.bind(this));
            return false;
        }

        if (this.state.password === '' || this.state.password.trim() === '') {
            this.setState({ pass_show: true }, this.pass_check.bind(this));
            return false;
        }

        if (this.state.cPassword.trim() != this.state.password.trim()) {
            this.setState({ confirm_show: true }, this.cpass_check.bind(this));
            return false;
        }

        this.handleSignUp();
    }

    handleSignUp = () => {
        this.setState({isLoading:true})
        firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((data) => 
            {
                //this.setState({isLoading:false});
                console.log(JSON.stringify(data));
                console.log(data.user.uid);
                db.collection("users").doc(data.user.uid).set({
                    firstname :this.state.firstName,
                    lastName:this.state.lastName,
                    email:this.state.email,
                    phone:this.state.phone,
                    password:this.state.password,
                    uid:data.user.uid
                }).then((user)=>{
                    this.setState({isLoading:false});
                    console.log("user data"+user);
                }).catch((error)=>console.log("user error"+error))
                // Alert.alert(
                //     'Pro In Your Pockets For Pros',
                //      'You have successfully Registered With Pro In Your Pockets For Pros',
                //     [
                //         { text: 'OK', onPress: () => this.props.navigation.replace('LoginScreen') },
                //     ],
                //     { cancelable: false }
                // );
            })
            .catch((error) => 
                {
                    this.setState({isLoading:false});
                    console.log(error);
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    console.log(errorCode + "-" + errorMessage);
                    CommonTasks._displayToast(errorMessage);
                })
    }


    firstName_check() {
        CommonTasks._displayToast("Please Enter Your First Name");
        this.uFirst.focus();
    }
    lastName_check() {
        CommonTasks._displayToast("Please Enter Your Last Name");
        this.uLast.focus();
    }

    email_check() {
        CommonTasks._displayToast("Please Enter Your email id");
        this.uEmail.focus();
    }
    ph_check() {
        CommonTasks._displayToast("Please Enter Your email id");
        this.uPhone.focus();
    }
    pass_check() {
        CommonTasks._displayToast("Please Enter Your Password");
        this.uPass.focus();
    }
    cpass_check() {
        CommonTasks._displayToast("Please Enter Same Password");
        this.cPass.focus();
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#1e1e1e"
                    barStyle="light-content"
                />
                <View style={styles.home_header}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={{ marginTop: 10 }}
                    >
                        <MaterialCommunityIcons name='arrow-left' color='#fff' size={30} />
                    </TouchableOpacity>

                    <Text style={{ marginLeft: 40, color: "#ffff", fontSize: 20, fontFamily: "SF-Pro-Display-Bold", marginTop: 10 }}>Sign Up</Text>
                </View>


                <ScrollView keyboardShouldPersistTaps="always">
                    <View style={{ marginTop: Platform.OS === 'ios' ? 90 : 70, padding: 20 }}>

                        <View style={{ width: screenSize.width - 40, flexDirection: 'row' }}>
                            <View style={{ marginTop: 10 }}>
                                <FontAwesome name='user' color='#1e1e1e' size={35} />
                            </View>
                            {(this.state.first_show == true) ?
                                (
                                    <View style={[styles.mainText]}>
                                        <View style={{ backgroundColor: "#ffff", position: "absolute", left: 10, bottom: 39 }}>
                                            <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>First name </Text>
                                        </View>

                                        <TextInput
                                            style={styles.textInput}
                                            keyboardType='email-address'
                                            underlineColorAndroid="transparent"
                                            placeholder="First Name"
                                            placeholderTextColor="#ababab"
                                            //returnKeyType='next'
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            //onSubmitEditing={() => this.uEmail.focus()}
                                            onChangeText={(input) => this.setState({ firstName: input })}
                                            ref={((input) => this.uFirst = input)}
                                        >
                                        </TextInput>
                                    </View>
                                )
                                :
                                (<TouchableOpacity style={styles.textBoxTouch}
                                    onPress={() => this.setState({ first_show: true }, this._fnameFocus.bind(this))}
                                >
                                    <Text style={styles.placeHolder}>First Name </Text>
                                </TouchableOpacity>)}
                        </View>

                        <View style={{ marginTop: 10, marginLeft: 25 }}>

                            {(this.state.last_show == true) ?
                                (
                                    <View style={[styles.mainText]}>
                                        <View style={{ backgroundColor: "#ffff", position: "absolute", left: 10, bottom: 39 }}>
                                            <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>Last name </Text>
                                        </View>

                                        <TextInput
                                            style={styles.textInput}
                                            keyboardType='email-address'
                                            underlineColorAndroid="transparent"
                                            placeholder="Last Name"
                                            placeholderTextColor="#ababab"
                                            //returnKeyType='next'
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            //onSubmitEditing={() => this.uEmail.focus()}
                                            onChangeText={(input) => this.setState({ lastName: input })}
                                            ref={((input) => this.uLast = input)}
                                        >
                                        </TextInput>
                                    </View>
                                )
                                :
                                (<TouchableOpacity style={styles.textBoxTouch}
                                    onPress={() => this.setState({ last_show: true }, this._lnameFocus.bind(this))}
                                >
                                    <Text style={styles.placeHolder}>Last Name </Text>
                                </TouchableOpacity>)}
                        </View>

                        <View style={{ width: screenSize.width - 40, flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ marginTop: 10 }}>
                                <MaterialCommunityIcons name='email' color='#1e1e1e' size={25} />
                            </View>
                            {(this.state.email_show == true) ?
                                (
                                    <View style={[styles.mainText]}>
                                        <View style={{ backgroundColor: "#ffff", position: "absolute", left: 10, bottom: 39 }}>
                                            <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>Email</Text>
                                        </View>

                                        <TextInput
                                            style={styles.textInput}
                                            keyboardType='email-address'
                                            underlineColorAndroid="transparent"
                                            placeholder="First Name"
                                            placeholderTextColor="#ababab"
                                            //returnKeyType='next'
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            //onSubmitEditing={() => this.uEmail.focus()}
                                            onChangeText={(input) => this.setState({ email: input })}
                                            ref={((input) => this.uEmail = input)}
                                        >
                                        </TextInput>
                                    </View>
                                )
                                :
                                (<TouchableOpacity style={styles.textBoxTouch}
                                    onPress={() => this.setState({ email_show: true }, this._emailFocus.bind(this))}
                                >
                                    <Text style={styles.placeHolder}>Email</Text>
                                </TouchableOpacity>)}
                        </View>


                        <View style={{ width: screenSize.width - 40, flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ marginTop: 10 }}>
                                <FontAwesome name='phone' color='#1e1e1e' size={35} />
                            </View>
                            {(this.state.ph_show == true) ?
                                (
                                    <View style={[styles.mainText]}>
                                        <View style={{ backgroundColor: "#ffff", position: "absolute", left: 10, bottom: 39 }}>
                                            <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>Phone Number</Text>
                                        </View>

                                        <TextInput
                                            style={styles.textInput}
                                            keyboardType='numeric'
                                            underlineColorAndroid="transparent"
                                            placeholder="Phone Number"
                                            placeholderTextColor="#ababab"
                                            //returnKeyType='next'
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            //onSubmitEditing={() => this.uEmail.focus()}
                                            onChangeText={(input) => this.setState({ phone: input })}
                                            ref={((input) => this.uPhone = input)}
                                        >
                                        </TextInput>
                                    </View>
                                )
                                :
                                (<TouchableOpacity style={styles.textBoxTouch}
                                    onPress={() => this.setState({ ph_show: true }, this._phoneFocus.bind(this))}
                                >
                                    <Text style={styles.placeHolder}>Phone</Text>
                                </TouchableOpacity>)}
                        </View>


                        <View style={{ width: screenSize.width - 40, flexDirection: 'row', marginTop: 10 }}>
                            <View style={{ marginTop: 10 }}>
                                <MaterialCommunityIcons name='lock' color='#1e1e1e' size={30} />
                            </View>
                            {(this.state.pass_show == true) ?
                                (
                                    <View style={[styles.mainText]}>
                                        <View style={{ backgroundColor: "#ffff", position: "absolute", left: 10, bottom: 39 }}>
                                            <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>Password</Text>
                                        </View>

                                        <TextInput
                                            style={styles.textInput}
                                            //keyboardType='email-address'
                                            secureTextEntry={true}
                                            underlineColorAndroid="transparent"
                                            placeholder="Password"
                                            placeholderTextColor="#ababab"
                                            //returnKeyType='next'
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            //onSubmitEditing={() => this.uEmail.focus()}
                                            onChangeText={(input) => this.setState({ password: input })}
                                            ref={((input) => this.uPass = input)}
                                        >
                                        </TextInput>
                                    </View>
                                )
                                :
                                (<TouchableOpacity style={styles.textBoxTouch}
                                    onPress={() => this.setState({ pass_show: true }, this._passFocus.bind(this))}
                                >
                                    <Text style={styles.placeHolder}>Password</Text>
                                </TouchableOpacity>)}
                        </View>

                        <View style={{ marginTop: 10, marginLeft: 30 }}>

                            {(this.state.confirm_show == true) ?
                                (
                                    <View style={[styles.mainText]}>
                                        <View style={{ backgroundColor: "#ffff", position: "absolute", left: 10, bottom: 39 }}>
                                            <Text style={{ color: "#3A4958", fontSize: 15, fontFamily: "SF-Pro-Display-Regular" }}>Confirm Password</Text>
                                        </View>

                                        <TextInput
                                            style={styles.textInput}
                                            //keyboardType='email-address'
                                            underlineColorAndroid="transparent"
                                            secureTextEntry={true}
                                            placeholder="Confirm Password"
                                            placeholderTextColor="#ababab"
                                            //returnKeyType='next'
                                            autoCorrect={false}
                                            autoCapitalize="none"
                                            //onSubmitEditing={() => this.uEmail.focus()}
                                            onChangeText={(input) => this.setState({ cPassword: input })}
                                            ref={((input) => this.cPass = input)}
                                        >
                                        </TextInput>
                                    </View>
                                )
                                :
                                (<TouchableOpacity style={styles.textBoxTouch}
                                    onPress={() => this.setState({ confirm_show: true }, this._cPassfocus.bind(this))


                                    }
                                >
                                    <Text style={styles.placeHolder}>Confirm Password</Text>
                                </TouchableOpacity>)}
                        </View>


                        <TouchableOpacity style={[styles.loginButton]}
                            onPress={() => this._formValidation()}
                        >
                            <Text style={{ fontFamily: "SF-Pro-Display-Medium", fontSize: 15, color: "#ffff" }}>SIGN UP</Text>
                        </TouchableOpacity>
                        <KeyboardSpacer />
                    </View>

                    <TouchableOpacity style={{ width: screenSize.width, height: 40, borderTopWidth: 1, borderTopColor: "#ababab", justifyContent: "center", alignItems: 'center', marginTop: 30 }}
                    // onPress={() => this.props.navigation.navigate('LoginScreen')}
                    >

                        <Text style={{ color: "#ababab", fontSize: 13, fontFamily: "SF-Pro-Display-Regular" }}>
                            Already have a confirmation code  ?
                          <Text style={{ color: "#3A4958", fontSize: 13, fontFamily: "SF-Pro-Display-Bold", marginLeft: 10 }}>
                                &nbsp;Enter it.
                    </Text>
                        </Text>
                    </TouchableOpacity>

                </ScrollView>

                {(this.state.isLoading==true)?
                (<View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                        <View style={styles.activity_sub}>
                    <ActivityIndicator
                        size="large"
                        color='#D0D3D4'
                        style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                    />
                </View>    
                            </View>)
                :null}


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffff',
    },
    bgStyle: {
        width: screenSize.width,
        height: screenSize.height,
    },
    home_header: {
        backgroundColor: '#1e1e1e',
        flexDirection: 'row',
        height: Platform.OS === 'ios' ? 75 : 55,
        width: Dimensions.get('window').width,
        // alignItems: 'center',
        //justifyContent: 'center',
        position: 'absolute',
        //elevation: 5,
        zIndex: 1,
        top: -1,
        paddingHorizontal: 10,
        //justifyContent: 'space-between'
    },
    textBoxTouch: {
        width: screenSize.width - 40 - 40,
        height: 55,
        borderRadius: 5,
        paddingHorizontal: 10,
        backgroundColor: "#ffff",
        borderColor: "#ababab",
        borderWidth: 1,
        justifyContent: "center",
        marginLeft: 10
    },
    placeHolder: {
        color: "#ababab",
        fontSize: 15,
        fontFamily: "SF-Pro-Display-Regular"
    },
    mainText: {
        width: screenSize.width - 40 - 40,
        height: 55,
        borderRadius: 5,
        paddingHorizontal: 10,
        backgroundColor: "#ffff",
        borderColor: "#3A4958",
        borderWidth: 2,
        justifyContent: "center",
        marginLeft: 10
    },
    textInput: {
        width: screenSize.width - 40 - 40,
        fontFamily: "SF-Pro-Display-Regular",
        fontSize: 15,
        color: "black",
        padding: 0,
        marginTop: 5,
    },
    loginButton: {
        width: screenSize.width - 40,
        height: 60,
        backgroundColor: "#3A4958",
        borderRadius: 3,
        justifyContent: "center",
        alignItems: 'center',
        marginTop: 30,
        alignSelf: "center"
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
}); 
