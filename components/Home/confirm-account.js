//   Confirm account Page
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class ConfirmAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#1e1e1e"
                    barStyle="light-content"
                />
                <ScrollView>
                    <View style={styles.topContainer}>
                        <Text style={styles.confirmText}>Confirm account</Text>
                    </View>

                    <View style={{ marginHorizontal: 10 }}>
                        <Text style={styles.sentText}>We sent a confirmation code to
                        <Text style={{ fontWeight: 'bold' }}>&nbsp;(360)862-1111</Text>
                        </Text>
                    </View>

                    <View style={styles.codeContainer}>
                        <MaterialCommunityIcons name="cellphone" color="#1e1e1e" size={30} />
                        <TextInput
                            style={styles.textInput}
                            keyboardType='numeric'
                            underlineColorAndroid="transparent"
                            placeholder="Confirmation Code"
                            placeholderTextColor="#ababab"
                            //returnKeyType='next'
                            autoCorrect={false}
                            autoCapitalize="none"
                            //onSubmitEditing={() => this.uEmail.focus()}
                            onChangeText={(input) => this.setState({ code: input })}
                            ref={((input) => this.cCode = input)}
                        />
                    </View>

                    <TouchableOpacity style={styles.confirmButton}>
                        <Text style = {styles.confirmBtnText}>CONFIRM ACCOUNT</Text>
                    </TouchableOpacity>
                </ScrollView>

                <View style={styles.resendTextContainer}>
                    <Text style={styles.resendText}>Didn't receive a confirmation code?
                        <Text style={{color : '#3A4958'}}>&nbsp;Resend it.</Text>
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    topContainer: {
        alignItems: 'center',
        marginVertical: 15
    },

    confirmText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 22,
        // fontWeight: '400'
    },

    sentText: {
        fontFamily: regularText,
        fontSize: 17,
        color: '#1e1e1e'
    },

    codeContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 10,
        marginVertical : 15
    },

    textInput: {
        flex: 1,
        fontFamily: regularText,
        // backgroundColor : 'red',
        borderTopColor: '#1e1e1e',
        borderTopWidth: 0.8,
        borderLeftColor: '#1e1e1e',
        borderLeftWidth: 0.8,
        borderBottomColor: '#1e1e1e',
        borderBottomWidth: 0.8,
        borderTopColor: '#1e1e1e',
        borderRightWidth: 0.8,
        paddingLeft: 10,
        fontSize: 17,
        marginLeft : 10,
        borderRadius : 4
    },

    confirmButton : {
        backgroundColor : '#3A4958',
        alignItems : 'center',
        justifyContent : 'center',
        marginHorizontal : 10,
        marginVertical : 15,
        height : 50,
        borderRadius : 4
    },

    confirmBtnText : {
        color : 'white',
        fontFamily : regularText,
        fontSize : 20
    },

    resendText : {
        fontFamily : regularText,
        fontSize : 16,
        textAlign: 'center'
    },

    resendTextContainer : {
        height : 50,
        // alignItems : 'center',
        justifyContent: 'center',
        borderTopColor : '#1e1e1e',
        borderTopWidth : 0.8
    }

});