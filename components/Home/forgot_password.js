import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    AsyncStorage,
    Button,
    ActivityIndicator,
    Platform,
    Dimensions,
    ImageBackground,
    ScrollView,
    Linking,
    StatusBar,
    Alert,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import firebase from 'react-native-firebase';
import CommonTasks from '../../common-tasks/common-tasks';
import KeyboardSpacer from 'react-native-keyboard-spacer';


const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');
const db = firebase.firestore();

export default class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            isLoading: false,
            id: "",
            verfication_statement: false,
            email: "",
            newPassword: "",
            confirmPassword: "",

        }
    }

    _sendCode() {
        if(this.state.email=== '' || this.state.email.trim() === '')
        {
            CommonTasks._displayToast("Please Enter Your Email");
            this.inputEmail.focus();
            return false;
        }
        this.setState({ isLoading: true });
        firebase.auth().sendPasswordResetEmail(this.state.email)
            .then((data) => {
                this.setState({ isLoading: false });
                Alert.alert(
                    'Pro In Your Pocket For Pros',
                    'A password reset link has been send to your email please check and continue login!!',
                    [
                        { text: 'OK', onPress: () => this.props.navigation.replace('SigninWithEmailScreen') },
                    ],
                    { cancelable: false }
                );
                console.log(data)
            })
            .catch((error) => {
                this.setState({ isLoading: false });
                console.log(error);
                const { code, message } = error;
                console.log("error" + error);
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode + "-" + errorMessage);
                CommonTasks._displayToast(errorMessage);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.backContainer}
                        onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" style={styles.headerImage} />
                        <Text style={styles.signinText}>Sign In</Text>
                    </TouchableOpacity>

                    <Text style={styles.headerText}>Forgot Password</Text>
                    <Text style={styles.nextText}></Text>
                </View>
                <ScrollView>
                    <View style={styles.verificationContainer}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="Email"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            // onSubmitEditing={() => this.inputLname.focus()}
                            onChangeText={(input) => this.setState({ email: input })}
                            ref={((input) => this.inputEmail = input)}
                        />

                        <TouchableOpacity
                            onPress={() => this._sendCode()}>
                            <Text style={styles.sendCodeText}>Send Code</Text>
                        </TouchableOpacity>

                    </View>


                    {/* <Text style={styles.verificationText}>A verification code was sent &nbsp;
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{this.state.email}.</Text>
                    </Text> */}


                    {/* <View style={styles.verificationContainer}>
                        <TextInput
                            style={styles.textInput}
                            keyboardType='numeric'
                            underlineColorAndroid="transparent"
                            placeholder="Verification code"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            // onSubmitEditing={() => this.inputLname.focus()}
                            onChangeText={(input) => this.setState({ code: input })}
                            ref={((input) => this.inputCode = input)}
                        />

                        <TextInput
                            style={styles.textInput}
                            secureTextEntry={true}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="New Password"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            // onSubmitEditing={() => this.inputLname.focus()}
                            onChangeText={(input) => this.setState({ newPassword: input })}
                            ref={((input) => this.inputPass = input)}
                        />
                        <TextInput
                            style={styles.textInput}
                            secureTextEntry={true}
                            keyboardType='default'
                            underlineColorAndroid="transparent"
                            placeholder="Confirm Password"
                            placeholderTextColor="#ababab"
                            returnKeyType='done'
                            autoCorrect={false}
                            autoCapitalize="none"
                            // onSubmitEditing={() => this.inputLname.focus()}
                            onChangeText={(input) => this.setState({ confirmPassword: input })}
                            ref={((input) => this.inputCPass = input)}
                        />

                        <TouchableOpacity
                            onPress={() => this._phoneVerify()}>
                            <Text style={styles.sendCodeText}>Change Password</Text>
                        </TouchableOpacity>

                    </View> */}

                    <KeyboardSpacer />
                </ScrollView >

                {
                    (this.state.isLoading == true) ?
                        (<View style={{position:'absolute',height: screenSize.height, width: screenSize.width,backgroundColor: 'rgba(255, 255, 255, 0.2)'}}>
                        <View style={styles.activity_sub}>
                            <ActivityIndicator
                                size="large"
                                color='#D0D3D4'
                                style={{ justifyContent: 'center', alignItems: 'center', height: 50 }}
                            />
                        </View>    
                        </View>
                            )
                        : null
                }

            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    headerContainer: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: '#f7f7f7',
        paddingVertical: 10,
        alignItems: 'center'
    },

    backContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

    signinText: {
        fontFamily: regularText,
        fontSize: 18,
        color: '#007aff',
        marginLeft: 10
    },

    headerImage: {
        fontSize: 30,
        color: '#007aff',
        fontWeight: 'bold',
        marginHorizontal: 3
    },

    headerText: {
        fontFamily: boldText,
        color: '#1e1e1e',
        fontSize: 18
    },

    nextText: {
        color: '#007aff',
        fontFamily: boldText,
        fontSize: 18,
        marginRight: 10
    },

    textInput: {
        borderBottomColor: '#8e8e93',
        borderBottomWidth: 0.7,
        // paddingLeft : 15,
        marginLeft: 15,
        fontFamily: regularText,
        fontSize: 17
    },

    verificationContainer: {
        backgroundColor: 'white',
        marginVertical: 20
    },

    verificationText: {
        fontFamily: regularText,
        fontSize: 15,
        marginLeft: 15
    },

    sendCodeText: {
        textAlign: 'center',
        fontFamily: regularText,
        fontSize: 20,
        color: '#007aff',
        marginVertical: 10
    },
    activity_sub: {
        position: 'absolute',
        top: screenSize.height / 2,
        backgroundColor: 'black',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
});