/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Alert,
    Image,
    Dimensions,
    StatusBar,
    TouchableOpacity,
    AsyncStorage
} from 'react-native';

import AppIntro from 'react-native-app-intro';
import { NavigationActions, StackActions } from 'react-navigation';

const windowsWidth = Dimensions.get('window').width;
const windowsHeight = Dimensions.get('window').height;
// import AppIntro from './AppIntro';

const regularText = "SF-Pro-Display-Regular";
const mediumText = "SF-Pro-Display-Medium";
const boldText = "SF-Pro-Display-Bold";
const screenSize = Dimensions.get('window');

export default class SplashPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_status: "",
            user_id: "",
        }
    }

    onSkipBtnHandle = (index) => {
        //    Alert.alert('Skip');
        this.props.navigation.replace('SigninConfirmScreen');
        console.log(index);
    }

    doneBtnHandle = () => {
        this.props.navigation.replace('SigninConfirmScreen');
        //this.props.navigation.navigate('CreateAccountStep3Screen');
    }
    nextBtnHandle = (index) => {
        //    Alert.alert('Next');
        console.log(index);
    }
    onSlideChangeHandle = (index, total) => {
        //    console.log(index, total);
    }

    componentDidMount() {
        AsyncStorage.getItem('user_status').then(value => { this.setState({ user_status: value }) });
        AsyncStorage.getItem('user_id').then(value => { this.setState({ user_id: value }) });
    }

    _getStarted() {
        if (this.state.user_status == "1") {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'ProjectSearchScreen',
                    params:
                    {
                        uid: this.state.user_id
                    }

                })],
            });
            this.props.navigation.dispatch(resetAction);
        }
        else {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({
                    routeName: 'SigninConfirmScreen',
                })],
            });
            this.props.navigation.dispatch(resetAction);
            //this.props.navigation.navigate('SigninConfirmScreen');
        }

    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {/* <StatusBar
                    backgroundColor="#ffff"
                    barStyle="dark-content"
                /> */}
                <View style={{ backgroundColor: 'white', borderBottomColor: '#8e8e93', borderBottomWidth: 0.8 }}>
                    <Image
                        source={require('../../assets/images/banner-header.png')}
                        style={{ width: 150, height: 50, alignSelf: 'center', resizeMode: 'contain' }}
                    />
                </View>
                <AppIntro
                    onNextBtnClick={this.nextBtnHandle}
                    //onDoneBtnClick={this.doneBtnHandle}
                    onSkipBtnClick={this.onSkipBtnHandle}
                    onSlideChange={this.onSlideChangeHandle}
                    customStyles={{ btnContainer: { flex: 1, } }}
                    dotColor={'rgba(169, 169, 169, 1)'}
                    activeDotColor={'black'}
                    leftTextColor={'#a9a9a9'}
                    rightTextColor={'black'}
                    showDoneButton={true}
                    nextBtnLabel={'Next'}

                >
                    <View style={[styles.slide, { backgroundColor: '#fff' }]}>
                        <View style={[styles.header, { width: windowsWidth }]}>
                            <Text style={[styles.topText, { color: '#0079cc' }]}>Find Projects</Text>
                            <View>
                                <Image style={{ width: screenSize.width, height: (screenSize.height) / 3.5, resizeMode: 'stretch' }} source={require('../../assets/images/custom2.png')} />
                            </View>
                        </View>
                        <View style={styles.info}>
                            <View level={15}><Text style={styles.description}>See what customers are in need of by searching for local projects. Sign up for notifications to get emailed when new projects are added.</Text></View>
                        </View>
                    </View>
                    <View style={[styles.slide, { backgroundColor: '#fff' }]}>
                        <View style={[styles.header, { width: windowsWidth }]}>
                            <Text style={[styles.topText, { color: '#2e29cd' }]}>Place bids</Text>
                            <View>
                                <Image style={{ width: screenSize.width, height: (screenSize.height) / 3.5, resizeMode: 'stretch' }} source={require('../../assets/images/custom3.png')} />
                            </View>
                        </View>
                        <View style={styles.info}>
                            <View level={15}><Text style={styles.description}>Place bids on the projects that you are interested in. You can also chat with the customer to help them get to know you.</Text></View>
                        </View>
                    </View>
                    <View style={[styles.slide, { backgroundColor: '#fff' }]}>
                        <View style={[styles.header, { width: windowsWidth }]}>
                            <Text style={[styles.topText, { color: '#fd7800' }]}>Get hired</Text>
                            <View>
                                <Image style={{ width: screenSize.width, height: (screenSize.height) / 3.5, resizeMode: 'stretch' }} source={require('../../assets/images/custom4.png')} />
                            </View>
                        </View>
                        <View style={styles.info}>
                            <View level={15}><Text style={styles.description}>Once you are hired by a customer, schedule an appointment and arrange payment.</Text></View>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => this._getStarted()} style={[styles.slide, { backgroundColor: '#fff' }]}>
                        <View style={[styles.header, { width: windowsWidth }]}>
                            <Text style={[styles.topText, { color: '#00a910' }]}>Experience growth</Text>
                            <View>
                                <Image style={{ width: screenSize.width, height: (screenSize.height) / 3.5, resizeMode: 'stretch' }} source={require('../../assets/images/custom1.png')} />
                            </View>
                        </View>
                        <View style={styles.info}>
                            <View level={15}><Text style={styles.description}>Using the Pro App you will grow your business quickly by making your business discoverable and acquiring new customers by completing projects.</Text></View>
                        </View>

                        <TouchableOpacity style={styles.startedContainer}
                            onPress={this._getStarted()}
                        >
                            <Text style={styles.startedText}>Get Started</Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </AppIntro>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    slide: {
        flex: 1
    },
    header: {
        // flex: 0.5,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    pic: {
        width: 75 * 2,
        height: 63 * 2,
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    info: {
        flex: 0.5,
        // alignItems: 'center',
        padding: 20
    },
    title: {
        color: '#fff',
        fontSize: 30,
        paddingBottom: 20,
    },
    description: {
        color: 'black',
        fontSize: 16,
        textAlign: 'center'
    },

    topText: {
        fontFamily: regularText,
        fontSize: 30,
        alignSelf: 'center',
        marginVertical: 15
    },

    startedContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#3A4958',
        width: 180,
        alignSelf: 'center',
        borderRadius: 5
    },

    startedText: {
        color: 'white',
        fontSize: 20
    }
});
