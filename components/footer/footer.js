import React, {Component} from 'react';
import {
   StyleSheet,
   Dimensions,
  Image,
  TouchableOpacity,
  Text,
  View,
  Alert,
  AsyncStorage
    } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { withNavigation } from 'react-navigation';

const screenSize = Dimensions.get('window');

type Props = {};
class Footer extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      uid : ''
    }
}

componentWillMount()
{
  AsyncStorage.getItem('user_id').then(value => this.setState({ uid: value }));
}
_navigateBidPending() { 
  this.props.navigation.navigate('BidsPendingScreen',{uid: this.state.uid, selectedOption : 'Pending'});
}


_navigateMessageListing()
{
this.props.navigation.navigate('MessageListingScreen',{uid: this.state.uid});
}


  render() {
    const { itemColor } = this.props;
    return (
      <View style={styles.footerContainer}>
          <TouchableOpacity style={styles.footerItem} onPress={() => this.props.navigation.navigate('ProjectSearchScreen',{uid : this.state.uid})}>
              {itemColor=='globe'?
                <MaterialCommunityIcons name="earth" style={[styles.footerImage, { color: '#3A4958' }]} />
              :
                <MaterialCommunityIcons name="earth" style={[styles.footerImage, { color: '#8e8e93' }]} />
              }
          </TouchableOpacity>

          <TouchableOpacity style={styles.footerItem} onPress={()=>this._navigateBidPending()}>
              {itemColor=='hammer'?
                <Image style={[styles.footerImage1, {tintColor: '#3A4958'}]} source={require('../../assets/images/hammer.png')}/>
              :
                <Image style={[styles.footerImage1, {tintColor: '#8e8e93'}]} source={require('../../assets/images/hammer.png')}/>
              }
          </TouchableOpacity>

          <TouchableOpacity style={styles.footerItem} onPress={()=>this._navigateMessageListing()}>
            {itemColor=='message'?
                //<SimpleLineIcons name="graph" style={styles.footerImage} color='#3A4958'/>
                <MaterialIcons name="message"  style={styles.footerImage} color='#3A4958'/>
              :
                //<SimpleLineIcons name="graph" style={styles.footerImage} color='#8e8e93'/>
                <MaterialIcons name="message" style={styles.footerImage} color='#8e8e93' />
            }
          </TouchableOpacity>

          <TouchableOpacity style={styles.footerItem} onPress={() => this.props.navigation.navigate('NotificationsScreen',{uid : this.state.uid})}>
            {itemColor=='notification'?
                //<SimpleLineIcons name="graph" style={styles.footerImage} color='#3A4958'/>
                <MaterialIcons name="notifications"  style={styles.footerImage} color='#3A4958'/>
              :
                //<SimpleLineIcons name="graph" style={styles.footerImage} color='#8e8e93'/>
                <MaterialIcons name="notifications" style={styles.footerImage} color='#8e8e93' />
            }
          </TouchableOpacity>    
         
          <TouchableOpacity style={styles.footerItem} onPress={() => this.props.navigation.navigate('MyAccountScreen',{uid : this.state.uid})}>
          {(itemColor=='account')?
              <MaterialCommunityIcons name="account-circle" style={styles.footerImage} color='#3A4958'/>
          :
            <MaterialCommunityIcons name="account-circle" style={styles.footerImage} color='#8e8e93'/>
          }
          </TouchableOpacity>

          {/* <TouchableOpacity style={styles.footerItem} onPress={() => this.props.navigation.navigate('BuySubscriptionScreen')}>
          {(itemColor=='rating')?
              <MaterialCommunityIcons name="account-star" style={styles.footerImage} color='#3A4958'/>
          :
            <MaterialCommunityIcons name="account-star" style={styles.footerImage} color='#8e8e93'/>
          }
          </TouchableOpacity> */}
            
          
      </View>
    ); 
  }
}

export default withNavigation(Footer);
const styles = StyleSheet.create({
  footerContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems : 'center',
    height: 60,
    backgroundColor: '#fff',
    position: 'absolute',
    bottom : 0,
    width : screenSize.width,
    left : 0,
},

footerItem: {
    width: screenSize.width / 6,
    alignItems: 'center',
    justifyContent: 'center'
},

footerImage: {
    fontSize: 30
},
footerImage1 :  {
  height : 30,
  width : 30
},
hammer : {
  tintColor: '#3A4958'
}
});
