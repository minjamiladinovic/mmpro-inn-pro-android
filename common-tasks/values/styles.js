import { Platform, StyleSheet } from "react-native";
import Dimension from "./dimension";
import Colors from "./colors";
import Fonts from "./fonts";
import colors from "./colors";

export default StyleSheet.create({
    toolbar: {
        height: Dimension.toolbarHeight,
        width: "100%",
        top: -1,
        zIndex: Dimension.zIndexNormal,
        padding: Dimension.padding5,
        position: "absolute",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: Colors.colorPrimary,
        elevation: Dimension.elevationLight
    },
    toolbarMain: {
        paddingTop: Dimension.padding7,
        paddingBottom: Dimension.padding7
    },
    toolbarLeftIcon: {
        height: Dimension.toolbarIconSize,
        width: Dimension.toolbarIconSize,
        marginStart: Dimension.margin5
    },


    // loader for all pages in center
    opacityLayer: {
        flex: 1,
        flexDirection: "column",
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: Dimension.zIndexNormal,
        ...Platform.select({
            android: { elevation: Dimension.elevationLight, },
            ios: {
                shadowColor: Colors.colorDarkGray,
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        backgroundColor: Colors.colorOpacityBlack,
    },

    containerMainWhite: {
        flex: 1,
        backgroundColor: Colors.colorWhite
    },
    containerMain: {
        flex: 1,
    },
    containerCenter: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    containerLeft: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    containerRight: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    containerCenterHorizontal: {
        flex: 1
    },
    containerBottom: {
        flex: 1,
        justifyContent: "flex-end"
    },
    containerFlexRow: {
        flexDirection: "row",
        alignItems: "center",
    },
    containerFlexRowSpaceBetween: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    width100height100: {
        width: "100%",
        height: "100%"
    },
    width25: {
        width: "25%"
    },

    //rounded containers

    //rounded button
    roundedButtonWhitePadding5Margin5Elevation: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.colorWhite,
        paddingTop: Dimension.padding5,
        paddingBottom: Dimension.padding5,
        paddingStart: Dimension.padding15,
        paddingEnd: Dimension.padding15,
        borderRadius: Dimension.radius25,
        zIndex: Dimension.zIndexNormal,
        elevation: Dimension.elevationNormal,
        marginTop: Dimension.margin5,
        marginBottom: Dimension.margin5
    },
    roundedButtonPrimaryElevation: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.colorPrimary,
        paddingTop: Dimension.padding10,
        paddingBottom: Dimension.padding10,
        paddingStart: Dimension.padding30,
        paddingEnd: Dimension.padding30,
        borderRadius: Dimension.radius25,
        zIndex: Dimension.zIndexNormal,
        elevation: Dimension.elevationNormal
    },
    roundedButtonOrangeRadius25ElevationLight: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.colorOrange,
        paddingTop: Dimension.padding5,
        paddingBottom: Dimension.padding5,
        paddingStart: Dimension.padding25,
        paddingEnd: Dimension.padding25,
        borderRadius: Dimension.radius25,
        zIndex: Dimension.zIndexNormal,
        elevation: Dimension.elevationLight
    },


    //text
    textRobotoRegular12Primary: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize12,
        color: Colors.colorPrimary,
    },
    textRobotoRegular14Gray: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize14,
        color: Colors.colorGray,
    },
    textRobotoRegular14TextXDarkGray: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize14,
        color: Colors.colorTextXDarkGray,
    },
    textRobotoRegular16White: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorWhite,
    },
    textRobotoRegular16DarkGray: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorDarkGray,
    },
    textRobotoRegular16Black: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorBlack,
    },
    textRobotoRegular15TextXDarkGray: {
        fontFamily: Fonts.robotoRegular,
        fontSize: Dimension.fontSize15,
        color: Colors.colorTextXDarkGray,
        padding: 0
    },
    textRalewayRegular12XDarkGray: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize12,
        color: Colors.colorTextXDarkGray,
    },
    textRalewayRegular14DarkGray: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize14,
        color: Colors.colorDarkGray
    },
    textRalewayMedium12DarkGray: {
        fontFamily: Fonts.ralewayMedium,
        fontSize: Dimension.fontSize12,
        color: Colors.colorDarkGray
    },
    textRalewayMedium13Gray: {
        fontFamily: Fonts.ralewayMedium,
        fontSize: Dimension.fontSize13,
        color: Colors.colorDarkGray
    },
    textRalewayMedium13LightGray: {
        fontFamily: Fonts.ralewayMedium,
        fontSize: Dimension.fontSize14,
        color: Colors.colorGray
    },
    textRalewayRegular14XDarkGray: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize14,
        color: Colors.colorTextXDarkGray
    },
    textRalewayRegular14XDarkGrayCenter: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize14,
        color: Colors.colorTextXDarkGray,
        textAlign: "center"
    },
    textRalewayRegular16White: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorWhite
    },
    textRalewayRegular16WhiteCenter: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorWhite,
        textAlign: "center"
    },
    textRalewayRegular16XDarkGray: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorTextXDarkGray
    },
    textRalewayRegular18XDarkGray: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize18,
        color: Colors.colorTextXDarkGray
    },
    textRalewayRegular18White: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize18,
        color: Colors.colorWhite
    },
    textRalewayRegular18DarkGray: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize18,
        color: Colors.colorDarkGray
    },
    textRalewayRegular18WhiteCenter: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize18,
        color: Colors.colorWhite,
        textAlign: "center"
    },
    textRalewayMedium18BlackCenter: {
        fontFamily: Fonts.ralewayMedium,
        fontSize: Dimension.fontSize18,
        color: Colors.colorBlack,
        textAlign: "center"
    },
    textRalewayRegular22White: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize20,
        color: Colors.colorWhite
    },
    textRalewayRegular22Black: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize22,
        color: Colors.colorBlack
    },
    textRalewayRegular26Black: {
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize26,
        color: Colors.colorBlack
    },

    //width

    //margin
    margin10: {
        margin: Dimension.margin10
    },
    margin20: {
        margin: Dimension.margin20
    },
    margin40: {
        margin: Dimension.margin40
    },
    marginTop5: {
        marginTop: Dimension.margin5
    },
    marginTop10: {
        marginTop: Dimension.margin10
    },
    marginTop20: {
        marginTop: Dimension.margin20
    },
    marginTop30: {
        marginTop: Dimension.margin30
    },
    marginTop50: {
        marginTop: Dimension.margin50
    },
    marginStartEnd15: {
        marginStart: Dimension.margin15,
        marginEnd: Dimension.margin15
    },
    marginStartEnd20: {
        marginStart: Dimension.margin20,
        marginEnd: Dimension.margin20
    },
    marginStartEnd30: {
        marginStart: Dimension.margin30,
        marginEnd: Dimension.margin30
    },
    marginStartEnd50: {
        marginStart: Dimension.margin50,
        marginEnd: Dimension.margin50
    },
    marginStart5: {
        marginStart: Dimension.margin5
    },
    marginStart10: {
        marginStart: Dimension.margin10
    },
    marginEnd15: {
        marginEnd: Dimension.margin15
    },
    marginEnd25: {
        marginEnd: Dimension.margin40
    },

    //padding
    padding5: {
        padding: Dimension.padding5
    },
    padding10: {
        padding: Dimension.padding10
    },

    //custom ui views
    textInputLayout: {
        height: 26,
        fontSize: 20,
        color: Colors.colorBlack,
        borderBottomWidth: 1,
        borderBottomColor: Colors.colorDarkGray
    },

    //review screen
    rvwContainer: {
        alignItems: "center",
    },
    rvwHeaderImage: {
        width: Dimension.size150,
        height: Dimension.size125,
        marginTop: Dimension.margin70,
        resizeMode: "contain"
    },
    rvwRatingBar: {
        width: Dimension.width - Dimension.size60,
        marginTop: Dimension.margin15,
        padding: Dimension.padding2
    },
    rvwRatingBarLabelParent: {
        width: Dimension.width - Dimension.size35,
    },
    rvwRatingBarLabel: {
        width: "20%", alignItems: "center"
    },
    rvwFullWidthMargin: {
        width: Dimension.width - Dimension.size40,
        marginTop: Dimension.margin15
    },
    rvwUploadPhotoParent: {
        width: "100%",
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginTop: Dimension.margin20,
        paddingTop: Dimension.padding10,
        paddingBottom: Dimension.padding10,
        paddingStart: Dimension.padding20,
        paddingEnd: Dimension.padding20,
        borderTopColor: Colors.colorXLightGray,
        borderTopWidth: 1,
        borderBottomColor: Colors.colorXLightGray,
        borderBottomWidth: 1,
    },
    rvwUploadFileTitle: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
    },
    rvwUploadPhotoImageIcon: {
        width: Dimension.size30,
        height: Dimension.size30,
    },
    rvwUploadFileItemParent: {
        width: Dimension.size140,
        height: Dimension.size140,
        // flex: 1,
        marginTop: Dimension.margin10,
        marginBottom: Dimension.margin10,
        marginStart: Dimension.margin1,
        marginEnd: Dimension.margin15,
        backgroundColor: Colors.colorDarkGray,
        borderRadius: Dimension.radius10
    },
    rvwUploadFileImage: {
        width: Dimension.size140,
        height: Dimension.size140,
        borderRadius: Dimension.radius10
    },
    rvwFileRemoveParent: {
        width: Dimension.size30,
        height: Dimension.size30,
        position: "absolute",
        top: 0,
        right: 0,
        borderTopRightRadius: Dimension.radius10,
        borderBottomLeftRadius: Dimension.radius10,
        backgroundColor: Colors.colorPrimary,
        justifyContent: "center",
        alignItems: "center"
    },
    rvwRemoveIcon: {
        width: Dimension.size15, height: Dimension.size15
    },
    rvwUploadFileVideoIcon: {
        width: Dimension.size40,
        height: Dimension.size40,
        position: "absolute",
        top: Dimension.margin50,
        left: Dimension.margin50,
        tintColor: Colors.colorWhite
    },
    rvw_modal_main_body: {
        backgroundColor: 'rgba(52, 52, 52, 0)',
        height: Dimension.height,
    },
    rvwModal: {
        height: Dimension.size150,
        marginTop: Dimension.height - Dimension.size150,
        backgroundColor: Colors.colorXLightGray,
        padding: Dimension.padding15
    },
    rvwCloseModal: {
        width: Dimension.size30,
        height: Dimension.size30,
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        right: 0,
        margin: Dimension.margin5,
        borderRadius: Dimension.radius5,
        backgroundColor: Colors.colorPrimary,
    },
    rvwSuccessPopupParent: {
        marginTop: Dimension.margin100,
        marginStart: Dimension.margin40,
        marginEnd: Dimension.margin40,
        borderRadius: Dimension.radius10,
        backgroundColor: Colors.colorWhite,
    },
    rvwSuccessAlertTopLeftBG: {
        width: Dimension.size90,
        height: Dimension.size50,
        borderTopLeftRadius: Dimension.radius10,
        marginStart: Dimension.sizeMinus5,
        resizeMode: "contain"

    },
    rvwSuccessAlertHeaderBG: {
        width: Dimension.size125,
        height: Dimension.size125,
        marginTop: Dimension.sizeMinus10
    },
    rvwSuccessAlertContainer: {
        marginStart: Dimension.margin10,
        marginEnd: Dimension.margin10,
        marginBottom: Dimension.margin30,
        alignItems: 'center'
    },
    rvwSuccessButton: {
        marginTop: Dimension.margin20,
        paddingTop: Dimension.padding10,
        paddingBottom: Dimension.padding10,
        paddingStart: Dimension.padding30,
        paddingEnd: Dimension.padding30,
        borderRadius: Dimension.radius3,
        backgroundColor: Colors.colorDarkSkyBlue,
    },

    //referral code screen
    rfrlContainerMain: {
        marginTop: Dimension.margin50,
        marginBottom: Dimension.margin250,
        marginStart: Dimension.margin30,
        marginEnd: Dimension.margin30
    },
    rfrlHeaderImage: {
        width: Dimension.size80,
        height: Dimension.size80,
        marginTop: Dimension.margin30,
        resizeMode: "contain"
    },
    rfrlTextInput: {
        width: "100%",
        marginTop: Dimension.margin20,
        paddingBottom: Dimension.padding2,
        borderBottomWidth: Dimension.size1,
        borderBottomColor: Colors.colorPrimary,
        fontFamily: Fonts.ralewayRegular,
        fontSize: Dimension.fontSize16,
        color: Colors.colorDarkGray,
    },

    //review detail screen
    rvDtlModalContainer: {
        height: Dimension.height,
        backgroundColor: Colors.colorWhite,
        position: 'absolute',
        top: -1,
        zIndex: 0,
        flex: 1
    },
    rvDtlHeaderContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between',
        backgroundColor: Colors.colorPrimary,
        height: Dimension.size50,
        width: Dimension.width,
        alignSelf: 'flex-start'
    },
    rvDtlModalCloseContainer: {
        width: Dimension.size30,
        height: Dimension.size30,
        justifyContent: "center",
        alignItems: "center",
        // position: "absolute",
        // top: Dimension.margin6,
        // end: Dimension.margin10,
    },
    rvDtlModalCloseIcon: {
        width: Dimension.size15,
        height: Dimension.size15,
        tintColor: Colors.colorWhite
    },
    rvDtlModalLikeIcon: {
        width: Dimension.size20,
        height: Dimension.size20,
        marginStart: Dimension.margin15,
        tintColor: Colors.colorWhite,
        paddingVertical: Dimension.padding10,
    },
    rvDtlHorizontalLine: {
        borderBottomColor: Colors.colorLightGray,
        borderBottomWidth: 1,
    },
    rvDtlComments: {
        marginTop: Dimension.margin10,
        // paddingBottom: 100,
    },
    rvDtlCommentMainContainer: {
        margin: Dimension.margin10,
        flexDirection: "row"
    },
    rvDtlCommentUserPicture: {
        width: Dimension.size50,
        height: Dimension.size50,
        borderRadius: Dimension.radius25,
        backgroundColor: Colors.colorXLightGray
    },
    rvDtlCommentContainer: {
        marginStart: Dimension.margin10,
        marginEnd: Dimension.margin80,
    },
    rvDtlNameCommentTextContainer: {
        padding: Dimension.padding10,
        backgroundColor: Colors.colorXLightGray,
        borderRadius: Dimension.radius10,
        // flexDirection:'row'
    },
    rvDtlNameCommentImageContainer: {
        paddingEnd: Dimension.padding10,
    },
    rvDtlCommentImageContainer: {
        width: Dimension.size140,
        height: Dimension.size105,
        marginTop: Dimension.margin5,
        backgroundColor: Colors.colorXLightGray,
        borderRadius: Dimension.radius10
    },
    rvDtlAddCommentContainer: {
        alignSelf: 'flex-end',
        position: "absolute",
        bottom: 0,
        marginTop: "auto",
        width: Dimension.width,
        height: Dimension.size50,
        zIndex: Dimension.zIndexNormal,
        elevation: Dimension.elevationHigh,
        backgroundColor: Colors.colorWhite,
        justifyContent: "center"
    },
    rvDtlReplies: {
        marginTop: Dimension.margin2,
    },
    rvDtlReplyMainContainer: {
        flexDirection: "row",
        marginTop: Dimension.margin10
    },
    rvDtlReplyUserPicture: {
        width: Dimension.size30,
        height: Dimension.size30,
        borderRadius: Dimension.radius15,
        backgroundColor: Colors.colorXLightGray
    },
    rvDtlReplyContainer: {
        marginStart: Dimension.margin5
    },
    rvDtlNameReplyTextContainer: {
        marginEnd: Dimension.margin30,
        padding: Dimension.padding5,
        backgroundColor: Colors.colorXLightGray,
        borderRadius: Dimension.radius10
    },
    rvDtlNameReplyImageContainer: {
        marginEnd: Dimension.margin30,
        paddingEnd: Dimension.padding5,
    },
    rvDtlAddReplyContainer: {
        width: Dimension.size250,
        height: Dimension.size50,
        backgroundColor: Colors.colorLightGray,
        justifyContent: "center"
    },
    rvDtlAddPhotoContainer: {
        width: Dimension.size34,
        height: Dimension.size34,
        marginStart: Dimension.margin10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: Dimension.radius3,
        elevation: Dimension.elevationNormal,
        backgroundColor: Colors.colorWhite
    },
    rvDtlAddPhotoIcon: {
        width: Dimension.size25,
        height: Dimension.size25,
        tintColor: Colors.colorRed
    },
    rvDtlMessageContainer: {
        paddingStart: Dimension.padding10,
        paddingEnd: Dimension.padding10,
        borderRadius: Dimension.radius25,
        position: "absolute",
        top: Dimension.margin7,
        bottom: Dimension.margin7,
        left: Dimension.margin50,
        end: Dimension.margin50,
        justifyContent: "center",
        backgroundColor: Colors.colorXLightGray,
    },
    rvDtlImojiContainer: {
        width: Dimension.size30,
        height: Dimension.size30,
        position: "absolute",
        end: Dimension.margin5,
        alignItems: "center",
        justifyContent: "center"
    },
    rvDtlEmojiIcon: {
        width: Dimension.size20,
        height: Dimension.size20,
        tintColor: Colors.colorGray
    },
    rvDtlSendCommentContainer: {
        width: Dimension.size34,
        height: Dimension.size34,
        position: "absolute",
        end: Dimension.margin10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: Dimension.radius17,
        elevation: Dimension.elevationNormal,
        backgroundColor: Colors.colorPrimary
    },
    rvDtlSendCommentIcon: {
        width: Dimension.size20,
        height: Dimension.size20,
        marginEnd: Dimension.margin2,
        tintColor: Colors.colorWhite
    },
    rvDtlPaginationContainer: {
        height: Dimension.size35,
        width: Dimension.width,
        position: "absolute",
        top: Dimension.margin40,
        backgroundColor: Colors.colorOpacityXLightGray,
        justifyContent: "center",
        alignItems: "center"
    },
    rDtlPersonInfo: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    rDtlPersonPhoto: {
        padding: 10,
        width: 70,
        height: 70,
    },
    rDtlPersonDetail: {
        flex: 1,
        paddingVertical: 5,
    },
    rDtlTimeContainer: {
        width: 120,
    },
    rDtlReviewerNameTxt: {
        color: Colors.colorXDarkGray,
        fontSize: 18,
        fontFamily: Fonts.ralewayMedium
    },
    rDtlTimeTxt: {
        textAlign: 'right',
        fontFamily: Fonts.montserratRegular,
        fontSize: 12
    },
    rDtlLikeIcon: {
        width: 20,
        height: 20,
        marginRight: 5
    },
    rDtlLikeIconTxt: {
        fontSize: 13,
        fontFamily: Fonts.montserratMedium,
        color: Colors.colorXDarkGray
    },
    rDtlLikeContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    rDtlShareContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
    },
    rDtlReportContainer: {
        flexDirection: 'row',
        paddingVertical: 10,
        alignItems: 'center'
    },
    rDtlLikeReportSahreContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: Dimension.width,
        padding: 10,
    },
    rDtlReviewDesc: {
        fontFamily: Fonts.montserratMedium,
        fontSize: 14,
        marginHorizontal: 10,
        marginBottom: 10,
        color: Colors.colorGray,
    },
    rDtlCommentHeadingTxt: {
        color: Colors.colorPrimary,
        fontFamily: Fonts.ralewayBold,
        fontSize: 16,
        margin: 10,
    },
    rDtlCommentsTitlRow: {
        flexDirection: 'row',
    },
    rDtlReadMoreBtnContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        width: Dimension.size120,
        paddingBottom: Dimension.padding20,
    },
    rDtlReadMoreBtnTxt: {
        color: Colors.colorPrimary,
        fontFamily: Fonts.montserratMedium,
        textDecorationLine: 'underline'
    },
    rDtlReadMoreBtnImg: {
        height: Dimension.size10,
        width: Dimension.size10,
        marginLeft: Dimension.margin2,
        marginTop: 3,
        tintColor: Colors.colorPrimary,
    },
    rDtlReviewMediaList: {
        marginVertical: 10,
        backgroundColor: Colors.colorLightGray,
    },
    rDtlReviewMediaImage: {
        width: Dimension.width,
        height: Dimension.width,
    },
    auto_height_img: {

    },
    videoThumbnails: {

    },
    rDtlReviewMediaPlayBtn: {
        position: 'absolute',
        top: 80,
        left: 150,
        height: 50,
        width: 50
    },
    rDtlReviewMediaThumnail: {
        height: Dimension.size200,
        width: Dimension.width
    },
    rvDtlAllCommentLink: {
        fontFamily: Fonts.ralewayMedium,
        color: colors.colorPrimary,
        fontSize: 14,
        textAlign: 'right',
        margin: 20,
        borderColor: colors.colorPrimary,
        textDecorationLine: 'underline'
    },


    //comment modal
    cmntModalContainer: {
        height: Dimension.height,
        marginTop: Dimension.margin30,
        borderTopLeftRadius: Dimension.radius10,
        borderTopRightRadius: Dimension.radius10,
        backgroundColor: Colors.colorWhite,
        flex: 1
    },
    cmntHeaderContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    cmntModalCloseContainer: {
        width: Dimension.size30,
        height: Dimension.size30,
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        top: Dimension.margin6,
        end: Dimension.margin10,
    },
    cmntModalCloseIcon: {
        width: Dimension.size15,
        height: Dimension.size15,
        tintColor: Colors.colorDarkGray
    },
    cmntModalLikeIcon: {
        width: Dimension.size20,
        height: Dimension.size20,
        marginStart: Dimension.margin15,
        tintColor: Colors.colorPrimary,
    },
    cmntHorizontalLine: {
        borderBottomColor: Colors.colorLightGray,
        borderBottomWidth: 1,
        position: "absolute",
        top: Dimension.margin40,
        start: 0,
        end: 0
    },
    cmntComments: {
        position: "absolute",
        top: Dimension.margin45,
        bottom: Dimension.margin50,
        start: 0,
        end: 0
    },
    cmntCommentMainContainer: {
        margin: Dimension.margin10,
        flexDirection: "row"
    },
    cmntCommentUserPicture: {
        width: Dimension.size40,
        height: Dimension.size40,
        borderRadius: Dimension.radius20,
        backgroundColor: Colors.colorXLightGray
    },
    cmntCommentContainer: {
        marginStart: Dimension.margin10,
        marginEnd: Dimension.margin80,
    },
    cmntNameCommentTextContainer: {
        padding: Dimension.padding10,
        backgroundColor: Colors.colorXLightGray,
        borderRadius: Dimension.radius10
    },
    cmntNameCommentImageContainer: {
        paddingEnd: Dimension.padding10,
    },
    cmntCommentImageContainer: {
        width: Dimension.size140,
        height: Dimension.size105,
        marginTop: Dimension.margin5,
        backgroundColor: Colors.colorXLightGray,
        borderRadius: Dimension.radius10
    },
    cmntAddCommentContainer: {
        alignSelf: 'flex-end',
        marginTop: "auto",
        width: Dimension.width,
        height: Dimension.size50,
        zIndex: Dimension.zIndexNormal,
        elevation: Dimension.elevationHigh,
        backgroundColor: Colors.colorWhite,
        justifyContent: "center"
    },
    cmntReplies: {
        marginTop: Dimension.margin2,
    },
    cmntReplyMainContainer: {
        flexDirection: "row",
        marginTop: Dimension.margin10
    },
    cmntReplyUserPicture: {
        width: Dimension.size30,
        height: Dimension.size30,
        borderRadius: Dimension.radius15,
        backgroundColor: Colors.colorXLightGray
    },
    cmntReplyContainer: {
        marginStart: Dimension.margin5
    },
    cmntNameReplyTextContainer: {
        marginEnd: Dimension.margin30,
        padding: Dimension.padding5,
        backgroundColor: Colors.colorXLightGray,
        borderRadius: Dimension.radius10
    },
    cmntNameReplyImageContainer: {
        marginEnd: Dimension.margin30,
        paddingEnd: Dimension.padding5,
    },
    cmntAddReplyContainer: {
        width: Dimension.size250,
        height: Dimension.size50,
        backgroundColor: Colors.colorLightGray,
        justifyContent: "center"
    },
    cmntAddPhotoContainer: {
        width: Dimension.size34,
        height: Dimension.size34,
        marginStart: Dimension.margin10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: Dimension.radius3,
        elevation: Dimension.elevationNormal,
        backgroundColor: Colors.colorWhite
    },
    cmntAddPhotoIcon: {
        width: Dimension.size25,
        height: Dimension.size25,
        tintColor: Colors.colorRed
    },
    cmntMessageContainer: {
        paddingStart: Dimension.padding10,
        paddingEnd: Dimension.padding10,
        borderRadius: Dimension.radius25,
        position: "absolute",
        top: Dimension.margin7,
        bottom: Dimension.margin7,
        left: Dimension.margin50,
        end: Dimension.margin50,
        justifyContent: "center",
        backgroundColor: Colors.colorXLightGray,
    },
    cmntImojiContainer: {
        width: Dimension.size30,
        height: Dimension.size30,
        position: "absolute",
        end: Dimension.margin5,
        alignItems: "center",
        justifyContent: "center"
    },
    cmntEmojiIcon: {
        width: Dimension.size20,
        height: Dimension.size20,
        tintColor: Colors.colorGray
    },
    cmntSendCommentContainer: {
        width: Dimension.size34,
        height: Dimension.size34,
        position: "absolute",
        end: Dimension.margin10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: Dimension.radius17,
        elevation: Dimension.elevationNormal,
        backgroundColor: Colors.colorPrimary
    },
    cmntSendCommentIcon: {
        width: Dimension.size20,
        height: Dimension.size20,
        marginEnd: Dimension.margin2,
        tintColor: Colors.colorWhite
    },
    cmntPaginationContainer: {
        height: Dimension.size35,
        width: Dimension.width,
        position: "absolute",
        top: Dimension.margin40,
        backgroundColor: Colors.colorOpacityXLightGray,
        justifyContent: "center",
        alignItems: "center"
    },


    //qr code scanner screen
    qrscnrContainerMain: {
        marginTop: Dimension.margin50
    },
    qrscnrScannerContainer: {
        height: Dimension.size500,
        backgroundColor: Colors.colorLightGray
    },
    qrscnrManualQRCode: {
        marginTop: Dimension.margin15,
        marginStart: Dimension.margin15,
        marginEnd: Dimension.margin15
    },

    //qr code viewer screen
    qrvwrContainerMain: {
        marginTop: Dimension.margin50,
        backgroundColor: Colors.colorXLightGray,
    },
    qrvwrQRDetailsContainer: {
        height: Dimension.size400,
        margin: Dimension.margin20,
        backgroundColor: Colors.colorWhite,
        alignItems: "center"
    },
    qrvwrUserPicture: {
        width: Dimension.size50,
        height: Dimension.size50,
        borderRadius: Dimension.size25,
        margin: Dimension.margin30
    },

    //report screen
    reportHeader: {
        flexDirection: "row",
        marginTop: Dimension.margin70,
        marginBottom: Dimension.margin2,
        marginStart: Dimension.margin20,
    },
    reportHeaderInfo: {
        marginStart: Dimension.margin20
    },
    reportHeaderImage: {
        width: Dimension.size15,
        height: Dimension.size15,
        marginBottom: Dimension.margin5,
        marginStart: Dimension.margin10,
        bottom: Dimension.size0,
        tintColor: Colors.colorOrange,
        alignSelf: "flex-end"
    },
    reportNameContainer: {
        width: "100%",
        flex: 1,
        flexDirection: "row",
        marginTop: Dimension.margin20,
        marginBottom: Dimension.margin5
    },
    widthFirstName: {
        flex: 0.5,
        marginStart: Dimension.margin20,
        marginEnd: Dimension.margin5
    },
    widthLastName: {
        flex: 0.5,
        marginStart: Dimension.margin5,
        marginEnd: Dimension.margin20
    },
    widthButton: {
        width: "50%",
        marginStart: Dimension.margin20,
        marginBottom: Dimension.margin20
    },



    //my account
    acFollowerContainer: {
        borderWidth: 1,
        borderColor: Colors.colorXLightGray,
        alignItems: "center",
        width: Dimension.width / 3,
        paddingHorizontal: 10,
        paddingBottom: 10
    },
    acFollowersPhotoContainer: {
        height: 60,
        width: 60,
        overflow: 'hidden',
        borderRadius: 85,
        borderColor: '#fff',
        borderWidth: 1,
        marginTop: 15
    },
    acFollowingStoreNameTxt: {
        // fontWeight: "bold",
        fontFamily: Fonts.ralewayBold,
        fontSize: 12.5,
        color: "#030303"
    },
    acFollowingStoreItemContainer: {
        borderWidth: 1,
        borderColor: Colors.colorXLightGray,
        alignItems: "center",
        paddingHorizontal: 10,
        paddingBottom: 10
    },
    acFollowingStoreAddressTxt: {
        marginTop: 5,
        fontSize: 12,
        color: "#9C9C9C",
        width: 160,
        textAlign: 'center',
        fontFamily: Fonts.ralewayRegular
    },
    acFollowFollowersBtnTxt: {
        fontFamily: Fonts.montserratMedium,
        fontSize: 10,
        color: Colors.colorPrimary,
        textAlign: "center",
        paddingTop: 2,
    },
    acLikedStoreNameTxt: {
        fontSize: 13,
        marginHorizontal: 5,
        // fontWeight: '400',
        fontFamily: Fonts.ralewayBold,
        color: '#000'
    },
    acLikedAddressTxt: {
        width: '60%',
        fontSize: 12,
        marginBottom: 2,
        fontFamily: Fonts.ralewayRegular,
    },
    acRvwGeadingRatingEditContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimension.width - 135
    },

    //settings
    stngOldPassWidthMargin: {
        width: Dimension.width - Dimension.size40,
        marginTop: 15
    },
    stngNewPassWidthMargin: {
        width: Dimension.width - Dimension.size40,
        marginTop: 55
    },
    stngConfirmPassWidthMargin: {
        width: Dimension.width - Dimension.size40,
        marginTop: 55
    },

    //common screens
    commonModalContainer: {
        // backgroundColor: 'rgba(58,58,58,0.8)',
        height: Dimension.height,
    },
    commonModalContent: {
        backgroundColor: Colors.colorWhite,
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        top: 130,
        paddingBottom: 30,
        elevation:5,
        padding:10
    },
    commonModalHead: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: Colors.colorLightGray,
        borderBottomWidth: 0.5,
        paddingVertical: 8
    },
    commonCloseModal: {
        width: Dimension.size30,
        height: Dimension.size30,
        justifyContent: "center",
        alignItems: "center",
        // position: "absolute",
        right: 0,
        margin: Dimension.margin5,
    },
    commonRemoveIcon: {
        width: Dimension.size13, height: Dimension.size13
    },
    commonModalBtnsContainer: {

    },
    commonModalBtns: {
        flexDirection: 'row',
        marginHorizontal: 10,
        paddingVertical: 15,
        justifyContent: 'space-between',
        borderBottomColor: Colors.colorLightGray,
        borderBottomWidth: 0.5
    },
    commonCardFooter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 7,
        borderTopWidth: 0.5,
        borderColor: 'gainsboro',
    },
    commonCardBtn: {
        flexDirection: 'row',
        padding: 10,
    },
    commonCardBtnIcon: {
        width: 20,
        height: 20,
        marginRight: 5
    },
    commonCardBtnTxt: {
        fontSize: 12,
        fontFamily: Fonts.montserratMedium,
    },
    commonGreenHeading: {
        color: '#000',
        fontSize: 16,
        // padding: 10,
        fontFamily: Fonts.ralewayBold,
    },
    commonCustomAlertBtnParent: {
        flexDirection: 'row',
        width: '100%',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        overflow: 'hidden',
        borderColor: Colors.colorLightGray,
        borderTopWidth: 0.5
    },
    commonCustomAlertBtn: {
        backgroundColor: Colors.colorWhite,
        width: '50%',
        alignItems: 'center',
        padding: 10,
        // borderBottomLeftRadius:
    },
    commonModalGradient: {
        position: 'absolute',
        height: 60,
        width: Dimension.width,
        flexDirection: 'row',
        top: 0,
        alignItems: 'center',
        zIndex: 5
    },
    greenHeading: {
        color: colors.colorPrimary,
        fontWeight: 'bold',
        fontSize: 16
    },
    activity_main: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 5,
        //elevation: 5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        width: Dimension.width,
        height: Dimension.height,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
    },
    activity_sub: {
        position: 'absolute',
        top: Dimension.height / 2,
        backgroundColor: '#0598F5',
        width: 50,
        alignSelf: 'center',
        justifyContent: "center",
        alignItems: 'center',
        zIndex: 10,
        //elevation:5,
        ...Platform.select({
            android: { elevation: 5, },
            ios: {
                shadowColor: '#999',
                shadowOffset: {
                    width: 0,
                    height: 3
                },
                shadowRadius: 5,
                shadowOpacity: 0.5,
            },
        }),
        height: 50,
        borderRadius: 10
    },
    commonPopupContent: {
        backgroundColor: Colors.colorWhite,
        position: 'absolute',
        bottom: 50,
        right: 30,
        left: 30,
        top: 70,
        // paddingBottom: 30,
        elevation:5,
        padding:10
    },
});
