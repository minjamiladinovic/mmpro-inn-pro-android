/*
*Common Tasks carried all over the project
*/

import React from 'react';
import Toast from 'react-native-root-toast';
import { Dimensions} from 'react-native';
import DeviceInfo from 'react-native-device-info';

const hasNotch = DeviceInfo.hasNotch(); 
const model = DeviceInfo.getModel();
export default class CommonTasks extends React.Component {

  //FUNCTION - for displaying toast
  static _displayToast(message){
    Toast.show(message, {
      duration: Toast.durations.SHORT,
      position: hasNotch?(model=='iPhone XR'?33:30):20,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor:"#1e1e1e",
      textColor:"#ffff",
      containerStyle: {
        padding:10,
        width: Dimensions.get('window').width,
        minHeight:60,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: "center",
      },

      onShow: () => {

      },
      onShown: () => {

      },
      onHide: () => {

      },
      onHidden: () => {

      }
    });
  }

  static _displayToastSuccess(message){
    Toast.show(message, {
      duration: Toast.durations.SHORT,
      position:10,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor:"#3A4958",
      textColor:"#ffff",
      containerStyle: {
        padding:10,
        width: Dimensions.get('window').width,
        minHeight:60,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: "center",
      },

      onShow: () => {

      },
      onShown: () => {

      },
      onHide: () => {

      },
      onHidden: () => {

      }
    });
  }

  static _displayToastNotification(message){
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position:10,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor:"#1e1e1e",
      textColor:"#ffff",
      containerStyle: {
        padding:10,
        width: Dimensions.get('window').width,
        minHeight:60,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: "center",
      },

      onShow: () => {

      },
      onShown: () => {

      },
      onHide: () => {

      },
      onHidden: () => {

      }
    });
  }




  static _displayToastStripe(message){
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position:0,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor:"#e0e0e0",
      textColor:"#ffff",
      containerStyle: {
        padding:10,
        width: Dimensions.get('window').width,
        minHeight:60,
        borderRadius: 0,
        justifyContent: 'center',
        alignItems: "center",
      },

      onShow: () => {

      },
      onShown: () => {

      },
      onHide: () => {

      },
      onHidden: () => {

      }
    });
  }

  

  //FUNCTION - verify whether entered email is valid or not
  static _verifyEmail(email){
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if(email.match(mailformat))
      {
          // alert("Valid Email");
          return true;
      }
      else
      {
          // alert("Invalid Email");
          return false;
      }
  }

  static serviceList = [
    {
        "name": "App Development",
        "id":"1",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fapp-development.jpg?alt=media&token=6bef7343-8f2f-4c4a-9425-2495451e2f08"           
    },
    {
        "name": "Area Rug Cleaning",
        "id":"2",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FAreaRugCleaning.jpg?alt=media&token=b79cada0-eada-459c-863b-bcbcf4dd1763"            
    },
    {
        "name": "Automotive A/C Repair",
        "id":"3",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fautomotive-ac-repair.jpg?alt=media&token=5c24874f-fdb0-4eec-bf6a-49d85a587cfd"            
    },
    {
        "name": "Automotive Brakes",
        "id":"4",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fautomotive-brakes.jpg?alt=media&token=668d3f0b-dcd2-4a27-811b-f84a192b66d9"          
    },
    {
        "name": "Babysitter",
        "id":"5",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fbabysitter.jpg?alt=media&token=1f9143de-033b-40c3-97c9-85bcf9e0db9c"           
    },
    {
        "name": "Bankruptcy Attorney",
        "id":"6",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FBankruptcyAttorney.jpg?alt=media&token=24cfc10b-b1aa-477d-9a39-2203afd30f71"          
    },
    {
        "name": "Bicycle Repair",
        "id":"7",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FBicycleRepair.jpg?alt=media&token=903c420a-25e6-49e2-8035-7de38885c35f"            
    },
    {
        "name": "Bicycle Sales",
        "id":"8",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FBicycleSales.jpg?alt=media&token=dad8c602-5c48-4d53-9641-35c20e8d7b27"              
    },
    {
        "name": "Blinds & Shutters Installation and Repairs",
        "id":"9",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fblinds-shutters-installation-repairs.jpg?alt=media&token=17e8e338-e9ec-47dc-aadf-62f1e19a9325"             
    },
    {
        "name": "Boat Maintenance",
        "id":"10",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FBoatMaintenance.jpg?alt=media&token=edde1423-aae4-426d-bb93-11a4cb14eed5"            
    },
    {
        "name": "Boat Repair",
        "id":"11",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FBoatRepair.jpg?alt=media&token=cac00bb6-bac1-4ce7-aead-50caed906938"              
    },
    {
        "name": "Body Building",
        "id":"12",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FBodyBuilding.jpg?alt=media&token=e91e4d59-6b9e-4666-b565-299f5f886450"              
    },
    {
        "name": "Auto Body Repair",
        "id":"13",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fauto-body-repair.jpg?alt=media&token=2273255c-e365-4631-836d-af3f0d64002c"               
    },
    {
        "name": "Bookkeeping",
        "id":"14",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fbookkeeping.jpg?alt=media&token=52cb6e7e-5f69-471f-bc32-48bc93f45466"            
    },
    {
        "name": "Business Consulting",
        "id":"15",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fbusiness-consulting.jpg?alt=media&token=8c939de1-a1b2-437e-937b-ca75f0dbc44c"            
    },
    {
        "name": "Cabinet Installation",
        "id":"16",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcabinet%20installation.jpg?alt=media&token=08fedc9c-ae5c-43f6-a7b7-90a3d0f9ad33"            
    },
    {
        "name": "Car & Truck Maintenance & Repair",
        "id":"17",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcar-truck-maintenance-repair.jpg?alt=media&token=189548d6-6977-46d4-8bfb-33eb264914dc"           
    },
    {
        "name": "Car and Truck Painting",
        "id":"18",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcar-truck-painting.jpg?alt=media&token=27dfbf14-427e-4d97-95ec-198b4a9057f9"

    },
    {
        "name": "Carpet Sales, Installation, Repair, & Removal",
        "id":"19",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcarpet-sales-installation-repair-removal.jpg?alt=media&token=2ecd6a2d-9dc6-4de9-b499-069dae708f0c"            
    },
    {
        "name": "Children Photo Sessions",
        "id":"20",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FChildrenPhotoSessions.jpg?alt=media&token=90431fc7-c121-417c-82f9-58eaea329850"            
    },
    {
        "name": "Chiropractic",
        "id":"21",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FChiropractic.jpg?alt=media&token=8259c7f5-a424-45e2-84d3-2e30819d4220"              
    },
    {
        "name": "Closets",
        "id":"22" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fclosets.jpg?alt=media&token=93dba45d-05e3-4de7-bd4d-074e60ed5334"             
    },
    {
        "name": "Floor Coatings",
        "id":"23",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ffloor-coatings.jpg?alt=media&token=824fd4e8-c467-4eb1-8eb6-890404a30c1c"            
    },
    {
        "name": "Commercial Air Vent & Duct Cleaning",
        "id":"24",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialAirVent%26Duct.jpg?alt=media&token=bed8d3cb-4dd4-4eb2-bbd2-f9d6f26bb0f0"           
    },
    {
        "name": "Commercial Asbestos Abatement",
        "id":"25",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialAsbestosAbatement.jpg?alt=media&token=c974aa08-83e6-4fe2-a1fe-d9ebca037fa4"             
    },
    {
        "name": "Commercial Asbestos Testing",
        "id":"26",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialAsbestosTesting.jpg?alt=media&token=bb476bea-efe9-492e-a962-a86c58bff339"            
    },
    {
        "name": "Commercial Block Wall",
        "id":"27",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialBlockWall.jpg?alt=media&token=024ff9e7-98fe-4c36-a293-3518dfc2de74"             
    },
    {
        "name": "Commercial Carpet Cleaning",
        "id":"28",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialCarpetCleaning.jpg?alt=media&token=a48e04fd-58c0-4d70-8de9-af9a47499528"             
    },
    {
        "name": "Commercial Cleaning Services",
        "id":"29",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialCleaningServices.jpg?alt=media&token=792f0c39-84f6-48e3-8c49-7267b4b58e65"             
    },
    {
        "name": "Commercial Concrete",
        "id":"30" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialConcrete.jpg?alt=media&token=e4348fc0-a99c-4a30-9aa5-8ed8abe5cebd"            
    },
    {
        "name": "Commercial Construction",
        "id":"31" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialConstruction.jpg?alt=media&token=f4f97b38-7e2d-4b95-b0a1-4ad1fe46df9a"            
    },
    {
        "name": "Commercial Drywall",
        "id":"32" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialDrywall.jpg?alt=media&token=2f481add-1900-4d17-8f67-c1a4ee9bdb84"           
    },
    {
        "name": "Commercial Electrical",
        "id":"33" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialElectrical.jpg?alt=media&token=6cae1caa-af69-4000-83e0-8b1d8622b217"           
    },
    {
        "name": "Commercial Fences",
        "id":"34" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialFences.jpg?alt=media&token=f4684bc8-d846-4ff0-927c-f77081db7903"            
    },
    {
        "name": "Commercial General Contractor",
        "id":"35" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialGeneralContractor.jpg?alt=media&token=3cff9da9-486d-490d-8824-28719efdc96f"            
    },
    {
        "name": "Commercial Pest Control",
        "id":"36" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialPestControl.jpg?alt=media&token=84cd91cf-5f8f-46a4-bf09-88050566eae5"            
    },
    {
        "name": "Commercial Plumbing",
        "id":"37",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialPlumbing.jpg?alt=media&token=1d3f510d-dd26-42da-ac96-b16510272829"            
    },
    {
        "name": "Commercial Real Estate Agent",
        "id":"38",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialRealEstateAgent.jpg?alt=media&token=dcf6a904-cd08-4271-bf22-728b8eb3a078"             
    },
    {
        "name": "Commercial Remodel",
        "id":"39",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialRemodel.jpg?alt=media&token=8e942ee5-27aa-4c03-a1b3-23d1f8d10847"            
    },
    {
        "name": "Commercial Roof Repair & Installation",
        "id":"40",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialRoofRepair%26Installation.jpg?alt=media&token=324ab468-54db-457e-b497-2cc06b1872c0"             
    },
    {
        "name": "Commercial Telephone",
        "id":"41",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCommercialTelephone.jpg?alt=media&token=bc31aa55-e004-40be-9abc-72ffdb3311ea"             
    },
    {
        "name": "Computer Repair",
        "id":"42" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcomputer-repair.jpg?alt=media&token=b970c350-c6b3-4aa6-8165-13ef98e2f171"            
    },
    {
        "name": "Corporate Law",
        "id":"43" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCorporateLaw.jpg?alt=media&token=8a334b9b-5c97-482b-844e-0e5a14bfcf46"        
    },
    {
        "name": "Countertop Installation",
        "id":"44",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcountertop-installation.jpg?alt=media&token=e3e31a75-0663-4c46-90ef-4a1a9a96e766"         
    },
    {
        "name": "Credit Card Processing",
        "id":"45",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCreditCardProcessing.jpg?alt=media&token=d8d3dd3e-224b-4623-83c8-9a33e4bd6799"            
    },
    {
        "name": "Custom Computer Build",
        "id":"46",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FCustomComputerBuild.jpg?alt=media&token=5e5733bb-65e4-45c1-b392-59f1073329ed"            
    },
    {
        "name": "Custom Home Build",
        "id":"47",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcustom-home-build.jpg?alt=media&token=308ddb5d-dd8d-4da2-b8a2-c6540c8906ee"           
    },
    {
        "name": "Dance",
        "id":"48",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FDance.jpg?alt=media&token=4e1bda16-cc17-4ff8-8308-a697f83ef353"           
    },
    {
        "name": "Dental",
        "id":"49",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FDental.jpg?alt=media&token=ba50f8e2-455b-499e-9dbd-235495782e10"            
    },
    {
        "name": "Diesel Maintenance & Repair",
        "id":"50",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fdiesel-maintenance-repair.jpg?alt=media&token=c4cc3da3-20ef-44e8-86c9-aeb152cdab9d"          
    },
    {
        "name": "Diesel Performance",
        "id":"51",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fdiesel-performance.jpg?alt=media&token=605a81cc-fbca-47b3-8405-ec52573fcb88"            
    },
    {
        "name": "Door Repair & Installation",
        "id":"52",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FDoorRepair%26Installation.jpg?alt=media&token=b0b638ec-90d2-4985-8db9-038699091379"            
    },
    {
        "name": "Driveway & Sidewalk",
        "id":"53",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FDriveway%26Sidewalk.jpg?alt=media&token=c6feab3f-e5f3-41cc-8bc1-d51453a0cf60"             
    },
    {
        "name": "Drone & Arial Photography",
        "id":"54",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FDrone%26ArialPhotography.jpg?alt=media&token=ecb56356-285e-4d70-a21b-0e38689e18d4"             
    },
    {
        "name": "Embroidery Services",
        "id":"55",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FEmbroideryServices.jpg?alt=media&token=f254d8dd-5139-42d1-88c2-a6b8c72de951"             
    },
    {
        "name": "Family Attorney",
        "id":"56",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFamilyAttorney.jpg?alt=media&token=2b3056fb-ea13-4067-a9f8-36733fddfbf9"             
    },
    {
        "name": "Family Photo Sessions",
        "id":"57",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFamilyPhotoSessions.jpg?alt=media&token=123c58af-28d7-4f82-9ad7-e4557225b403"             
    },
    {
        "name": "Faux Painting",
        "id":"58",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFauxPainting.jpg?alt=media&token=0ada313a-ea4b-4d54-ac69-c7fbe97782fc"             
    },
    {
        "name": "Filing Taxes",
        "id":"59",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ffilling-taxes.jpg?alt=media&token=29da2839-6bce-4539-b988-fc832621b726"              
    },
    {
        "name": "Fire & Smoke Restoration",
        "id":"60",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ffire-restoration.jpg?alt=media&token=5cded916-6b24-4a77-80cd-c505e3b1327a"           
    },
    {
        "name": "Fleet Service",
        "id":"61",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFleetService.jpg?alt=media&token=04a8b51a-c2fa-42c8-a2ed-fb3a587d221d"            
    },
    {
        "name": "Flood Restoration",
        "id":"62",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fflood-restoration.jpg?alt=media&token=2511e00d-8ff9-4589-9866-b5a183013766"             
    },
    {
        "name": "Floor Installation",
        "id":"63",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFloorInstallation.jpg?alt=media&token=edceb988-696f-47e3-974c-0ac517656d63"            
    },
    {
        "name": "Floor Repair",
        "id":"64",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFloorRepair.jpg?alt=media&token=62ae9849-fda9-4adf-be30-35476481a247"             
    },
    {
        "name": "Foundation Repair and Stabilization",
        "id":"65",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FFoundationRepair%26Stabilization.jpg?alt=media&token=76ebfa49-dafa-496a-8ca2-dd21a0fe4f03"             
    },
    {
        "name": "Garage Cabinets",
        "id":"66" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fgarage-cabinets.jpg?alt=media&token=47ea7cc5-2b0e-4dfb-b3fc-7a4a9034fe3f"            
    },
    {
        "name": "Garage Door",
        "id":"67",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fgarage-door.jpg?alt=media&token=b0150fc5-f1cb-43ba-9035-dfcd3e32662a"            
    },
    {
        "name": "Glass & Mirror Repair & Replacement",
        "id":"68",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FGlass%26MirrorReplacement.jpg?alt=media&token=14f7471a-5ae4-422f-b253-3448240788d9"               
    },
    {
        "name": "Graphic Design",
        "id":"69",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fgraphic-design.jpg?alt=media&token=9d01dc94-ddf2-476e-9719-72ff316195ff"               
    },
    {
        "name": "Gutter Cleaning & Service",
        "id":"70",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FGutterCleaningService.jpg?alt=media&token=337ad391-e710-42dd-9d17-286886ee7853"            
    },
    {
        "name": "Gym Membership",
        "id":"71",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FGymMembership.jpg?alt=media&token=04febbf7-cb7a-442a-9aba-6ec7effb9cbb"           
    },
    {
        "name": "Handyman",
        "id":"72",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fhandyman.jpg?alt=media&token=03e775f2-753b-48bf-850d-3a8a1a4c3f4f"              
    },
    {
        "name": "Hair Stylist",
        "id":"73",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FHairStylist.jpg?alt=media&token=c372efba-41af-4a47-bd25-a0c7f52c4cb4"           
    },
    {
        "name": "Home Automation",
        "id":"74",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fhome-automation.jpg?alt=media&token=1103b677-c9a5-4e13-8963-c81c2fe80c9e"              
    },
    {
        "name": "HVAC Maintenance & Repair",
        "id":"75",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fhvac-maintenance-repair.jpg?alt=media&token=202368c3-d371-439f-99df-9abd294c7663"            
    },
    {
        "name": "Imaging (MRI/XRAY)",
        "id":"76",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fit-support.jpg?alt=media&token=015cb2f8-104e-4a97-9201-5c88837853ed"            
    },
    {
        "name": "Indoor Air Quality Testing",
        "id":"77",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FIndoorAirQualityTesting.jpg?alt=media&token=9c3e187f-939c-4068-9cf1-475b438b36e8"            
    },
    {
        "name": "Irrigation & Sprinkler Installation & Repair",
        "id":"78",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FIrrigation%26SprinklerInstallation.jpg?alt=media&token=f55fc3de-a420-4cb9-a38c-a7e9977ebd0e"           
    },
    {
        "name": "IT Support",
        "id":"79",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fit-support.jpg?alt=media&token=015cb2f8-104e-4a97-9201-5c88837853ed"            
    },
    {
        "name": "Janitorial Services",
        "id":"80",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FJanitorialServices.jpg?alt=media&token=8e025b5d-ba0d-4efd-b364-020ebd6924be"            
    },
    {
        "name": "Junk Removal",
        "id":"81",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FJunkRemoval.jpg?alt=media&token=88a31344-e9e0-441d-9b63-c070eb4b572c"             
    },
    {
        "name": "Landscaping Design & Installation",
        "id":"82",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Flandscaping-design-installation.jpg?alt=media&token=70db68e2-4394-4c4b-a1e2-bce40fba6f16"             
    },
    {
        "name": "Landscaping Maintenance",
        "id":"83",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Flandscaping-maintenance.jpg?alt=media&token=5df8a8b7-6a1b-4965-b63d-e2156baea151"           
    },
    {
        "name": "Lawn Care",
        "id":"84",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Flawn-care.jpg?alt=media&token=c460deec-b491-4d3c-9dbf-6a7746e95a76"            
    },
    {
        "name": "Lighting Service & Installation",
        "id":"85",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Flighting-service-installation.jpg?alt=media&token=5794427d-c53d-47c5-890a-2b690d5d5086"            
    },
    {
        "name": "Loan Services",
        "id":"86",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FLoanServices.jpg?alt=media&token=b9dd4029-0f79-49b3-b670-9b1415d4ae01"            
    },
    {
        "name": "Locksmith & Keys",
        "id":"87",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FLocksmith%26Keys.jpg?alt=media&token=c07bcfc1-594f-4f21-beac-ad8d62a4aacd"            
    },
    {
        "name": "Logo Design",
        "id":"88",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Flogo-design.jpg?alt=media&token=ad7aee2b-d677-49c3-9f1e-8e1f882cd242"            
    },
    {
        "name": "Low Voltage Service & Installation",
        "id":"89",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Flogo-design.jpg?alt=media&token=ad7aee2b-d677-49c3-9f1e-8e1f882cd242"            
    },
    {
        "name": "Maid Services",
        "id":"90",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fmaid-service.jpg?alt=media&token=2db3518f-f571-4500-93ee-3f46aa3ba121"          
    },
    {
        "name": "Medical",
        "id":"91",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FMedical.jpg?alt=media&token=0590e1fe-a2b9-4124-aaec-c5787473199c"            
    },
    {
        "name": "Misting & Fogging Systems",
        "id":"92",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fmisting%20-fogging-systems.jpg?alt=media&token=475aee68-a84d-4317-8080-1628bda224c3"             
    },
    {
        "name": "Mold Prevention & Abatement",
        "id":"93",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FMoldPrevention%26Abatement.jpg?alt=media&token=694718cb-db05-4c83-88a7-648cb32c52da"            
    },
    {
        "name": "Mold Testing",
        "id":"94",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FMoldTesting.jpg?alt=media&token=f380d1fa-3d0e-4233-8119-b12667faad1c"            
    },
    {
        "name": "Mortgage Services",
        "id":"95",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FMortgageServices.jpg?alt=media&token=e22a8f28-44ad-4d73-ad9b-b888e728c537"            
    },
    {
        "name": "Move Interstate",
        "id":"96",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fmoving-interstate.jpg?alt=media&token=2e27b744-5844-4d88-9912-d0aabaab6122"           
    },
    {
        "name": "Move Same State",
        "id":"97",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fmove-same-state.jpg?alt=media&token=c3e1f504-a690-465f-9c40-c47983b74a72"            
    },
    {
        "name": "Move Within 150 Miles",
        "id":"98",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fmove-within-150-miles.jpg?alt=media&token=ce09df8d-e00d-490d-8dcd-d07bfa564969"            
    },
    {
        "name": "Move Within 50 Miles",
        "id":"99",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fmove-within-50-miles.jpg?alt=media&token=6f4ea6c8-be08-4e0c-b5dc-a7099aada7bb"            
    },
    {
        "name": "Mural Painting",
        "id":"100",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FMuralPainting.jpg?alt=media&token=b7f5efbf-d8ca-4bd5-9f44-f36cdd027eca"           
    },
    {
        "name": "Natural Stone Cleaning & Restoration",
        "id":"101",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FNaturalStoneCleaning%26Restoration.jpg?alt=media&token=cf99c4ea-5f15-48ec-988f-476070545671"              
    },
    {
        "name": "Network Sales & Service",
        "id":"102",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FNetworkSales%26Service.jpg?alt=media&token=90bf3916-ba6f-423d-8729-75a103416ca4"              
    },
    {
        "name": "Office Cabinets",
        "id":"103",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FOfficeCabinets.jpg?alt=media&token=a9e980c2-1c5f-40c2-a010-38cb3b6cb8ac"              
    },
    {
        "name": "Oil Change",
        "id":"104",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Foil-change.jpg?alt=media&token=fc302894-f0f0-4d7c-acfb-178ab0b17f2a"              
    },
    {
        "name": "Online Reputation Management",
        "id":"105",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FOnlineReputationManagement.jpg?alt=media&token=b27dd73a-5f45-47eb-8c7e-f2886524dfd0"            
    },
    {
        "name": "Painting - Exterior",
        "id":"106",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPaintExterior.jpg?alt=media&token=4ccdc3e9-71b2-4820-b81d-4ee2ce710bec"            
    },
    {
        "name": "Painting - Interior",
        "id":"107",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPaintInterior.jpg?alt=media&token=0ab3c87b-12dc-45d2-96e0-3265fb19e919"            
    },
    {
        "name": "Parking Lot Service, Repair & Installation",
        "id":"108",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FParkingLotServiceRepair%26Installation.jpg?alt=media&token=d19b0f97-1298-44ce-bd8b-b9e57c383474"            
    },
    {
        "name": "Patio Enclosures & Covers",
        "id":"109",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPatioEnclosures%26Covers.jpg?alt=media&token=97160f6d-7282-49f5-98cb-67f8b5a384d8"            
    },
    {
        "name": "Pet Doors",
        "id":"110",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPetDoors.jpg?alt=media&token=21716985-42cc-4bf8-b8ce-fb9b63ad43a4"            
    },
    {
        "name": "Pet Grooming",
        "id":"111",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPetGrooming.jpg?alt=media&token=772df82c-5391-4102-a058-de900e501639"            
    },
    {
        "name": "Physical Trainer",
        "id":"112",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPhysicalTrainer.jpg?alt=media&token=9d903e27-caf3-4f0b-82fa-4cb8b7112a91"            
    },
    {
        "name": "Pool & Spa Build & Remodel",
        "id":"113",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPool%26SpaBuild%26Remodel.jpg?alt=media&token=304bea8c-23b8-4fb1-aa25-1721803527ff"            
    },
    {
        "name": "Pool Decking & Coatings",
        "id":"114",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPoolDecking%26Coatings.jpg?alt=media&token=d3961d17-42b8-4918-a02b-a9372dd7882f"            
    },
    {
        "name": "Pool Fences & Barriers",
        "id":"115",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPoolFences%26Barriers.jpg?alt=media&token=916c22ea-bf0b-4f06-9c94-2350acd24e66"             
    },
    {
        "name": "Pool Maintenance & Repair",
        "id":"116",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fpool-maintenance-and-repair.jpg?alt=media&token=628c4cc1-bf76-4a1e-9f1e-320299c31948"            
    },
    {
        "name": "Popcorn Ceiling Removal & Repair",
        "id":"117",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPopcornCeilingRemoval.jpg?alt=media&token=02cf2a02-156f-4dad-ad04-e7d71c6cdd4f"            
    },
    {
        "name": "Portrait Photography",
        "id":"118",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPortraitPhotographer.jpg?alt=media&token=0db7130d-824f-439b-92c7-f0cd238c17f8"            
    },
    {
        "name": "PPC (Pay Per Click) Services",
        "id":"119",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fpay-per-click-ppc.jpg?alt=media&token=74c0e4d2-1dad-4b51-82b1-1aa8f2766f55"            
    },
    {
        "name": "Pressure (Power) Washing",
        "id":"120",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fpressure-washing-power-washing.jpg?alt=media&token=68e622ab-f18f-4894-93d3-f16aed553a46"           
    },
    {
        "name": "Print Services",
        "id":"121",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPrintServices.jpg?alt=media&token=0c2e65b1-f438-46bc-bfdb-1730d6a133d1"            
    },
    {
        "name": "Promotional Products",
        "id":"122",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FPromotionalProducts.jpg?alt=media&token=3147d689-2df9-4777-abad-dc646bc8a070"             
    },
    {
        "name": "Residential Air Vent & Duct Cleaning",
        "id":"123",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialAirVent%26DuctCleaning.jpg?alt=media&token=e637e1a6-cbce-4ad2-a077-0bb1ebefe2ab"             
    },
    {
        "name": "Residential Asbestos Abatement",
        "id":"124",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialAsbestosAbatement.jpg?alt=media&token=d0bd416f-3733-408c-916a-29f2ea80a180"             
    },
    {
        "name": "Residential Asbestos Testing",
        "id":"125",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialAsbestosTesting.jpg?alt=media&token=c3d7ebea-2f75-48b9-afc2-b05040cea54f"             
    },
    {
        "name": "Residential Block Wall",
        "id":"126" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialBlockWall.jpg?alt=media&token=9ca26ed2-3c56-467b-a3f3-2567ce503c65"            
    },
    {
        "name": "Residential Carpet Cleaning",
        "id":"127" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fresidential-carpet-cleaning.jpg?alt=media&token=31acd405-9679-458a-8e73-a14812bda6f5"            
    },
    {
        "name": "Residential Cleaning Services",
        "id":"128",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fresidential-cleaning-services.jpg?alt=media&token=db138e55-3c94-4dc6-8f7a-cbffa105f6c7"          
    },
    {
        "name": "Residential Concrete",
        "id":"129",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialConcrete.jpg?alt=media&token=f42530d1-1d1f-4c03-b4ee-6b8e057a7a48"          
    },
    {
        "name": "Residential Construction",
        "id":"130",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialConstruction.jpg?alt=media&token=5af111a5-fee8-4175-9117-8332682a53dd"            
    },
    {
        "name": "Residential Drywall",
        "id":"131",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialDrywall.jpg?alt=media&token=89d2ab46-4abe-405f-b0e1-67122c91fd54"            
    },
    {
        "name": "Residential Electrical",
        "id":"132",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialElectrical.jpg?alt=media&token=51699ae8-cd2f-4743-8c9d-b12d01e81546"            
    },
    {
        "name": "Residential Fences",
        "id":"133",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialFences.jpg?alt=media&token=73ab4ad4-0b57-4974-abb9-e83dc80872de"            
    },
    {
        "name": "Residential General Contractor",
        "id":"134",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialGeneralContractor.jpg?alt=media&token=1ebae8ba-8344-432d-9e46-28cb76a777a2"            
    },
    {
        "name": "Residential Pest Control",
        "id":"135",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fresidential-pest-control.jpg?alt=media&token=4e1e4b91-045d-4163-9c0c-49ec780b39a2"

    },
    {
        "name": "Residential Plumbing",
        "id":"136",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialPlumbing.jpg?alt=media&token=975292cd-ba9a-41e1-bf7f-65155ad0f600"          
    },
    {
        "name": "Residential Real Estate Agent",
        "id":"137",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fresidential-real-estate.jpg?alt=media&token=adf78587-f5b0-46a1-aa62-12321aa1300e"             
    },
    {
        "name": "Residential Remodel",
        "id":"138",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialRemodel.jpg?alt=media&token=9a76d96a-6482-45aa-a71d-b197181c4531"             
    },
    {
        "name": "Residential Roof Repair & Installation",
        "id":"139",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialRoofRepair%26Installation.jpg?alt=media&token=daf8f247-63f3-41fa-ac4a-c9d7f42f739a"            
    },
    {
        "name": "Residential Telephone",
        "id":"140",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialTelephone.jpg?alt=media&token=34ac1927-8b43-4650-aaf0-8f559f2730bc"             
    },
    {
        "name": "Retractable Screens & Awnings",
        "id":"141",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FRetractableScreens%26Awnings.jpg?alt=media&token=b4226dfa-9968-4cac-a90e-a09ba84e9317"             
    },
    {
        "name": "Reverse Osmosis Sales & Installation",
        "id":"142",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FReverseOsmosisSales%26Installation.jpg?alt=media&token=d665bfe7-525f-4d1b-8044-bcbff43d4ba6"             
    },
    {
        "name": "Reverse Osmosis Service & Repair",
        "id":"143",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FReverseOsmosisService%26Repair.jpg?alt=media&token=a773b74f-cb04-461c-882d-2305a2a776ef"             
    },
    {
        "name": "Safe Sales & Installation",
        "id":"144",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSafeSales%26Installation.jpg?alt=media&token=8b2f4eaa-8542-49c2-a9bd-d17608fcfa74"             
    },
    {
        "name": "Screen Cleaning",
        "id":"145",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FScreenCleaning.jpg?alt=media&token=8d558afd-4c77-4b55-beec-a34c2704d207"             
    },
    {
        "name": "Screen Door Repair & Replacement",
        "id":"146"  ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FScreenDoorRepair%26Replacement.jpg?alt=media&token=d825c096-9051-47f6-a991-a4db399f52ba"           
    },
    {
        "name": "Search Engine Marketing (SEM)",
        "id":"147",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fsearch-engine-marketing.jpg?alt=media&token=c9703cef-5384-4a0a-b949-7453ef269dcc"            
    },
    {
        "name": "Security Doors",
        "id":"148",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSecurityDoors.jpg?alt=media&token=f7b77298-19b6-4b52-a180-74d74ac28aa8" 

    },
    {
        "name": "Security System",
        "id":"149",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fsecurity-system.jpg?alt=media&token=b6113ce1-627d-4c76-8988-22d2d75b55e5"             
    },
    {
        "name": "Security Windows",
        "id":"150",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSecurityWindows.jpg?alt=media&token=df399d28-c630-428b-8161-9c3d9f03dc88"            
    },
    {
        "name": "SEO Services",
        "id":"151",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fseo-search-engine-optimization.jpg?alt=media&token=17b14874-d354-4300-9cf7-957d596e5b70"           
    },
    {
        "name": "Septic Inspections",
        "id":"152",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSepticInspections.jpg?alt=media&token=66284c6a-89fa-4c16-ac00-991ad5769699"            
    },
    {
        "name": "Septic Pumping",
        "id":"153",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSepticPumping.jpg?alt=media&token=1af182bc-950a-48fe-bbb8-0f718c2ba28a"            
    },
    {
        "name": "Septic Repair & Installation",
        "id":"154",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSepticRepair%26Installation.jpg?alt=media&token=4d409c07-1dfb-4bfb-816f-78d2c71efcd6"            
    },
    {
        "name": "Shower & Bath Repair & Replacement",
        "id":"155",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FShower%26BathRepair%26Replacment.jpg?alt=media&token=7b0cda01-3652-4403-bc47-9b34706654ee"           
    },
    {
        "name": "Siding Repair & Replacement",
        "id":"156",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSidingRepair%26Replacement.jpg?alt=media&token=1e022a2e-b7a4-4778-82d8-6667f5ae095a"            
    },
    {
        "name": "Signs",
        "id":"157",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fbusiness-signs.jpg?alt=media&token=04be2e42-f948-4e3d-b4b7-7b5551de56ba"        
    },
    {
        "name": "Sliding Glass Door Repair & Replacement",
        "id":"158",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSlidingGlassDoorRepair%26Replacement.jpg?alt=media&token=07cf7273-c5b6-4ca0-9d8c-97203bbff6c5"            
    },
    {
        "name": "Social Media Service",
        "id":"159",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fsocial-media-management.jpg?alt=media&token=0d3a4777-6ccb-47bd-b82d-522616eee859"            
    },
    {
        "name": "Solar Energy Systems",
        "id":"160",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FSolarEnergySystems.jpg?alt=media&token=d1634d9d-fe50-45ae-98df-f33a7f05bd89"            
    },
    {
        "name": "Storage & Closet",
        "id":"161",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FStorage%26Closet.jpg?alt=media&token=d97b517e-13ea-4c6a-b451-2ef644584c3c"            
    },
    {
        "name": "Stucco Installation & Repair",
        "id":"162",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FStuccoInstallation%26Repair.jpg?alt=media&token=3a94c95c-fb24-4396-b380-2aaf8cbbbd07"            
    },
    {
        "name": "Tax Attorney",
        "id":"163",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FTaxAttorney.jpg?alt=media&token=98811060-8baf-4ee5-9956-1184339d7ce0"            
    },
    {
        "name": "Termite Control",
        "id":"164",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FTermiteControl.jpg?alt=media&token=349a71c3-4366-4a18-b8e9-05906e34f207"            
    },
    {
        "name": "Termite Inspection",
        "id":"165",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FTermiteInspection.jpg?alt=media&token=7d5f920a-55ed-4562-8deb-5578153eecba"            
    },
    {
        "name": "Movie Theater Installation & Service",
        "id":"166",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FMovieTheaterInstallation%26Service.jpg?alt=media&token=5b14a33b-3c5e-4df5-a14a-f0c6f93dbc33"            
    },
    {
        "name": "Tile & Stone Sales & Installation",
        "id":"167" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FTile%26StoneSales%26Installation.jpg?alt=media&token=61ec7362-6cf8-4c37-ac31-a43c1c047917"           
    },
    {
        "name": "Tile and Grout Cleaning",
        "id":"168" ,
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FTile%26GroutCleaning.jpg?alt=media&token=00469063-fc58-42c4-9452-260d648c56f2"           
    },
    {
        "name": "Tires",
        "id":"169",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ftire-service.jpg?alt=media&token=8df1e7bb-e164-4524-accc-55c331cc08a8"            
    },
    {
        "name": "Towing",
        "id":"170",
        "service_url" :"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ftowing.jpg?alt=media&token=79866514-7c4c-4541-9817-e7b6d8504eea"           
    },
    {
        "name": "Transmission",
        "id":"171",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ftransmission.jpg?alt=media&token=74d99f85-1d17-4bb6-a853-a7721c4a4c7e"            
    },
    {
        "name": "Tree Care",
        "id":"172",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Ftree-service.jpg?alt=media&token=2951b24f-9275-42f1-8ae9-a99b4a46cc1a"            
    },
    {
        "name": "Turf & Synthetic Grass",
        "id":"173",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fsynthetic-grass.jpg?alt=media&token=109728ca-b7ed-467b-bedd-44229b337336"          
    },
    {
        "name": "Upholstery Cleaning",
        "id":"174",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fupholstery-cleaning.jpg?alt=media&token=fb6536ff-9026-4230-893a-a1976f0fd7e0"            
    },
    {
        "name": "Upholstery Repair & Refinishing",
        "id":"175",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fuholstery-repair.jpg?alt=media&token=a65037f7-a339-4e22-a1c2-c6d10fec2b2c"           
    },
    {
        "name": "Vehicle Window Tinting",
        "id":"176",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcar-truck-tint.jpg?alt=media&token=ecf02b22-48a9-4f47-af82-ee26dbcb4976"          
    },
    {
        "name": "Vehicle Wraps & Graphics",
        "id":"177",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fcar-truck-wraps-graphics.jpg?alt=media&token=a41c07e4-ecd8-4151-9679-12484eaf0006"           
    },
    {
        "name": "Veterinary Services",
        "id":"178",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FVeterinaryServices.jpg?alt=media&token=1e011a65-ee00-4be5-b6c0-753068c52b75"            
    },
    {
        "name": "Virus Removal",
        "id":"179",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FVirusRemoval.jpg?alt=media&token=3f83bc50-2940-4768-a0ca-4f8b922c9490"            
    },
    {
        "name": "VoIP Managed Systems & Services",
        "id":"180",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FVOIPManagedSystems%26Services.jpg?alt=media&token=3989592c-57e2-4391-8278-5491542d58a1"            
    },
    {
        "name": "Water Damage Restoration",
        "id":"181",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWaterDamageRestoration.jpg?alt=media&token=63fd8070-5198-491f-b3cf-2127897cf2d5"            
    },
    {
        "name": "Water Heater Sales Repair & Installation",
        "id":"182",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWaterHeaterSalesRepair%26Installation.jpg?alt=media&token=a21193fe-a0f3-483d-b274-9c53de822afe"            
    },
    {
        "name": "Water Softener Sales Repair & Installation",
        "id":"183",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWaterSoftenerSalesRepair%26Installation.jpg?alt=media&token=3d75e1a2-9ce1-403f-b21a-4d764937f13a"            
    },
    {
        "name": "Website Development",
        "id":"184",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWebsiteDevelopment.jpg?alt=media&token=30387649-9d76-432b-bad3-d33649531d8a"            
    },
    {
        "name": "Wedding Photography",
        "id":"185",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWeddingPhotography.jpg?alt=media&token=9e975345-28af-409b-bee8-45148ee5bbe9"            
    },
    {
        "name": "Weed Control",
        "id":"186",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fweed-control.jpg?alt=media&token=5bcf5b2f-6e46-45dc-b464-fc0cfe45ebcd"             
    },
    {
        "name": "Welding Services",
        "id":"187",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWeldingServices.jpg?alt=media&token=818fa546-948c-4c69-94d5-b6f1b3d904f0"            
    },
    {
        "name": "Window Cleaning & Washing",
        "id":"188",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fwindow-washing.jpg?alt=media&token=63b479a4-60a8-481d-89df-0503a59d822c"             
    },
    {
        "name": "Window Installation",
        "id":"189",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWindowInstallation.jpg?alt=media&token=da402803-52e5-4856-88d9-9a4e3e0e8348"            
    },
    {
        "name": "Window Screen Repair & Replacement",
        "id":"190",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FWindowScreenRepair%26Replacment.jpg?alt=media&token=864ad781-a3cf-4084-a0df-d887855742f1"             
    },
    {
        "name": "Residential Window Tinting",
        "id":"191",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2FResidentialWindowtinting.jpg?alt=media&token=b81665ac-9f8e-4ca9-866e-ffe22f39421d"           
    },
    {
        "name": "Windshield & Glass Repair or Replacement",
        "id":"192",
        "service_url":"https://firebasestorage.googleapis.com/v0/b/pro-in-your-pocket.appspot.com/o/service-images%2Fwindshield-repair-replacement-windscreen.jpg?alt=media&token=daf3d615-e91d-4351-9fa6-053d6479df69"             
    }
];



static stateList = [
    
    {
        "name": "Alabama",
        "abbreviation": "AL"
    },
    {
        "name": "Alaska",
        "abbreviation": "AK"
    },
    {
        "name": "American Samoa",
        "abbreviation": "AS"
    },
    {
        "name": "Arizona",
        "abbreviation": "AZ"
    },
    {
        "name": "Arkansas",
        "abbreviation": "AR"
    },
    {
        "name": "California",
        "abbreviation": "CA"
    },
    {
        "name": "Colorado",
        "abbreviation": "CO"
    },
    {
        "name": "Connecticut",
        "abbreviation": "CT"
    },
    {
        "name": "Delaware",
        "abbreviation": "DE"
    },
    {
        "name": "District Of Columbia",
        "abbreviation": "DC"
    },
    {
        "name": "Federated States Of Micronesia",
        "abbreviation": "FM"
    },
    {
        "name": "Florida",
        "abbreviation": "FL"
    },
    {
        "name": "Georgia",
        "abbreviation": "GA"
    },
    {
        "name": "Guam",
        "abbreviation": "GU"
    },
    {
        "name": "Hawaii",
        "abbreviation": "HI"
    },
    {
        "name": "Idaho",
        "abbreviation": "ID"
    },
    {
        "name": "Illinois",
        "abbreviation": "IL"
    },
    {
        "name": "Indiana",
        "abbreviation": "IN"
    },
    {
        "name": "Iowa",
        "abbreviation": "IA"
    },
    {
        "name": "Kansas",
        "abbreviation": "KS"
    },
    {
        "name": "Kentucky",
        "abbreviation": "KY"
    },
    {
        "name": "Louisiana",
        "abbreviation": "LA"
    },
    {
        "name": "Maine",
        "abbreviation": "ME"
    },
    {
        "name": "Marshall Islands",
        "abbreviation": "MH"
    },
    {
        "name": "Maryland",
        "abbreviation": "MD"
    },
    {
        "name": "Massachusetts",
        "abbreviation": "MA"
    },
    {
        "name": "Michigan",
        "abbreviation": "MI"
    },
    {
        "name": "Minnesota",
        "abbreviation": "MN"
    },
    {
        "name": "Mississippi",
        "abbreviation": "MS"
    },
    {
        "name": "Missouri",
        "abbreviation": "MO"
    },
    {
        "name": "Montana",
        "abbreviation": "MT"
    },
    {
        "name": "Nebraska",
        "abbreviation": "NE"
    },
    {
        "name": "Nevada",
        "abbreviation": "NV"
    },
    {
        "name": "New Hampshire",
        "abbreviation": "NH"
    },
    {
        "name": "New Jersey",
        "abbreviation": "NJ"
    },
    {
        "name": "New Mexico",
        "abbreviation": "NM"
    },
    {
        "name": "New York",
        "abbreviation": "NY"
    },
    {
        "name": "North Carolina",
        "abbreviation": "NC"
    },
    {
        "name": "North Dakota",
        "abbreviation": "ND"
    },
    {
        "name": "Northern Mariana Islands",
        "abbreviation": "MP"
    },
    {
        "name": "Ohio",
        "abbreviation": "OH"
    },
    {
        "name": "Oklahoma",
        "abbreviation": "OK"
    },
    {
        "name": "Oregon",
        "abbreviation": "OR"
    },
    {
        "name": "Palau",
        "abbreviation": "PW"
    },
    {
        "name": "Pennsylvania",
        "abbreviation": "PA"
    },
    {
        "name": "Puerto Rico",
        "abbreviation": "PR"
    },
    {
        "name": "Rhode Island",
        "abbreviation": "RI"
    },
    {
        "name": "South Carolina",
        "abbreviation": "SC"
    },
    {
        "name": "South Dakota",
        "abbreviation": "SD"
    },
    {
        "name": "Tennessee",
        "abbreviation": "TN"
    },
    {
        "name": "Texas",
        "abbreviation": "TX"
    },
    {
        "name": "Utah",
        "abbreviation": "UT"
    },
    {
        "name": "Vermont",
        "abbreviation": "VT"
    },
    {
        "name": "Virgin Islands",
        "abbreviation": "VI"
    },
    {
        "name": "Virginia",
        "abbreviation": "VA"
    },
    {
        "name": "Washington",
        "abbreviation": "WA"
    },
    {
        "name": "West Virginia",
        "abbreviation": "WV"
    },
    {
        "name": "Wisconsin",
        "abbreviation": "WI"
    },
    {
        "name": "Wyoming",
        "abbreviation": "WY"
    },
    {
        "name": "District of Columbia",
        "abbreviation": "DC"
    }
];

}